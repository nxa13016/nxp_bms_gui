# -*- coding: utf-8 -*-
# ------------------------------------------------------------------------------
# Copyright (c) 2015, NXP Semiconductors.
# All rights reserved.
#  

# Redistribution and use in source and binary forms, with or without modification,
# are permitted provided that the following conditions are met:
#  
# o Redistributions of source code must retain the above copyright notice, this list
#   of conditions and the following disclaimer.
#  
# o Redistributions in binary form must reproduce the above copyright notice, this
#   list of conditions and the following disclaimer in the documentation and/or
#   other materials provided with the distribution.
#  
# o Neither the name of NXP Semiconductors nor the names of its
#   contributors may be used to endorse or promote products derived from this
#   software without specific prior written permission.
#  
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
# ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
# WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
# DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR
# ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
# (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
# LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
# ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
# (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
# SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
# ------------------------------------------------------------------------------

"""
Created on Thu Sep 08 16:17:31 2016

@author: R51406
@version: 
"""

import sys
from PyQt4.QtCore import *                              #@UnusedWildImport (Eclipse)
from PyQt4.QtGui import *                               #@UnusedWildImport (Eclipse)
from numpy import *
#from MySerial import *                                  #@UnusedWildImport (Eclipse)
#from batpack import *                                   #@UnusedWildImport (Eclipse)
from MC3377x import *                                   #@UnusedWildImport (Eclipse)
from CommId import *                                    #@UnusedWildImport (Eclipse)
import numpy as np
from guiqwt.pyplot import *

ErrorMsg =   ['Ok', "Tx", "NoRes", "Len", "CRC" ]        
ErrorColor = [Qt.green, Qt.blue, Qt.red, Qt.yellow, Qt.magenta]

RtermStatus = ['Open','Closed']
RtermColor = [Qt.gray,Qt.cyan]

BMSVIEW = [
    #  ID                ,    name              ,  unit ,  decoder   , physical
#( ID_BCCSTATE            , 'Fault'              , ''    , codeHex16  ,      0 ),
#( ID_RTERM                , 'RTERM'            , 'n/a'  , None       ,      0 ),
#( ID_GUID                , 'GUID'               , None  , None       ,      0 ),
( ID_MEAS_VPWR           , 'Stack'              , 'V'   , codeVPwr   ,      0 ),
( ID_MEAS_VCT1           , 'Cell-1'             , 'V'   , codeVCell  ,      0 ),
( ID_MEAS_VCT2           , 'Cell-2'             , 'V'   , codeVCell  ,      0 ),
( ID_MEAS_VCT3           , 'Cell-3'             , 'V'   , codeVCell  ,      0 ),
( ID_MEAS_VCT4           , 'Cell-4'             , 'V'   , codeVCell  ,      0 ),
( ID_MEAS_VCT5           , 'Cell-5'             , 'V'   , codeVCell  ,      0 ),
( ID_MEAS_VCT6           , 'Cell-6'             , 'V'   , codeVCell  ,      0 ),
( ID_MEAS_VCT7           , 'Cell-7'             , 'V'   , codeVCell  ,      0 ),
( ID_MEAS_VCT8           , 'Cell-8'             , 'V'   , codeVCell  ,      0 ),
( ID_MEAS_VCT9           , 'Cell-9'             , 'V'   , codeVCell  ,      0 ),
( ID_MEAS_VCT10          , 'Cell-10'            , 'V'   , codeVCell  ,      0 ),
( ID_MEAS_VCT11          , 'Cell-11'            , 'V'   , codeVCell  ,      0 ),
( ID_MEAS_VCT12          , 'Cell-12'            , 'V'   , codeVCell  ,      0 ),
( ID_MEAS_VCT13          , 'Cell-13'            , 'V'   , codeVCell  ,      0 ),
( ID_MEAS_VCT14          , 'Cell-14'            , 'V'   , codeVCell  ,      0 ),
( ID_MEAS_VCUR           , 'Current'            , 'mV'  , codeCur    ,      0 ),
( ID_MEAS_ICTEMP         , 'T chip'             , 'C'   , codeICDegC ,      0 ),
( ID_MEAS_VAN0           , 'AN0'                , 'C'   , codeAnDegC ,      0 ),
( ID_MEAS_VAN1           , 'AN1'                , 'C'   , codeAnDegC ,      0 ),
( ID_MEAS_VAN2           , 'AN2'                , 'C'   , codeAnDegC ,      0 ),
( ID_MEAS_VAN3           , 'AN3'                , 'C'   , codeAnDegC ,      0 ),
( ID_MEAS_VAN4           , 'AN4'                , 'C'   , codeAnDegC ,      0 ),
( ID_MEAS_VAN5           , 'AN5'                , 'C'   , codeAnDegC ,      0 ),
( ID_MEAS_VAN6           , 'AN6'                , 'C'   , codeAnDegC ,      0 ),
( ID_MEAS_VBG_DIAG_ADC1A , 'Vbg Diag 1A'        , 'V'   , codeVCell  ,      0 ),
( ID_MEAS_VBG_DIAG_ADC1B , 'Vbg Diag 1B'        , 'V'   , codeVCell  ,      0 ),
( ID_COM_STATUS          , 'COM_STATUS'          , ''    , codeHex16  ,      0 ),
( ID_FAULT_STATUS1       , 'Fault Status 1'     , ''    , codeHex16  ,      0 ),
#( ID_FAULT1              , 'Fault 1'            , ''    , codeHex16  ,      0 ),
( ID_FAULT_STATUS2       , 'Fault Status 2'     , ''    , codeHex16  ,      0 ),
#( ID_FAULT2              , 'Fault 2'            , ''    , codeHex16  ,      0 ),
( ID_FAULT_STATUS3       , 'Fault Status 3'     , ''    , codeHex16  ,      0 ),
#( ID_FAULT3              , 'Fault 3'            , ''    , codeHex16  ,      0 ),
( ID_COMM_ERR       , 'COMM error'     , ''    , codeHex16  ,      0 ),
( ID_MEAS_SM1       , 'SM1'     , ''    , codeHex32  ,      0 ),
( ID_MEAS_SM2       , 'SM2'     , ''    , codeHex32  ,      0 ),
( ID_MEAS_SM3       , 'SM3'     , ''    , codeHex32  ,      0 ),
( ID_MEAS_SM4       , 'SM4'     , ''    , codeHex32  ,      0 ),
( ID_MEAS_SM5       , 'SM5'     , ''    , codeHex32  ,      0 ),
( ID_MEAS_SM6       , 'SM6'     , ''    , codeHex32  ,      0 ),
( ID_MEAS_SM7       , 'SM7'     , ''    , codeHex32  ,      0 ),
( ID_MEAS_SM36       , 'SM36'     , ''    , codeHex32  ,      0 ),
( ID_MEAS_SM40       , 'SM40'     , ''    , codeHex32  ,      0 ),
( ID_MEAS_SM41       , 'SM41'     , ''    , codeHex32  ,      0 ),
( ID_MEAS_SMX_AN       , 'SMx_AN'     , ''    , codeHex32  ,      0 ),
( ID_MEAS_SMX_OTHER      , 'SMx_other'     , ''    , codeHex32  ,      0 ),


]

showErrors = True


ERRORVIEW = [
(  0  , 'Ok'                 , '1'    , None       ,      0 ),
(  0  , 'Tx Error'           , '1'    , None       ,      0 ),
(  0  , 'No Resp.'           , '1'    , None       ,      0 ),
(  0  , 'Len Resp.'          , '1'    , None       ,      0 ),
(  0  , 'CRC Resp.'          , '1'    , None       ,      0 ),
]


class tableBMSview(QTableWidget):
    
    clearStatusClicked = pyqtSignal(int)
    reverseRtermClicked = pyqtSignal(int)
    statisticsClicked = pyqtSignal(int)
    appendText = pyqtSignal(str)
    
    
    rowHeight = 21
    NoCTs = 0
    NoClusters = 0
    CT_error = 10
    
    Diag_err_cell = range(32)
    Diag_err_text = range(32)
    Diag_err_title = range(32)
    Diag_err_display = 0

    def __init__(self, parent=None):
        QTableWidget.__init__(self)
        self.cellClicked.connect(self.myCellClicked)
        self.horizontalHeader().sortIndicatorChanged.connect(self.myHeaderClicked)
    def diagtotext(self, diag_item, diag_data):
        if (diag_item == ID_MEAS_SM1):
            for diag_t in range(14):
                if diag_data & (1<<diag_t):
                    pass
                else:
                    self.Diag_err_cell[self.Diag_err_display]=diag_t+1
                    self.Diag_err_text[self.Diag_err_display] = "UV(SM1)"
                    self.Diag_err_title[self.Diag_err_display] = "UV"
                    self.Diag_err_display = self.Diag_err_display + 1
            for diag_t in range(16, 30):
                if diag_data & (1<<diag_t):
                    pass
                else:
                    self.Diag_err_cell[self.Diag_err_display]=diag_t-15
                    self.Diag_err_text[self.Diag_err_display] = "OV"
                    self.Diag_err_title[self.Diag_err_display] = "OV"
                    self.Diag_err_display = self.Diag_err_display + 1

        elif (diag_item == ID_MEAS_SM2):
            for diag_t in range(14):
                if diag_data & (1<<diag_t):
                    self.Diag_err_cell[self.Diag_err_display]=diag_t+1
                    self.Diag_err_text[self.Diag_err_display] = "CT open(SM2)"
                    self.Diag_err_title[self.Diag_err_display] = "CT"
                    self.Diag_err_display = self.Diag_err_display + 1
        elif (diag_item == ID_MEAS_SM3):
            for diag_t in range(2, 14):
                if diag_data & (1<<diag_t):
                    pass
                else:
                    self.Diag_err_cell[self.Diag_err_display]=diag_t+1
                    self.Diag_err_text[self.Diag_err_display] = "Cell Voltage(SM3)"
                    self.Diag_err_title[self.Diag_err_display] = "CT"
                    self.Diag_err_display = self.Diag_err_display + 1
        elif (diag_item == ID_MEAS_SM4):
            for diag_t in range(14):
                if diag_data & (1<<diag_t):
                    self.Diag_err_cell[self.Diag_err_display]=diag_t
                    self.Diag_err_text[self.Diag_err_display] = "Cell Leakage(SM4)"
                    self.Diag_err_title[self.Diag_err_display] = "CT"
                    self.Diag_err_display = self.Diag_err_display + 1
        elif (diag_item == ID_MEAS_SM5):
            for diag_t in range(7):
                if diag_data & (1<<diag_t):
                    pass
                else:
                    self.Diag_err_cell[self.Diag_err_display]=diag_t
                    self.Diag_err_text[self.Diag_err_display] = "UT(SM5)"
                    self.Diag_err_title[self.Diag_err_display] = "UT"
                    self.Diag_err_display = self.Diag_err_display + 1
            for diag_t in range(8, 15):
                if diag_data & (1<<diag_t):
                    pass
                else:
                    self.Diag_err_cell[self.Diag_err_display]=diag_t - 8
                    self.Diag_err_text[self.Diag_err_display] = "OT(SM5)"
                    self.Diag_err_title[self.Diag_err_display] = "OT"
                    self.Diag_err_display = self.Diag_err_display + 1
        elif (diag_item == ID_MEAS_SM6):
            for diag_t in range(7):
                if diag_data & (1<<diag_t):
                    self.Diag_err_cell[self.Diag_err_display]=diag_t
                    self.Diag_err_text[self.Diag_err_display] = "Open Load(SM6)"
                    self.Diag_err_title[self.Diag_err_display] = "AN"
                    self.Diag_err_display = self.Diag_err_display + 1
        elif (diag_item == ID_MEAS_SM7):
            for diag_t in range(2):
                if diag_data & (1<<diag_t):
                    self.Diag_err_cell[self.Diag_err_display]=diag_t + 1
                    self.Diag_err_text[self.Diag_err_display] = "BandGap(SM7)"
                    self.Diag_err_title[self.Diag_err_display] = "BG"
                    self.Diag_err_display = self.Diag_err_display + 1
        elif (diag_item == ID_MEAS_SM36):
            for diag_t in range(1):
                if diag_data & (1<<diag_t):
                    self.Diag_err_cell[self.Diag_err_display]=diag_t + 1
                    self.Diag_err_text[self.Diag_err_display] = "Current Open(SM36)"
                    self.Diag_err_title[self.Diag_err_display] = "IOL"
                    self.Diag_err_display = self.Diag_err_display + 1
        elif (diag_item == ID_MEAS_SM40):
            for diag_t in range(14):
                if diag_data & (1<<diag_t):
                    self.Diag_err_cell[self.Diag_err_display]=diag_t + 1
                    self.Diag_err_text[self.Diag_err_display] = "Balancing Open(SM40)"
                    self.Diag_err_title[self.Diag_err_display] = "CT"
                    self.Diag_err_display = self.Diag_err_display + 1
                else:
                    pass
        elif (diag_item == ID_MEAS_SM41):
            for diag_t in range(14):
                if diag_data & (1<<diag_t):
                    self.Diag_err_cell[self.Diag_err_display]=diag_t + 1
                    self.Diag_err_text[self.Diag_err_display] = "Balancing Short(SM41)"
                    self.Diag_err_title[self.Diag_err_display] = "CT"
                    self.Diag_err_display = self.Diag_err_display + 1
        elif (diag_item == ID_MEAS_SMX_AN):
            for diag_t in range(8, 15):
                if diag_data & (1<<diag_t):
                    self.Diag_err_cell[self.Diag_err_display]=diag_t - 8
                    self.Diag_err_text[self.Diag_err_display] = "AN Short(SM_AN)"
                    self.Diag_err_title[self.Diag_err_display] = "SH"
                    self.Diag_err_display = self.Diag_err_display + 1
        elif (diag_item == ID_MEAS_SMX_OTHER):
            for diag_t in range(8, 26):
                if diag_data & (1<<diag_t):
                    self.Diag_err_cell[self.Diag_err_display]=diag_t
                    self.Diag_err_text[self.Diag_err_display] = "SM(SM_OTHR)"
                    self.Diag_err_title[self.Diag_err_display] = "SM"
                    self.Diag_err_display = self.Diag_err_display + 1
        
        
    def myCellClicked(self, row, col):
        rowStatus = 0
        cid = col+1
        if row==rowStatus:
            self.clearStatusClicked.emit(cid)
        elif row==rowStatus+1:
            self.reverseRtermClicked.emit(cid)
    def config(self, ct_error):
        self.CT_error = ct_error
    def myHeaderClicked(self, col): 
        cid = col+1
        self.statisticsClicked.emit(cid)

    def draw(self, clusters):
        """ draw(self, clusters)
        
        draws the table 
        - clusters defines the number of rows
        - cts no of cell terminals to display (6:BCC6 or 14:BCC14)
        """
        #need to redraw? 
        if self.NoClusters==clusters:
            return
            
        self.NoClusters = clusters
        self.clear()

        # row headers
        rowheader = ["Status"] + ["{:} [{:}]".format(name, unit) for _, name, unit, _, _ in BMSVIEW]
        if showErrors:
            rowheader = rowheader + ["{:} [{:}]".format(name, unit) for _, name, unit, _, _ in ERRORVIEW]
        
        self.setRowCount(len(rowheader))   
        self.setVerticalHeaderLabels(rowheader)
        self.setColumnCount(self.NoClusters)

        # column header
        hheaders = []
        for i in range(self.NoClusters):
            hheaders.append("CID-{:02d}".format(i+1))
        self.setHorizontalHeaderLabels(hheaders)


        # first row
        for c in range(self.columnCount()):
            self.setItem(0, c, QTableWidgetItem("---"))
            self.setColumnWidth(c,60)  
            self.item(0,c).setBackgroundColor(Qt.yellow)
            self.item(0,c).setFlags(Qt.ItemIsEnabled);
            self.item(0,c).setTextAlignment(Qt.AlignCenter)
#            self.item(0,c).setToolTip("CID-{} last error: {}".format(c+1, "none"))

        #self.lst.item(3,0).setBackgroundColor(Qt.red);
        #other columns    
        self.setRowHeight(0, self.rowHeight)

        for r in range(1, self.rowCount()):
            self.setRowHeight(r, self.rowHeight)
            self.setColumnWidth(r,70)        
            for c in range(self.columnCount()):
                self.setItem(r,c, QTableWidgetItem(""))
                self.item(r,c).setTextAlignment(Qt.AlignCenter)
                self.item(r,c).setFlags(Qt.ItemIsEnabled);
#                self.lst.item(r,c).setFlags(Qt::ItemIsSelectable|Qt::ItemIsEnabled);
        self.setAlternatingRowColors(1)
#        self.setFixedHeight(40+self.NoClusters*30)        # height of whole table
        
                
    def update(self, cid, guid, dataset, showMean=False):
        """ updates the table content
        
            :parameter:
            
            cid          cluster ID
            dataset      index ringbuffers containg the data
        """
        #status
    #            if bat.getErrors()==0:
    #            self.item(0, col).setText('Ok')         
        rowStatus = 0
        row = 1             # row = 0 is status line
        col = cid-1
        for idx, _, _, coder, _ in BMSVIEW:
            if idx==ID_GUID:
                self.item(row, col).setText(guid)
                row = row + 1
                continue
            
            value = dataset[idx].getValue()
            #no data available 
            if value==None:   
                self.item(row, col).setText("")
                row = row + 1
                continue
            if idx in ID_GROUP_DIAGNOSE:
                #self.appendText.emit("Diag CID-{:02}:SM{:} Error {:}mV (> {:}mV)".format(cid,  row-2,  val, self.CT_error))
                u32Diag_data = int(value)
                #print u32Diag_data
                if(u32Diag_data!=0):
                    tableBMSview.diagtotext(self, idx,u32Diag_data)
                    while self.Diag_err_display > 0:
                        self.appendText.emit("Diag CID-{:02} {:}{:} : Error {:} ".format(cid, self.Diag_err_title[self.Diag_err_display-1], self.Diag_err_cell[self.Diag_err_display-1],  self.Diag_err_text[self.Diag_err_display-1] ))
                        self.Diag_err_display = self.Diag_err_display -1;
                #continue
            if showMean and idx in ID_GROUP_SHOWMEAN:
                stat = dataset[idx].getStat() 
                mean = stat['mean']
                max = stat['max']
                min = stat['min']
                if (max - mean) > (mean - min):
                    offset = max - mean
                else:
                    offset = mean - min
                text = coder(decode, value, form   = '{:2.3f}')
                a = coder(decode, offset, form   = '{:.3f}')
                val = int(float(a) * 1000)
                text += '({:d})'.format(val)
                #if CT error > 10, mark status to red for indication
                if (idx >=  ID_MEAS_VCT1) \
                    & (idx <=  ID_MEAS_VCT14):
                    if (val > self.CT_error):
                        self.item(rowStatus, col).setBackgroundColor(Qt.darkGreen)
                        self.item(row, col).setTextColor(Qt.red)
                        self.appendText.emit("CID-{:02}:CELL{:} Error {:}mV (> {:}mV)".format(cid,  row-2,  val, self.CT_error))
                        #print cid
                    else:
                        self.item(row, col).setTextColor(Qt.black)
                        
            else:
                text = coder(decode, value)
                
            if idx == ID_BCCSTATE:
                if text=="0x0000":
                    self.item(row, col).setBackgroundColor(Qt.green)
                else:
                    self.item(row, col).setBackgroundColor(Qt.red)

            # indicate change by toggling a suffix 
#             suffix = '.'
#             nosuffix = ' '
#             t = self.item(row, col).text()
#             if len(t)>1 and t[-1]==suffix: 
#                 self.item(row, col).setText(text+nosuffix)
#             else:
#                 self.item(row, col).setText(text+suffix)
                    
            # no indication
            self.item(row, col).setText(text)
            row = row + 1


    def updateStatus(self, cid, lastStatus, lastError):
        col = cid-1                                                 # count from 0...        
        rowStatus = 0
        # test is last Status
        self.item(rowStatus, col).setText('{0}'.format(ErrorMsg[lastStatus]))
        #color is last Error
        self.item(rowStatus, col).setBackgroundColor(ErrorColor[lastError])

 #init Rterm Connect Status
    def RtermStatus(self, cid, lastStatus, lastError):
        col = cid - 1                                               # count from 0...
        RStatus = 1
        # test is last Status
        self.item(RStatus, col).setText('{0}'.format(RtermStatus[lastStatus]))
     #   self.item(RStatus, col).setText('sss')
        self.item(RStatus, col).setBackgroundColor(RtermColor[lastError])


#     def clearErrorStatus(self, c):
#         self.item(0,c).setBackgroundColor(Qt.green)

            
    def updateCommCounter(self, cid, commCounters):
        if not showErrors:
            return

        col = cid-1                                                 # count from 0...        
        row = len(BMSVIEW)+1
        i = 0
        
#        print col, r, i
        # update Error Counters
        for counter in commCounters:
            try:
                self.item(row,col).setText('{:}'.format(counter))
            except AttributeError:
                #ignore
                dummy = 0     
                print "dummy"
            row = row +1
            i = i+1
            




# ------------------------------------------------------------------------------
#for test purposes only
class TestApp(QMainWindow):
    interface = ""

 
    def __init__(self, parent = None):
        QMainWindow.__init__(self, parent)
        self.create_main_frame()
#        self.btnStart.clicked.connect(self.commands)


    def create_main_frame(self):
        self.main_frame = QWidget()
        
        self.viewBMS = tableBMSview()
        self.setCentralWidget(self.viewBMS)
        self.resize(1280,600)


    #def commands(self):
        #sender = self.sender()
        ## -------------------------------
        #if sender == self.btnStart:
        ## -------------------------------
            #print "start"
            #print self.winMeasurements.getTest()
            #if self.winMeasurements.getTest()=="BCC6":
                #self.winMeasurements.close()
                #self.winMeasurements = TableRegView(BCC14_V3_MEAS, "BCC14")
            #else:
                #self.winMeasurements.close()
                #self.winMeasurements = TableRegView(BCC6_V1_MEAS, "BCC6")
                

            #self.wx.addWidget(self.winMeasurements, row=1, col=0)

##            self.winRegView.update()
           
            #self.winBitRegView.setDisabled(True)


# ----------------------------------------------------------------------------- 
def main():
    app = QApplication(sys.argv)
    form = TestApp()
    form.show()
    app.exec_()

if __name__ == "__main__":
    main()
