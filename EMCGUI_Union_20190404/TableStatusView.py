
import sys
from PyQt4.QtCore import *                              #@UnusedWildImport (Eclipse)
from PyQt4.QtGui import *                               #@UnusedWildImport (Eclipse)
#from MySerial import *                                  #@UnusedWildImport (Eclipse)
#from batpack import *                                   #@UnusedWildImport (Eclipse)
from MC3377x import *                                   #@UnusedWildImport (Eclipse)
from CommId import *                                    #@UnusedWildImport (Eclipse)
from MC3377x_RegMap import *
from HtmlColors import *

from uiEMCGuiMain import Ui_MainWindow
#import numpy as np


STATUSVIEW = [
    #  ID                ,    name              ,  decoder   , RegAddress
#( ID_BCCSTATE            , 'Fault'              , ''    , codeHex16  ,      0 ),
( ID_CELL_OV             , 'CELL_OV_FLT'   ,  None       ,      0x09 ),
( ID_CELL_UV             , 'CELL_UV_FLT'   ,  None       ,      0x0A ),
( ID_CB_OPEN_FAULT       , 'CB_OPEN_FLT'   ,  None       ,      0x1A ),
( ID_CB_SHORT_FAULT      , 'CB_SHORT_FLT'  ,  None       ,      0x1B ),
( ID_CB_DRV_STATUS       , 'CB_DRV_STS'    ,  None       ,      0x1C ),
( ID_GPIO_STS            , 'GPIO_STS'       , None        ,      0x1F ),
( ID_AN_OT_UT            , 'AN_OT_UT_FLT'  ,  None       ,      0x20 ),
( ID_GPIO_SHORT_OPEN     , 'GPIO_SHORT_ANx_OPEN_STS', None,   0x21 ),
( ID_I_STATUS            , 'I_STATUS'       ,  None       ,      0x22 ),
( ID_COM_STATUS          , 'COM_STATUS'    ,  None        ,      0x23 ),
( ID_FAULT_STATUS1       , 'FAULT1_STATUS' ,  None       ,      0x24 ),
( ID_FAULT_STATUS2       , 'FAULT2_STATUS' ,  None       ,      0x25 ),
( ID_FAULT_STATUS3       , 'FAULT3_STATUS' ,  None       ,      0x26 ),
( ID_MEAS_ISENSE2        , 'MEAS_ISENSE2'   , None       ,      0x31 ),
( ID_SILIVON_REV         , 'SILICON_REV'   ,  None       ,      0x6B ),
]

showErrors = True
BITNUM = 16

class TableStatusView(QTableWidget):

    BitClicked = pyqtSignal(int,int)
    NoClusters = 0
    rowHeight = 27
    StatusRegMap = MC3377x_STAT

    #bit colors  color off,  color on
    bcolor = [ ( QColor(DarkGreen)  , QColor(LimeGreen)   ),   # 0 - r/w
               ( QColor(SteelBlue)  , QColor(DeepskyBlue) ),   # 1 -  r
               ( QColor(FireBrick)  , QColor(Red)         ),   # 2 - w
               ( QColor(Grey)       , QColor(Grey)        ),   # 3 - not implemented
               ( QColor(DarkOrange) , QColor(Yellow)      ) ]  # 4 - w1

    def __init__(self, parent = None):
        QTableWidget.__init__(self)
        self.cellClicked.connect(self.BitClick)

    def BitClick(self, row, col):
        self.BitClicked.emit(row, col)

    def draw(self, clusters):

        if self.NoClusters == clusters:
            return

        self.NoClusters = clusters
      #  self.configui.table_Configview.clear()
        self.clear()
        # row headers
        rowheader = ["{:}".format(name) for _, name, _, _ in STATUSVIEW]
  #      if showErrors:
  #          rowheader = rowheader + ["{:}".format(name) for _, name, _, _ in CONFIGVIEW]

        self.setRowCount(len(rowheader))
        self.setVerticalHeaderLabels(rowheader)
        self.setColumnCount(BITNUM)

        # column header
        hheaders = []
        for i in range(BITNUM):
            hheaders.append("BIT-{:02d}".format(15-i))
        self.setHorizontalHeaderLabels(hheaders)

        # first row
        for r in range(0,self.rowCount()):
            self.setRowHeight(r, self.rowHeight)
            self.setColumnWidth(r, 70)
            for c in range(0,self.columnCount()):
                self.setItem(r, c, QTableWidgetItem(self.StatusRegMap[r][c+1][0]))
                self.item(r, c).setToolTip(self.StatusRegMap[r][c+1][0])
                self.item(r, c).setTextAlignment(Qt.AlignCenter)
                self.item(r, c).setFlags(Qt.ItemIsEnabled);
                color = self.StatusRegMap[r][c+1][1]
                self.item(r, c).setBackgroundColor(self.bcolor[color][0])  # bit type

    def update(self,dataset):
        self.index = [x[0] for x in STATUSVIEW]
        for n in self.index:
            row = self.index.index(n)
            data = dataset[n].getValue()
            if data == None:
                data = 0
            data = int(data)
            bit15to0 = range(15, -1, -1)
            # update bits
            for b in bit15to0:
                bitstate = (data & (2 ** b) > 0)
                color = self.StatusRegMap[row][16 - b][1]
                self.item(row, 16 - b -1).setBackgroundColor(self.bcolor[color][bitstate])  # bit type

    #                self.lst.item(r,c).setFlags(Qt::ItemIsSelectable|Qt::ItemIsEnabled);
 #   self.setAlternatingRowColors(1)
#        self.setFixedHeight(40+self.NoClusters*30)        # height of whole table