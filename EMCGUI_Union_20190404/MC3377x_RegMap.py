# -*- coding: utf-8 -*-
"""
// ----------------------------------------------------------------------------
//  Copyright (c) 2015, Freescale Semiconductor, Inc.
//  All rights reserved.
//
//  Redistribution and use in source and binary forms, with or without modification,
//  are permitted provided that the following conditions are met:
//
//  o Redistributions of source code must retain the above copyright notice, this list
//    of conditions and the following disclaimer.
//
//  o Redistributions in binary form must reproduce the above copyright notice, this
//    list of conditions and the following disclaimer in the documentation and/or
//    other materials provided with the distribution.
//
//  o Neither the name of Freescale Semiconductor, Inc. nor the names of its
//    contributors may be used to endorse or promote products derived from this
//    software without specific prior written permission.
//
//  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
//  ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
//  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
//  DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR
//  ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
//  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
//  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
//  ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
//  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
//  SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
// ----------------------------------------------------------------------------
"""
from MC3377x import *  # import MC3377x parameters
from CommId import *

# ------------------------------------------------------------------------------
# bit types
rw = 0  # read/write
r = 1  # read
w = 2  # write
res = 3  # res
w1 = 4  # write once
# ------------------------------------------------------------------------------
# register informations
# ------------------------------------------------------------------------------
# ---BCC14 V3 Measurements
MC33771p4_MEAS = [
    # ID                     , name                 ,  unit ,  decoder   , physical
    (ID_MEAS_VCUR, 'MEAS_ISENSE', 'mV', codeVCur, 0),
    (ID_MEAS_VPWR, 'MEAS_STACK', 'V', codeVPwr, 0),
    (ID_MEAS_VCT1, 'MEAS_CELL1', 'V', codeVCell, 0),
    (ID_MEAS_VCT2, 'MEAS_CELL2', 'V', codeVCell, 0),
    (ID_MEAS_VCT3, 'MEAS_CELL3', 'V', codeVCell, 0),
    (ID_MEAS_VCT4, 'MEAS_CELL4', 'V', codeVCell, 0),
    (ID_MEAS_VCT5, 'MEAS_CELL5', 'V', codeVCell, 0),
    (ID_MEAS_VCT6, 'MEAS_CELL6', 'V', codeVCell, 0),
    (ID_MEAS_VCT7, 'MEAS_CELL7', 'V', codeVCell, 0),
    (ID_MEAS_VCT8, 'MEAS_CELL8', 'V', codeVCell, 0),
    (ID_MEAS_VCT9, 'MEAS_CELL9', 'V', codeVCell, 0),
    (ID_MEAS_VCT10, 'MEAS_CELL10', 'V', codeVCell, 0),
    (ID_MEAS_VCT11, 'MEAS_CELL11', 'V', codeVCell, 0),
    (ID_MEAS_VCT12, 'MEAS_CELL12', 'V', codeVCell, 0),
    (ID_MEAS_VCT13, 'MEAS_CELL13', 'V', codeVCell, 0),
    (ID_MEAS_VCT14, 'MEAS_CELL14', 'V', codeVCell, 0),
    (ID_MEAS_VAN0, 'MEAS_AN0', 'degC', codeAnDegC, 0),
    (ID_MEAS_VAN1, 'MEAS_AN1', 'degC', codeAnDegC, 0),
    (ID_MEAS_VAN2, 'MEAS_AN2', 'degC', codeAnDegC, 0),
    (ID_MEAS_VAN3, 'MEAS_AN3', 'degC', codeAnDegC, 0),
    (ID_MEAS_VAN4, 'MEAS_AN4', 'degC', codeAnDegC, 0),
    (ID_MEAS_VAN5, 'MEAS_AN5', 'degC', codeAnDegC, 0),
    (ID_MEAS_VAN6, 'MEAS_AN6', 'degC', codeAnDegC, 0),
    (ID_MEAS_ICTEMP, 'MEAS_IC_TEMP', 'degC', codeICDegC, 0),
    (ID_MEAS_VBG_DIAG_ADC1A, 'MEAS_VBG_DIAG_ADC1A', 'V', codeVCell, 0),
    (ID_MEAS_VBG_DIAG_ADC1B, 'MEAS_VBG_DIAG_ADC1B', 'V', codeVCell, 0),
    (ID_CC_NB_SAMPLE, 'CC_NB_SAMPLES', '-', codeHex16, 0),
    (ID_COULOMB_CNT, 'COULOMB_CNT', '-', codeHex32, 0),
    (ID_SILIVON_REV, 'SILICON_REV', '-', codeHex16, 0),
    (ID_DED_HAMMING_CNT, 'DED_HAMMING_CNT', '-', codeHex32, 0),
]
# ------------------------------------------------------------------------------
# ---BCC14 V3 Thresholds
MC33771p4_THRES = [
    # ID                     , name                 ,  unit ,  decoder   , physical (add, bstart, blen)
    (ID_TH_ALL_CT_OV, 'TH_ALL_CT_OV', 'V', codeThVolt, (0x4B, 8, 8)),
    (ID_TH_ALL_CT_UV, 'TH_ALL_CT_UV', 'V', codeThVolt, (0x4B, 0, 8)),
    (ID_TH_CT1_OV, 'TH_CT1_OV', 'V', codeThVolt, (0x59, 8, 8)),
    (ID_TH_CT1_UV, 'TH_CT1_UV', 'V', codeThVolt, (0x59, 0, 8)),
    (ID_TH_CT2_OV, 'TH_CT2_OV', 'V', codeThVolt, (0x58, 8, 8)),
    (ID_TH_CT2_UV, 'TH_CT2_UV', 'V', codeThVolt, (0x58, 0, 8)),
    (ID_TH_CT3_OV, 'TH_CT3_OV', 'V', codeThVolt, (0x57, 8, 8)),
    (ID_TH_CT3_UV, 'TH_CT3_UV', 'V', codeThVolt, (0x57, 0, 8)),
    (ID_TH_CT4_OV, 'TH_CT4_OV', 'V', codeThVolt, (0x56, 8, 8)),
    (ID_TH_CT4_UV, 'TH_CT4_UV', 'V', codeThVolt, (0x56, 0, 8)),
    (ID_TH_CT5_OV, 'TH_CT5_OV', 'V', codeThVolt, (0x55, 8, 8)),
    (ID_TH_CT5_UV, 'TH_CT5_UV', 'V', codeThVolt, (0x55, 0, 8)),
    (ID_TH_CT6_OV, 'TH_CT6_OV', 'V', codeThVolt, (0x54, 8, 8)),
    (ID_TH_CT6_UV, 'TH_CT6_UV', 'V', codeThVolt, (0x54, 0, 8)),
    (ID_TH_CT7_OV, 'TH_CT7_OV', 'V', codeThVolt, (0x53, 8, 8)),
    (ID_TH_CT7_UV, 'TH_CT7_UV', 'V', codeThVolt, (0x53, 0, 8)),
    (ID_TH_CT8_OV, 'TH_CT8_OV', 'V', codeThVolt, (0x52, 8, 8)),
    (ID_TH_CT8_UV, 'TH_CT8_UV', 'V', codeThVolt, (0x52, 0, 8)),
    (ID_TH_CT9_OV, 'TH_CT9_OV', 'V', codeThVolt, (0x51, 8, 8)),
    (ID_TH_CT9_UV, 'TH_CT9_UV', 'V', codeThVolt, (0x51, 0, 8)),
    (ID_TH_CT10_OV, 'TH_CT10_OV', 'V', codeThVolt, (0x50, 8, 8)),
    (ID_TH_CT10_UV, 'TH_CT10_UV', 'V', codeThVolt, (0x50, 0, 8)),
    (ID_TH_CT11_OV, 'TH_CT11_OV', 'V', codeThVolt, (0x4f, 8, 8)),
    (ID_TH_CT11_UV, 'TH_CT11_UV', 'V', codeThVolt, (0x4f, 0, 8)),
    (ID_TH_CT12_OV, 'TH_CT12_OV', 'V', codeThVolt, (0x4e, 8, 8)),
    (ID_TH_CT12_UV, 'TH_CT12_UV', 'V', codeThVolt, (0x4e, 0, 8)),
    (ID_TH_CT13_OV, 'TH_CT13_OV', 'V', codeThVolt, (0x4d, 8, 8)),
    (ID_TH_CT13_UV, 'TH_CT13_UV', 'V', codeThVolt, (0x4d, 0, 8)),
    (ID_TH_CT14_OV, 'TH_CT14_OV', 'V', codeThVolt, (0x4c, 8, 8)),
    (ID_TH_CT14_UV, 'TH_CT14_UV', 'V', codeThVolt, (0x4c, 0, 8)),
    (ID_TH_AN0_OT, 'TH_AN0_OT', 'degC', codeThDegC, (0x60, 0, 10)),
    (ID_TH_AN1_OT, 'TH_AN1_OT', 'degC', codeThDegC, (0x5F, 0, 10)),
    (ID_TH_AN2_OT, 'TH_AN2_OT', 'degC', codeThDegC, (0x5E, 0, 10)),
    (ID_TH_AN3_OT, 'TH_AN3_OT', 'degC', codeThDegC, (0x5D, 0, 10)),
    (ID_TH_AN4_OT, 'TH_AN4_OT', 'degC', codeThDegC, (0x5C, 0, 10)),
    (ID_TH_AN5_OT, 'TH_AN5_OT', 'degC', codeThDegC, (0x5B, 0, 10)),
    (ID_TH_AN6_OT, 'TH_AN6_OT', 'degC', codeThDegC, (0x5A, 0, 10)),
    (ID_TH_AN0_UT, 'TH_AN0_UT', 'degC', codeThDegC, (0x67, 0, 10)),
    (ID_TH_AN1_UT, 'TH_AN1_UT', 'degC', codeThDegC, (0x66, 0, 10)),
    (ID_TH_AN2_UT, 'TH_AN2_UT', 'degC', codeThDegC, (0x65, 0, 10)),
    (ID_TH_AN3_UT, 'TH_AN3_UT', 'degC', codeThDegC, (0x64, 0, 10)),
    (ID_TH_AN4_UT, 'TH_AN4_UT', 'degC', codeThDegC, (0x63, 0, 10)),
    (ID_TH_AN5_UT, 'TH_AN5_UT', 'degC', codeThDegC, (0x62, 0, 10)),
    (ID_TH_AN6_UT, 'TH_AN6_UT', 'degC', codeThDegC, (0x61, 0, 10)),
    (ID_TH_ISENSE_OC, 'TH_ISENSE_OC', '-', codeHex16, (0x68, 0, 12)),
    (ID_TH_COULOMB_CNT, 'TH_COULOMB_CNT', '-', codeHex32, (0x69, 0, 32)),
]
# ------------------------------------------------------------------------------
# ---BCC14 V3 Status Bits
MC3377x_STAT = [
    #     ID   name          , physical  add
    ((ID_CELL_OV, 'CELL_OV', 0x09),  # reg
     ("", res),  # bit15
     ("", res),  # bit14
     ("CT14_OV", w),  # bit13
     ("CT13_OV", w),  # bit12
     ("CT12_OV", w),  # bit11
     ("CT11_OV", w),  # bit10
     ("CT10_OV", w),  # bit9
     ("CT9_OV", w),  # bit8
     ("CT8_OV", w),  # bit7
     ("CT7_OV", w),  # bit6
     ("CT6_OV", w),  # bit5
     ("CT5_OV", w),  # bit4
     ("CT4_OV", w),  # bit3
     ("CT3_OV", w),  # bit2
     ("CT2_OV", w),  # bit1
     ("CT1_OV", w)),  # bit0

    ((ID_CELL_UV, 'CELL_UV', 0x0A),  # reg
     ("", res),  # bit15
     ("", res),  # bit14
     ("CT14_UV", w),  # bit13
     ("CT13_UV", w),  # bit12
     ("CT12_UV", w),  # bit11
     ("CT11_UV", w),  # bit10
     ("CT10_UV", w),  # bit9
     ("CT9_UV", w),  # bit8
     ("CT8_UV", w),  # bit7
     ("CT7_UV", w),  # bit6
     ("CT6_UV", w),  # bit5
     ("CT5_UV", w),  # bit4
     ("CT4_UV", w),  # bit3
     ("CT3_UV", w),  # bit2
     ("CT2_UV", w),  # bit1
     ("CT1_UV", w)),  # bit0

    ((ID_CB_OPEN_FAULT, 'CB_Open_Flt', 0x1A),  # reg
     ("", res),  # bit15
     ("", res),  # bit14
     ("CB14_Open", w),  # bit13
     ("CB13_Open", w),  # bit12
     ("CB12_Open", w),  # bit11
     ("CB11_Open", w),  # bit10
     ("CB10_Open", w),  # bit9
     ("CB9_Open", w),  # bit8
     ("CB8_Open", w),  # bit7
     ("CB7_Open", w),  # bit6
     ("CB6_Open", w),  # bit5
     ("CB5_Open", w),  # bit4
     ("CB4_Open", w),  # bit3
     ("CB3_Open", w),  # bit2
     ("CB2_Open", w),  # bit1
     ("CB1_Open", w)),  # bit0

    ((ID_CB_SHORT_FAULT, 'CB_Short_Flt', 0x1B),  # reg
     ("", res),  # bit15
     ("", res),  # bit14
     ("CB14_Short", w),  # bit13
     ("CB13_Short", w),  # bit12
     ("CB12_Short", w),  # bit11
     ("CB11_Short", w),  # bit10
     ("CB10_Short", w),  # bit9
     ("CB9_Short", w),  # bit8
     ("CB8_Short", w),  # bit7
     ("CB7_Short", w),  # bit6
     ("CB6_Short", w),  # bit5
     ("CB5_Short", w),  # bit4
     ("CB4_Short", w),  # bit3
     ("CB3_Short", w),  # bit2
     ("CB2_Short", w),  # bit1
     ("CB1_Short", w)),  # bit0

    ((ID_CB_DRV_STATUS, 'CB_DrvStatus', 0x1C),  # reg
     ("", res),  # bit15
     ("", res),  # bit14
     ("CB14_STS", r),  # bit13
     ("CB13_STS", r),  # bit12
     ("CB12_STS", r),  # bit11
     ("CB11_STS", r),  # bit10
     ("CB10_STS", r),  # bit9
     ("CB9_STS", r),  # bit8
     ("CB8_STS", r),  # bit7
     ("CB7_STS", r),  # bit6
     ("CB6_STS", r),  # bit5
     ("CB5_STS", r),  # bit4
     ("CB4_STS", r),  # bit3
     ("CB3_STS", r),  # bit2
     ("CB2_STS", r),  # bit1
     ("CB1_STS", r)),  # bit0

    ((ID_GPIO_STS, 'GPIO_STS', 0x1F),  # reg
     ("", res),  # bit15
     ("GPIO6_H", w),  # bit14
     ("GPIO5_H", w),  # bit13
     ("GPIO4_H", w),  # bit12
     ("GPIO3_H", w),  # bit11
     ("GPIO2_H", w),  # bit10
     ("GPIO1_H", w),  # bit9
     ("GPIO0_H", w),  # bit8
     ("", res),  # bit7
     ("GPIO6_ST", r),  # bit6
     ("GPIO5_ST", r),  # bit5
     ("GPIO4_ST", r),  # bit4
     ("GPIO3_ST", r),  # bit3
     ("GPIO2_ST", r),  # bit2
     ("GPIO1_ST", r),  # bit1
     ("GPIO0_ST", r)),  # bit0

    ((ID_AN_OT_UT, 'AN_OT_UT', 0x20),  # reg
     ("", res),  # bit15
     ("AN6_OT", w),  # bit14
     ("AN5_OT", w),  # bit13
     ("AN4_OT", w),  # bit12
     ("AN3_OT", w),  # bit11
     ("AN2_OT", w),  # bit10
     ("AN1_OT", w),  # bit9
     ("AN0_OT", w),  # bit8
     ("", res),  # bit7
     ("AN6_UT", w),  # bit6
     ("AN5_UT", w),  # bit5
     ("AN4_UT", w),  # bit4
     ("AN3_UT", w),  # bit3
     ("AN2_UT", w),  # bit2
     ("AN1_UT", w),  # bit1
     ("AN0_UT", w)),  # bit0

    ((ID_GPIO_SHORT_OPEN, 'GPIO_Short_Anx_Open_Sts', 0x21),  # reg
     ("", res),  # bit15
     ("GPIO6_SH", r),  # bit14
     ("GPIO5_SH", r),  # bit13
     ("GPIO4_SH", r),  # bit12
     ("GPIO3_SH", r),  # bit11
     ("GPIO2_SH", r),  # bit10
     ("GPIO1_SH", r),  # bit9
     ("GPIO0_SH", r),  # bit8
     ("", res),  # bit7
     ("AN6_OPEN", r),  # bit6
     ("AN5_OPEN", r),  # bit5
     ("AN4_OPEN", r),  # bit4
     ("AN3_OPEN", r),  # bit3
     ("AN2_OPEN", r),  # bit2
     ("AN1_OPEN", r),  # bit1
     ("AN0_OPEN", r)),  # bit0

    ((ID_I_STATUS, 'I_STATUS', 0x22),  # reg
     ("PGA_DAC", r),  # bit15
     ("PGA_DAC", r),  # bit14
     ("PGA_DAC", r),  # bit13
     ("PGA_DAC", r),  # bit12
     ("PGA_DAC", r),  # bit11
     ("PGA_DAC", r),  # bit10
     ("PGA_DAC", r),  # bit9
     ("PGA_DAC", r),  # bit8
     ("", res),  # bit7
     ("", res),  # bit6
     ("", res),  # bit5
     ("", res),  # bit4
     ("", res),  # bit3
     ("", res),  # bit2
     ("", res),  # bit1
     ("", res)),  # bit0

    ((ID_COM_STATUS, 'COM_STATUS', 0x23),  # reg
     ("COM_ERR_COUNT", r),  # bit15
     ("COM_ERR_COUNT", r),  # bit14
     ("COM_ERR_COUNT", r),  # bit13
     ("COM_ERR_COUNT", r),  # bit12
     ("COM_ERR_COUNT", r),  # bit11
     ("COM_ERR_COUNT", r),  # bit10
     ("COM_ERR_COUNT", r),  # bit9
     ("COM_ERR_COUNT", r),  # bit8
     ("", res),  # bit7
     ("", res),  # bit6
     ("", res),  # bit5
     ("", res),  # bit4
     ("", res),  # bit3
     ("", res),  # bit2
     ("", res),  # bit1
     ("", res)),  # bit0

    ((ID_FAULT_STATUS1, 'FaultStatus1', 0x24),  # reg
     ("POR", w),
     ("RESET", w),
     ("COM_ERR_OVR", w),
     ("VPWR_OV", w),
     ("VPWR_LV", w),
     ("COM_LOSS", w),
     ("COM_ERR", w),
     ("CSB_WUP", w),
     ("GPIO0_WUP", w),
     ("I2C_ERR", w),
     ("ISENSE_OL", w),
     ("ISENSE_OC", w),
     ("AN_OT", r),
     ("AN_UT", r),
     ("CT_OV", r),
     ("CT_UV", r)),

    ((ID_FAULT_STATUS2, 'FaultStatus2', 0x25),  # reg
     ("VCOM_OV", w),  # bit15
     ("VCOM_UV", w),  # bit14
     ("VANA_OV", w),  # bit13
     ("VANA_UV", w),  # bit12
     ("ADC1_B", w),  # bit11
     ("ADC1_A", w),  # bit10
     ("GND_LOSS", w),  # bit9
     ("IC_TSD", w),  # bit8
     ("IDLE_MODE", w),  # bit7
     ("AN_OPEN", w),  # bit6
     ("GPIO_SHORT", w),  # bit5
     ("CB_SHORT", w),  # bit4
     ("CB_OPEN", w),  # bit3
     ("OSC_ERR", w),  # bit2
     ("DED_ERR", w),  # bit1
     ("FUSE_ERR", w)),  # bit0

    ((ID_FAULT_STATUS3, 'FaultStatus3', 0x26),  # reg
     ("CC_OVR", w),  # bit15
     ("DIAG_TO", w),  # bit14
     ("EOT_CB14", w),  # bit13
     ("EOT_CB13", w),  # bit12
     ("EOT_CB12", w),  # bit11
     ("EOT_CB11", w),  # bit10
     ("EOT_CB10", w),  # bit9
     ("EOT_CB9", w),  # bit8
     ("EOT_CB8", w),  # bit7
     ("EOT_CB7", w),  # bit6
     ("EOT_CB6", w),  # bit5
     ("EOT_CB5", w),  # bit4
     ("EOT_CB4", w),  # bit3
     ("EOT_CB3", w),  # bit2
     ("EOT_CB2", w),  # bit1
     ("EOT_CB1", w)),  # bit0

    ((ID_MEAS_ISENSE2, 'MeasIsense2', 0x31),  # reg
     ("DataRdy", r),  # bit15
     ("", res),  # bit14
     ("", res),  # bit13
     ("", res),  # bit12
     ("", res),  # bit11
     ("", res),  # bit10
     ("PGAGain", r),  # bit9
     ("PGAGain", r),  # bit8
     ("ADC2_SAT", r),  # bit7
     ("PGAChange", r),  # bit6
     ("", res),  # bit5
     ("", res),  # bit4
     ("MeasI3", r),  # bit3
     ("MeasI2", r),  # bit2
     ("MeasI1", r),  # bit1
     ("MeasI0", r)),  # bit0

    ((ID_SILIVON_REV, 'SILICON_REV', 0x6B),  # reg
     ("", res),  # bit15
     ("", res),  # bit14
     ("", res),  # bit13
     ("", res),  # bit12
     ("", res),  # bit11
     ("", res),  # bit10
     ("", res),  # bit9
     ("", res),  # bit8
     ("", res),  # bit7
     ("", res),  # bit6
     ("FREV", r),  # bit5
     ("FREV", r),  # bit4
     ("FREV", r),  # bit3
     ("MREV", r),  # bit2
     ("MREV", r),  # bit1
     ("MREV", r)),  # bit0
]
# ------------------------------------------------------------------------------
# ---BCC14 V3 Control Bits
MC3377x_CTRL = [
    #     ID   name          , physical  add
    ((ID_INIT, 'INIT', 0x01),  # reg
     ("", res),  # bit15
     ("", res),  # bit14
     ("", res),  # bit13
     ("", res),  # bit12
     ("", res),  # bit11
     ("", res),  # bit10
     ("", res),  # bit9
     ("", res),  # bit8
     ("", res),  # bit7
     ("", res),  # bit6
     ("RTERM", rw),  # bit5
     ("BUSSW", rw),  # bit4
     ("CID3", w1),  # bit3
     ("CID2", w1),  # bit2
     ("CID1", w1),  # bit1
     ("CID0", w1)),  # bit0

    ((ID_SYS_CFG_GLOBAL, 'SYS_CfgGlobal', 0x02),  # reg
     ("", res),  # bit15
     ("", res),  # bit14
     ("", res),  # bit13
     ("", res),  # bit12
     ("", res),  # bit11
     ("", res),  # bit10
     ("", res),  # bit9
     ("", res),  # bit8
     ("", res),  # bit7
     ("", res),  # bit6
     ("", res),  # bit5
     ("", res),  # bit4
     ("", res),  # bit3
     ("", res),  # bit2
     ("", res),  # bit1
     ("GO2SLEEP", w)),  # bit0

    ((ID_SYS_CFG1, 'SYS_CFG1', 0x03),  # reg
     ("CYCLIC_TIMER", rw),  # bit15
     ("CYCLIC_TIMER", rw),  # bit14
     ("CYCLIC_TIMER", rw),  # bit13
     ("DiagTimeout", rw),  # bit12
     ("DiagTimeout", rw),  # bit11
     ("DiagTimeout", rw),  # bit10
     ("IMeasEn", rw),  # bit9
     ("CBAutoPause", rw),  # bit8
     ("CBDrvEn", rw),  # bit7
     ("GO2DIAG/DIAGST", rw),  # bit6
     ("CBManuPause", rw),  # bit5
     ("SOFT_RST", w),  # bit4
     ("FaultWave", rw),  # bit3
     ("Wave_DC", rw),  # bit2
     ("Wave_DC", rw),  # bit1
     ("Reserved_0", res)),  # bit0

    ((ID_SYS_CFG2, 'SYS_CFG2', 0x04),  # reg
     ("", res),  # bit15
     ("", res),  # bit14
     ("", res),  # bit13
     ("PreviousState", r),  # bit12
     ("PreviousState", r),  # bit11
     ("PreviousState", r),  # bit10
     ("FLT_RST_CFG", rw),  # bit9
     ("FLT_RST_CFG", rw),  # bit8
     ("FLT_RST_CFG", rw),  # bit7
     ("FLT_RST_CFG", rw),  # bit6
     ("TimeoutComm", rw),  # bit5
     ("TimeoutComm", rw),  # bit4
     ("", res),  # bit3
     ("", res),  # bit2
     ("NumbOdd", rw),  # bit1
     ("HammEncode", rw)),  # bit0

    ((ID_SYS_DIAG, 'SYS_DIAG', 0x05),  # reg
     ("FaultDiag", rw),  # bit15
     ("", res),  # bit14
     ("", res),  # bit13
     ("IMux", rw),  # bit12
     ("IMux", rw),  # bit11
     ("IsenseOLDiag", rw),  # bit10
     ("ANxOLDiag", rw),  # bit9
     ("ANxTempDiag", rw),  # bit8
     ("DaDiag", rw),  # bit7
     ("Polarity", rw),  # bit6
     ("CTLeakDiag", rw),  # bit5
     ("CT_OV_UV", rw),  # bit4
     ("CT_OL_Odd", rw),  # bit3
     ("CT_OL_Even", rw),  # bit2
     ("CB_OL_Odd", rw),  # bit1
     ("CB_OL_Even", rw)),  # bit0

    ((ID_ADC_CFG, 'ADC_CFG', 0x06),  # reg
     ("TagID", rw),  # bit15
     ("TagID", rw),  # bit14
     ("TagID", rw),  # bit13
     ("TagID", rw),  # bit12
     ("SoC/nEoC", rw),  # bit11
     ("PGAGain", rw),  # bit10
     ("PGAGain", rw),  # bit9
     ("PGAGain", rw),  # bit8
     ("CCRst", w),  # bit7
     ("DisChComp", rw),  # bit6
     ("ADC1ADef", rw),  # bit5
     ("ADC1ADef", rw),  # bit4
     ("ADC1BDef", rw),  # bit3
     ("ADC1BDef", rw),  # bit2
     ("ADC2Def", rw),  # bit1
     ("ADC2Def", rw)),  # bit0

    ((ID_OV_UV_EN, 'OV_UV_EN', 0x08),  # reg
     ("CommonOVTh", rw),  # bit15
     ("CommonUVTh", rw),  # bit14
     ("CT14_OVUV_EN", rw),  # bit13
     ("CT13_OVUV_EN", rw),  # bit12
     ("CT12_OVUV_EN", rw),  # bit11
     ("CT11_OVUV_EN", rw),  # bit10
     ("CT10_OVUV_EN", rw),  # bit9
     ("CT9_OVUV_EN", rw),  # bit8
     ("CT8_OVUV_EN", rw),  # bit7
     ("CT7_OVUV_EN", rw),  # bit6
     ("CT6_OVUV_EN", rw),  # bit5
     ("CT5_OVUV_EN", rw),  # bit4
     ("CT4_OVUV_EN", rw),  # bit3
     ("CT3_OVUV_EN", rw),  # bit2
     ("CT2_OVUV_EN", rw),  # bit1
     ("CT1_OVUV_EN", rw)),  # bit0

    ((ID_GPIO_CFG1, 'GPIO_CFG1', 0x1D),  # reg
     ("", res),  # bit15
     ("", res),  # bit14
     ("GPIO6_Cfg", rw),  # bit13
     ("GPIO6_Cfg", rw),  # bit12
     ("GPIO5_Cfg", rw),  # bit11
     ("GPIO5_Cfg", rw),  # bit10
     ("GPIO4_Cfg", rw),  # bit9
     ("GPIO4_Cfg", rw),  # bit8
     ("GPIO3_Cfg", rw),  # bit7
     ("GPIO3_Cfg", rw),  # bit6
     ("GPIO2_Cfg", rw),  # bit5
     ("GPIO2_Cfg", rw),  # bit4
     ("GPIO1_Cfg", rw),  # bit3
     ("GPIO1_Cfg", rw),  # bit2
     ("GPIO0_Cfg", rw),  # bit1
     ("GPIO0_Cfg", rw)),  # bit0

    ((ID_GPIO_CFG2, 'GPIO_CFG2', 0x1E),  # reg
     ("", res),  # bit15
     ("", res),  # bit14
     ("", res),  # bit13
     ("", res),  # bit12
     ("", res),  # bit11
     ("", res),  # bit10
     ("GPIO2_SOC", rw),  # bit9
     ("GPIO0_WU", rw),  # bit8
     ("GPIO0_FLT_ACT", rw),  # bit7
     ("GPIO6_DR", rw),  # bit6
     ("GPIO5_DR", rw),  # bit5
     ("GPIO4_DR", rw),  # bit4
     ("GPIO3_DR", rw),  # bit3
     ("GPIO2_DR", rw),  # bit2
     ("GPIO1_DR", rw),  # bit1
     ("GPIO0_DR", rw)),  # bit0

    #      ((ID_GPIO_STS, 'GPIO_STS'        ,   0x1F ),     #reg
    #         ("", res),                            #bit15
    #        ("GPIO6_H", w),                            #bit14
    #        ("GPIO5_H", w),                            #bit13
    #        ("GPIO4_H", w),                            #bit12
    #        ("GPIO3_H", w),                            #bit11
    #        ("GPIO2_H", w),                            #bit10
    #        ("GPIO1_H", w),                            #bit9
    #        ("GPIO0_H", w),                            #bit8
    #        ("", res),                            #bit7
    #        ("GPIO6_ST", r),                            #bit6
    #        ("GPIO5_ST", r),                            #bit5
    #        ("GPIO4_ST", r),                            #bit4
    #        ("GPIO3_ST", r),                            #bit3
    #        ("GPIO2_ST", r),                            #bit2
    #        ("GPIO1_ST", r),                            #bit1
    #        ("GPIO0_ST", r)                      ),     #bit0

    ((ID_FAULT_MASK1, 'FaultMask1 (disable)', 0x27),  # reg
     #        ("", res),                            #bit15
     #        ("", res),                            #bit14
     #        ("", res),                            #bit13
     #        ("MASK12_F", rw),                            #bit12
     #        ("MASK11_F", rw),                            #bit11
     #        ("MASK10_F", rw),                            #bit10
     #        ("MASK9_F", rw),                            #bit9
     #        ("MASK8_F", rw),                            #bit8
     #        ("MASK7_F", rw),                            #bit7
     #        ("MASK6_F", rw),                            #bit6
     #        ("MASK5_F", rw),                            #bit5
     #        ("MASK4_F", rw),                            #bit4
     #        ("MASK3_F", rw),                            #bit3
     #        ("MASK2_F", rw),                            #bit2
     #        ("MASK1_F", rw),                            #bit1
     #        ("MASK0_F", rw)                      ),     #bit0
     ("", res),  # bit15
     ("", res),  # bit14
     ("", res),  # bit13
     ("VPWR_OV", rw),  # bit12
     ("VPWR_LV", rw),  # bit11
     ("COM_LOSS", rw),  # bit10
     ("COM_ERR", rw),  # bit9
     ("CSB_WUP", rw),  # bit8
     ("GPIO0_WUP", rw),  # bit7
     ("I2C_ERR", rw),  # bit6
     ("ISENSE_OL", rw),  # bit5
     ("ISENSE_OC", rw),  # bit4
     ("AN_OT", rw),  # bit3
     ("AN_UT", rw),  # bit2
     ("CT_OV", rw),  # bit1
     ("CT_UV", rw)),  # bit0

    ((ID_FAULT_MASK2, 'FaultMask2 (disable)', 0x28),  # reg
     #        ("MASK15_F", rw),                            #bit15
     #        ("MASK14_F", rw),                            #bit14
     #        ("MASK13_F", rw),                            #bit13
     #        ("MASK12_F", rw),                            #bit12
     #        ("MASK11_F", rw),                            #bit11
     #        ("MASK10_F", rw),                            #bit10
     #        ("MASK9_F", rw),                            #bit9
     #        ("", res),                            #bit8
     #        ("", res),                            #bit7
     #        ("MASK6_F", rw),                            #bit6
     #        ("MASK5_F", rw),                            #bit5
     #        ("MASK4_F", rw),                            #bit4
     #        ("MASK3_F", rw),                            #bit3
     #        ("MASK2_F", rw),                            #bit2
     #        ("MASK1_F", rw),                            #bit1
     #        ("MASK0_F", rw)                      ),     #bit0

     ("VCOM_OV", rw),  # bit15
     ("VCOM_UV", rw),  # bit14
     ("VANA_OV", rw),  # bit13
     ("VANA_UV", rw),  # bit12
     ("ADC1_B", rw),  # bit11
     ("ADC1_A", rw),  # bit10
     ("GND_LOSS", rw),  # bit9
     ("", res),  # bit8
     ("", res),  # bit7
     ("AN_OPEN", rw),  # bit6
     ("GPIO_SHORT", rw),  # bit5
     ("CB_SHORT", rw),  # bit4
     ("CB_OPEN", rw),  # bit3
     ("OSC_ERR", rw),  # bit2
     ("DED_ERR", rw),  # bit1
     ("FUSE_ERR", rw)),  # bit0

    ((ID_FAULT_MASK3, 'FaultMask3 (disable)', 0x29),  # reg
     #        ("MASK15_F", rw),                            #bit15
     #        ("MASK14_F", rw),                            #bit14
     #        ("MASK13_F", rw),                            #bit13
     #        ("MASK12_F", rw),                            #bit12
     #        ("MASK11_F", rw),                            #bit11
     #        ("MASK10_F", rw),                            #bit10
     #        ("MASK9_F", rw),                            #bit9
     #        ("MASK8_F", rw),                            #bit8
     #        ("MASK7_F", rw),                            #bit7
     #        ("MASK6_F", rw),                            #bit6
     #        ("MASK5_F", rw),                            #bit5
     #        ("MASK4_F", rw),                            #bit4
     #        ("MASK3_F", rw),                            #bit3
     #        ("MASK2_F", rw),                            #bit2
     #        ("MASK1_F", rw),                            #bit1
     #        ("MASK0_F", rw)                      ),     #bit0

     ("CC_OVR", rw),  # bit15
     ("DIAG_TO", rw),  # bit14
     ("EOT_CB14", rw),  # bit13
     ("EOT_CB13", rw),  # bit12
     ("EOT_CB12", rw),  # bit11
     ("EOT_CB11", rw),  # bit10
     ("EOT_CB10", rw),  # bit9
     ("EOT_CB9", rw),  # bit8
     ("EOT_CB8", rw),  # bit7
     ("EOT_CB7", rw),  # bit6
     ("EOT_CB6", rw),  # bit5
     ("EOT_CB5", rw),  # bit4
     ("EOT_CB4", rw),  # bit3
     ("EOT_CB3", rw),  # bit2
     ("EOT_CB2", rw),  # bit1
     ("EOT_CB1", rw)),  # bit0

    ((ID_WAKEUP_MASK1, 'WakeupMask1 (disable)', 0x2A),  # reg
     #        ("", res),                            #bit15
     #        ("", res),                            #bit14
     #        ("", res),                            #bit13
     #        ("MASK12_F", rw),                            #bit12
     #        ("MASK11_F", rw),                            #bit11
     #        ("", res),                            #bit10
     #        ("", res),                            #bit9
     #        ("MASK8_F", rw),                            #bit8
     #        ("MASK7_F", rw),                            #bit7
     #        ("", res),                            #bit6
     #        ("", res),                            #bit5
     #        ("MASK4_F", rw),                            #bit4
     #        ("MASK3_F", rw),                            #bit3
     #        ("MASK2_F", rw),                            #bit2
     #        ("MASK1_F", rw),                            #bit1
     #        ("MASK0_F", rw)                      ),     #bit0

     ("", res),  # bit15
     ("", res),  # bit14
     ("", res),  # bit13
     ("VPWR_OV", rw),  # bit12
     ("VPWR_LV", rw),  # bit11
     ("", res),  # bit10
     ("", res),  # bit9
     ("CSB_WUP", rw),  # bit8
     ("GPIO0_WUP", rw),  # bit7
     ("", res),  # bit6
     ("", res),  # bit5
     ("ISENSE_OC", rw),  # bit4
     ("AN_OT", rw),  # bit3
     ("AN_UT", rw),  # bit2
     ("CT_OV", rw),  # bit1
     ("CT_UV", rw)),  # bit0

    ((ID_WAKEUP_MASK2, 'WakeupMask2 (disable)', 0x2B),  # reg
     #        ("MASK15_F", rw),                            #bit15
     #        ("MASK14_F", rw),                            #bit14
     #        ("MASK13_F", rw),                            #bit13
     #        ("MASK12_F", rw),                            #bit12
     #        ("MASK11_F", rw),                            #bit11
     #        ("MASK10_F", rw),                            #bit10
     #        ("MASK9_F", rw),                            #bit9
     #        ("MASK8_F", rw),                            #bit8
     #        ("", res),                            #bit7
     #        ("", res),                            #bit6
     #        ("MASK5_F", rw),                            #bit5
     #        ("MASK4_F", rw),                            #bit4
     #        ("", res),                            #bit3
     #        ("MASK2_F", rw),                            #bit2
     #        ("MASK1_F", rw),                            #bit1
     #        ("", res)                      ),     #bit0

     ("VCOM_OV", rw),  # bit15
     ("VCOM_UV", rw),  # bit14
     ("VANA_OV", rw),  # bit13
     ("VANA_UV", rw),  # bit12
     ("ADC1_B", rw),  # bit11
     ("ADC1_A", rw),  # bit10
     ("GND_LOSS", rw),  # bit9
     ("IC_TSD", rw),  # bit8
     ("", res),  # bit7
     ("", res),  # bit6
     ("GPIO_SHORT", rw),  # bit5
     ("CB_SHORT", rw),  # bit4
     ("", res),  # bit3
     ("OSC_ERR", rw),  # bit2
     ("DED_ERR", rw),  # bit1
     ("", res)),  # bit0

    ((ID_WAKEUP_MASK3, 'WakeupMask3 (disable)', 0x2C),  # reg
     #        ("", res),                            #bit15
     #        ("", res),                            #bit14
     #        ("MASK13_F", rw),                            #bit13
     #        ("MASK12_F", rw),                            #bit12
     #        ("MASK11_F", rw),                            #bit11
     #        ("MASK10_F", rw),                            #bit10
     #        ("MASK9_F", rw),                            #bit9
     #        ("MASK8_F", rw),                            #bit8
     #        ("MASK7_F", rw),                            #bit7
     #        ("MASK6_F", rw),                            #bit6
     #        ("MASK5_F", rw),                            #bit5
     #        ("MASK4_F", rw),                            #bit4
     #        ("MASK3_F", rw),                            #bit3
     #        ("MASK2_F", rw),                            #bit2
     #        ("MASK1_F", rw),                            #bit1
     #        ("MASK0_F", rw)                      ),     #bit0

     ("CC_OVR", rw),  # bit15
     ("", res),  # bit14
     ("EOT_CB14", rw),  # bit13
     ("EOT_CB13", rw),  # bit12
     ("EOT_CB12", rw),  # bit11
     ("EOT_CB11", rw),  # bit10
     ("EOT_CB10", rw),  # bit9
     ("EOT_CB9", rw),  # bit8
     ("EOT_CB8", rw),  # bit7
     ("EOT_CB7", rw),  # bit6
     ("EOT_CB6", rw),  # bit5
     ("EOT_CB5", rw),  # bit4
     ("EOT_CB4", rw),  # bit3
     ("EOT_CB3", rw),  # bit2
     ("EOT_CB2", rw),  # bit1
     ("EOT_CB1", rw)),  # bit0

    ((ID_ADC2_OFFSET_COMP, 'ADC2_OFFSET_COMP', 0x07),  # reg
     ("CCRstCfg", rw),  # bit15
     ("FreeCnt", rw),  # bit14
     ("CC_POvl", r),  # bit13
     ("CC_NOvl", r),  # bit12
     ("SampOvl", r),  # bit11
     ("CC_OVT", r),  # bit10
     ("", res),  # bit9
     ("", res),  # bit8
     ("ADC2OffsetComp", rw),  # bit7
     ("ADC2OffsetComp", rw),  # bit6
     ("ADC2OffsetComp", rw),  # bit5
     ("ADC2OffsetComp", rw),  # bit4
     ("ADC2OffsetComp", rw),  # bit3
     ("ADC2OffsetComp", rw),  # bit2
     ("ADC2OffsetComp", rw),  # bit1
     ("ADC2OffsetComp", rw)),  # bit0


    ((ID_CB_CFG_1, 'CB_CFG_1', 0x0C),  # reg
     ("", res),  # bit15
     ("", res),  # bit14
     ("", res),  # bit13
     ("", res),  # bit12
     ("", res),  # bit11
     ("", res),  # bit10
     ("CBEN/STS", rw),  # bit9
     ("CBTimer", rw),  # bit8
     ("CBTimer", rw),  # bit7
     ("CBTimer", rw),  # bit6
     ("CBTimer", rw),  # bit5
     ("CBTimer", rw),  # bit4
     ("CBTimer", rw),  # bit3
     ("CBTimer", rw),  # bit2
     ("CBTimer", rw),  # bit1
     ("CBTimer", rw)),  # bit0

    ((ID_CB_CFG_2, 'CB_CFG_2', 0x0D),  # reg
     ("", res),  # bit15
     ("", res),  # bit14
     ("", res),  # bit13
     ("", res),  # bit12
     ("", res),  # bit11
     ("", res),  # bit10
     ("CBEN/STS", rw),  # bit9
     ("CBTimer", rw),  # bit8
     ("CBTimer", rw),  # bit7
     ("CBTimer", rw),  # bit6
     ("CBTimer", rw),  # bit5
     ("CBTimer", rw),  # bit4
     ("CBTimer", rw),  # bit3
     ("CBTimer", rw),  # bit2
     ("CBTimer", rw),  # bit1
     ("CBTimer", rw)),  # bit0

    ((ID_CB_CFG_3, 'CB_CFG_3', 0x0E),  # reg
     ("", res),  # bit15
     ("", res),  # bit14
     ("", res),  # bit13
     ("", res),  # bit12
     ("", res),  # bit11
     ("", res),  # bit10
     ("CBEN/STS", rw),  # bit9
     ("CBTimer", rw),  # bit8
     ("CBTimer", rw),  # bit7
     ("CBTimer", rw),  # bit6
     ("CBTimer", rw),  # bit5
     ("CBTimer", rw),  # bit4
     ("CBTimer", rw),  # bit3
     ("CBTimer", rw),  # bit2
     ("CBTimer", rw),  # bit1
     ("CBTimer", rw)),  # bit0

    ((ID_CB_CFG_4, 'CB_CFG_4', 0x0F),  # reg
     ("", res),  # bit15
     ("", res),  # bit14
     ("", res),  # bit13
     ("", res),  # bit12
     ("", res),  # bit11
     ("", res),  # bit10
     ("CBEN/STS", rw),  # bit9
     ("CBTimer", rw),  # bit8
     ("CBTimer", rw),  # bit7
     ("CBTimer", rw),  # bit6
     ("CBTimer", rw),  # bit5
     ("CBTimer", rw),  # bit4
     ("CBTimer", rw),  # bit3
     ("CBTimer", rw),  # bit2
     ("CBTimer", rw),  # bit1
     ("CBTimer", rw)),  # bit0

    ((ID_CB_CFG_5, 'CB_CFG_5', 0x10),  # reg
     ("", res),  # bit15
     ("", res),  # bit14
     ("", res),  # bit13
     ("", res),  # bit12
     ("", res),  # bit11
     ("", res),  # bit10
     ("CBEN/STS", rw),  # bit9
     ("CBTimer", rw),  # bit8
     ("CBTimer", rw),  # bit7
     ("CBTimer", rw),  # bit6
     ("CBTimer", rw),  # bit5
     ("CBTimer", rw),  # bit4
     ("CBTimer", rw),  # bit3
     ("CBTimer", rw),  # bit2
     ("CBTimer", rw),  # bit1
     ("CBTimer", rw)),  # bit0

    ((ID_CB_CFG_6, 'CB_CFG_6', 0x11),  # reg
     ("", res),  # bit15
     ("", res),  # bit14
     ("", res),  # bit13
     ("", res),  # bit12
     ("", res),  # bit11
     ("", res),  # bit10
     ("CBEN/STS", rw),  # bit9
     ("CBTimer", rw),  # bit8
     ("CBTimer", rw),  # bit7
     ("CBTimer", rw),  # bit6
     ("CBTimer", rw),  # bit5
     ("CBTimer", rw),  # bit4
     ("CBTimer", rw),  # bit3
     ("CBTimer", rw),  # bit2
     ("CBTimer", rw),  # bit1
     ("CBTimer", rw)),  # bit0

    ((ID_CB_CFG_7, 'CB_CFG_7', 0x12),  # reg
     ("", res),  # bit15
     ("", res),  # bit14
     ("", res),  # bit13
     ("", res),  # bit12
     ("", res),  # bit11
     ("", res),  # bit10
     ("CBEN/STS", rw),  # bit9
     ("CBTimer", rw),  # bit8
     ("CBTimer", rw),  # bit7
     ("CBTimer", rw),  # bit6
     ("CBTimer", rw),  # bit5
     ("CBTimer", rw),  # bit4
     ("CBTimer", rw),  # bit3
     ("CBTimer", rw),  # bit2
     ("CBTimer", rw),  # bit1
     ("CBTimer", rw)),  # bit0

    ((ID_CB_CFG_8, 'CB_CFG_8', 0x13),  # reg
     ("", res),  # bit15
     ("", res),  # bit14
     ("", res),  # bit13
     ("", res),  # bit12
     ("", res),  # bit11
     ("", res),  # bit10
     ("CBEN/STS", rw),  # bit9
     ("CBTimer", rw),  # bit8
     ("CBTimer", rw),  # bit7
     ("CBTimer", rw),  # bit6
     ("CBTimer", rw),  # bit5
     ("CBTimer", rw),  # bit4
     ("CBTimer", rw),  # bit3
     ("CBTimer", rw),  # bit2
     ("CBTimer", rw),  # bit1
     ("CBTimer", rw)),  # bit0

    ((ID_CB_CFG_9, 'CB_CFG_9', 0x14),  # reg
     ("", res),  # bit15
     ("", res),  # bit14
     ("", res),  # bit13
     ("", res),  # bit12
     ("", res),  # bit11
     ("", res),  # bit10
     ("CBEN/STS", rw),  # bit9
     ("CBTimer", rw),  # bit8
     ("CBTimer", rw),  # bit7
     ("CBTimer", rw),  # bit6
     ("CBTimer", rw),  # bit5
     ("CBTimer", rw),  # bit4
     ("CBTimer", rw),  # bit3
     ("CBTimer", rw),  # bit2
     ("CBTimer", rw),  # bit1
     ("CBTimer", rw)),  # bit0

    ((ID_CB_CFG_10, 'CB_CFG_10', 0x15),  # reg
     ("", res),  # bit15
     ("", res),  # bit14
     ("", res),  # bit13
     ("", res),  # bit12
     ("", res),  # bit11
     ("", res),  # bit10
     ("CBEN/STS", rw),  # bit9
     ("CBTimer", rw),  # bit8
     ("CBTimer", rw),  # bit7
     ("CBTimer", rw),  # bit6
     ("CBTimer", rw),  # bit5
     ("CBTimer", rw),  # bit4
     ("CBTimer", rw),  # bit3
     ("CBTimer", rw),  # bit2
     ("CBTimer", rw),  # bit1
     ("CBTimer", rw)),  # bit0

    ((ID_CB_CFG_11, 'CB_CFG_11', 0x16),  # reg
     ("", res),  # bit15
     ("", res),  # bit14
     ("", res),  # bit13
     ("", res),  # bit12
     ("", res),  # bit11
     ("", res),  # bit10
     ("CBEN/STS", rw),  # bit9
     ("CBTimer", rw),  # bit8
     ("CBTimer", rw),  # bit7
     ("CBTimer", rw),  # bit6
     ("CBTimer", rw),  # bit5
     ("CBTimer", rw),  # bit4
     ("CBTimer", rw),  # bit3
     ("CBTimer", rw),  # bit2
     ("CBTimer", rw),  # bit1
     ("CBTimer", rw)),  # bit0

    ((ID_CB_CFG_12, 'CB_CFG_12', 0x17),  # reg
     ("", res),  # bit15
     ("", res),  # bit14
     ("", res),  # bit13
     ("", res),  # bit12
     ("", res),  # bit11
     ("", res),  # bit10
     ("CBEN/STS", rw),  # bit9
     ("CBTimer", rw),  # bit8
     ("CBTimer", rw),  # bit7
     ("CBTimer", rw),  # bit6
     ("CBTimer", rw),  # bit5
     ("CBTimer", rw),  # bit4
     ("CBTimer", rw),  # bit3
     ("CBTimer", rw),  # bit2
     ("CBTimer", rw),  # bit1
     ("CBTimer", rw)),  # bit0

    ((ID_CB_CFG_13, 'CB_CFG_13', 0x18),  # reg
     ("", res),  # bit15
     ("", res),  # bit14
     ("", res),  # bit13
     ("", res),  # bit12
     ("", res),  # bit11
     ("", res),  # bit10
     ("CBEN/STS", rw),  # bit9
     ("CBTimer", rw),  # bit8
     ("CBTimer", rw),  # bit7
     ("CBTimer", rw),  # bit6
     ("CBTimer", rw),  # bit5
     ("CBTimer", rw),  # bit4
     ("CBTimer", rw),  # bit3
     ("CBTimer", rw),  # bit2
     ("CBTimer", rw),  # bit1
     ("CBTimer", rw)),  # bit0

    ((ID_CB_CFG_14, 'CB_CFG_14', 0x19),  # reg
     ("", res),  # bit15
     ("", res),  # bit14
     ("", res),  # bit13
     ("", res),  # bit12
     ("", res),  # bit11
     ("", res),  # bit10
     ("CBEN/STS", rw),  # bit9
     ("CBTimer", rw),  # bit8
     ("CBTimer", rw),  # bit7
     ("CBTimer", rw),  # bit6
     ("CBTimer", rw),  # bit5
     ("CBTimer", rw),  # bit4
     ("CBTimer", rw),  # bit3
     ("CBTimer", rw),  # bit2
     ("CBTimer", rw),  # bit1
     ("CBTimer", rw)),  # bit0

    #      (( 233, 'Reserved'        ,   0x68 ),     #reg
    #        ("", res),                            #bit15
    #        ("", res),                            #bit14
    #        ("", res),                            #bit13
    #        ("", res),                            #bit12
    #        ("", res),                            #bit11
    #        ("", res),                            #bit10
    #        ("", res),                            #bit9
    #        ("", res),                            #bit8
    #        ("", res),                            #bit7
    #        ("", res),                            #bit6
    #        ("", res),                            #bit5
    #        ("", res),                            #bit4
    #        ("", res),                            #bit3
    #        ("", res),                            #bit2
    #        ("", res),                            #bit1
    #        ("", res)                      ),     #bit0
    #
    #      (( 234, 'Reserved'        ,   0x6A ),     #reg
    #        ("", res),                            #bit15
    #        ("", res),                            #bit14
    #        ("", res),                            #bit13
    #        ("", res),                            #bit12
    #        ("", res),                            #bit11
    #        ("", res),                            #bit10
    #        ("", res),                            #bit9
    #        ("", res),                            #bit8
    #        ("", res),                            #bit7
    #        ("", res),                            #bit6
    #        ("", res),                            #bit5
    #        ("", res),                            #bit4
    #        ("", res),                            #bit3
    #        ("", res),                            #bit2
    #        ("", res),                            #bit1
    #        ("", res)                      ),     #bit0

    #      ((ID_EEPROM_CTRL, 'EEPROM_CTRL'        ,   0x6C ),     #reg
    #        ("R/W (busy)", rw),                            #bit15
    #        ("EepromAdd", rw),                            #bit14
    #        ("EepromAdd", rw),                            #bit13
    #        ("EepromAdd", rw),                            #bit12
    #        ("EepromAdd", rw),                            #bit11
    #        ("EepromAdd", rw),                            #bit10
    #        ("EepromAdd", rw),                            #bit9
    #        ("EepromAdd", rw),                            #bit8
    #        ("Data", rw),                            #bit7
    #        ("Data", rw),                            #bit6
    #        ("Data", rw),                            #bit5
    #        ("Data", rw),                            #bit4
    #        ("Data", rw),                            #bit3
    #        ("Data", rw),                            #bit2
    #        ("Data", rw),                            #bit1
    #        ("Data", rw)                      ),     #bit0

]
