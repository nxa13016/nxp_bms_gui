rem ***  local path of cmd file   ***

set script=setupWindows.py
rem set script=setupWindowswdll.py

set locpath=%~dp0
cd %locpath%


rem ***  the script must be located in the same folder as the *.py files   ***

copy %script% ..
cd ..
rmdir /S /Q \dist


rem ***  run py2exe and put outputs in log.txt   ***

python.exe %script% py2exe >py2exe\log.txt


rem ***  cleanup  ***

del %script%
rem echo "remove some MS stuff"
del dist\API-MS*.dll
del dist\POWRPROF.dll
rmdir /S /Q build
pause

