# -*- coding: utf-8 -*-
# ------------------------------------------------------------------------------
# Copyright (c) 2015, NXP Semiconductors.
# All rights reserved.
#  
# Redistribution and use in source and binary forms, with or without modification,
# are permitted provided that the following conditions are met:
#  
# o Redistributions of source code must retain the above copyright notice, this list
#   of conditions and the following disclaimer.
#  
# o Redistributions in binary form must reproduce the above copyright notice, this
#   list of conditions and the following disclaimer in the documentation and/or
#   other materials provided with the distribution.
#  

# o Neither the name of NXP Semiconductors nor the names of its
#   contributors may be used to endorse or promote products derived from this
#   software without specific prior written permission.
#  
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
# ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
# WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
# DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR
# ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
# (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
# LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
# ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
# (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
# SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
# ------------------------------------------------------------------------------
"""
Created on Thu Sep 08 16:17:31 2016

@author: R51406
"""
#widget generated with QtDesigner


import webbrowser

import sys
import os

import sip
sip.setapi("QString",2) 
sip.setapi("QVariant",2)
sip.setapi("QDate",2)
sip.setapi("QDateTime",2)
sip.setapi("QTextStream",2)
sip.setapi("QTime",2)
sip.setapi("QUrl",2)

#widget generated with QtDesigner
from uiEMCGuiMain import Ui_MainWindow        
from PyQt4.QtCore import *                          #@UnusedWildImport (Eclipse)
from PyQt4.QtGui import *                           #@UnusedWildImport (Eclipse)

#from MySerial import *                              #@UnusedWildImport (Eclipse)
from RingBuf import *                               #@UnusedWildImport (Eclipse)
from BCCList import *                               #@UnusedWildImport (Eclipse)

from MC3377x import *                               #@UnusedWildImport (Eclipse)
from datalog import *                               #@UnusedWildImport (Eclipse)
from CommId import *                                #@UnusedWildImport (Eclipse)

from worker import *                                #@UnusedWildImport (Eclipse)
#from evb import EVB #chiwg
from tableBMSview import *
from NTC_Table import *
from waveform import *

#version
title      = "NXP BMS GUI(CAN)"
swid       = 0x1001
version    = 2
subversion = 03

link_to_GUI_docu  = "docu/Documentation.htm" 
#link_to_GUI_docu  = "docu/html/pagegui.html" 
link_to_web       = "http://www.nxp.com/battery"

MESSAGE = [
    #0..9
    "BCC Pack Controller for EMC testing",
    "   *   CID-{:}  working ",
    "   *   CID-{:}  Error {:}",
    "Global reset", 
    "-Error",
    "TPL enable and BCC wakeup",
    " pass",
    " fail",
    "Init Phase:",
    "BCC - test nodes",
    #10..19
    "BCC - Configuration",
    "   *   CID-{:}  configuration ",
    "   *   CID-{:}  config. error {:}",
    "Recover operation started",
    "   *   CID-{:}  working normally",
    "   *   CID-{:}  recovered",
    "   *   CID-{:}  not recovered",
    "   *   CID-{:}  not working",
    "Goto Sleep Mode",
    "Slave wakeup received...",
    #20..
    "20",
]


#configuration
#max_com_port  = 255
baudrates = ['500', '1000']
ErrorMsg =   ['Ok', "Tx", "NoRes", "Len", "CRC" ]        
#ErrorColor = [Qt.green, Qt.blue, Qt.red, Qt.yellow, Qt.magenta]
sep = "\t"      #needed for error log
N_BUFFER = 50               # number of samples for statistics

# Volt14 = ["GUID",  "Stack [V]", "Cell-1 [V]", "Cell-2 [V]", "Cell-3 [V]", "Cell-4 [V]", "Cell-5 [V]", "Cell-6 [V]", "Cell-7 [V]", "Cell-8 [V]", "Cell-9 [V]", "Cell-10 [V]", "Cell-11 [V]", "Cell-12 [V]", "Cell-13 [V]", "Cell-14 [V]", 
#         "Current [mV]", "T_chip [C]", "AN0 [C]", "AN1 [C]", "AN2 [C]", "AN3 [C]", "AN4 [C]", "AN5 [C]", "AN6 [C]", "VBG_DIAG1A [V]", "VBG_DIAG1B [V]",
#         "Fault1", "Fault2", "Fault3",
#         "", 
#         "Ok", "Tx Error", "No Resp.", "Len Resp.", "CRC Resp."        
#         ]
DATALOG_PARSER = [
  #     ID                       name                 header        , header raw  ,  decoder     , physical
    (  ID_MEAS_VPWR          , 'MEAS_STACK'         , 'Vstack[V]'  , 'Vstack[1]'  , codeVPwr   ,      0 ),
    (  ID_MEAS_VCT1          , 'MEAS_CELL1'         , 'Vct1[V]'    , 'Vct1[1]'    , codeVCell  ,      0 ),
    (  ID_MEAS_VCT2          , 'MEAS_CELL2'         , 'Vct2[V]'    , 'Vct2[1]'    , codeVCell  ,      0 ),
    (  ID_MEAS_VCT3          , 'MEAS_CELL3'         , 'Vct3[V]'    , 'Vct3[1]'    , codeVCell  ,      0 ),
    (  ID_MEAS_VCT4          , 'MEAS_CELL4'         , 'Vct4[V]'    , 'Vct4[1]'    , codeVCell  ,      0 ),
    (  ID_MEAS_VCT5          , 'MEAS_CELL5'         , 'Vct5[V]'    , 'Vct5[1]'    , codeVCell  ,      0 ),
    (  ID_MEAS_VCT6          , 'MEAS_CELL6'         , 'Vct6[V]'    , 'Vct6[1]'    , codeVCell  ,      0 ),
    (  ID_MEAS_VCT7          , 'MEAS_CELL7'         , 'Vct7[V]'    , 'Vct7[1]'    , codeVCell  ,      0 ),
    (  ID_MEAS_VCT8          , 'MEAS_CELL8'         , 'Vct8[V]'    , 'Vct8[1]'    , codeVCell  ,      0 ),
    (  ID_MEAS_VCT9          , 'MEAS_CELL9'         , 'Vct9[V]'    , 'Vct9[1]'    , codeVCell  ,      0 ),
    (  ID_MEAS_VCT10         , 'MEAS_CELL10'        , 'Vct10[V]'   , 'Vct10[1]'   , codeVCell  ,      0 ),
    (  ID_MEAS_VCT11         , 'MEAS_CELL11'        , 'Vct11[V]'   , 'Vct11[1]'   , codeVCell  ,      0 ),
    (  ID_MEAS_VCT12         , 'MEAS_CELL12'        , 'Vct12[V]'   , 'Vct12[1]'   , codeVCell  ,      0 ),
    (  ID_MEAS_VCT13         , 'MEAS_CELL13'        , 'Vct13[V]'   , 'Vct13[1]'   , codeVCell  ,      0 ),
    (  ID_MEAS_VCT14         , 'MEAS_CELL14'        , 'Vct14[V]'   , 'Vct14[1]'   , codeVCell  ,      0 ),
    (  ID_MEAS_VCUR          , 'MEAS_ISENSE'        , 'Visense[mV]', 'Visense[1]' , codeVCur   ,      0 ),
    (  ID_MEAS_ICTEMP        , 'MEAS_IC_TEMP'       , 'Tchip[C]'   , 'Tchip[1]'   , codeICDegC ,      0 ),
    (  ID_MEAS_VBG_DIAG_ADC1A, 'MEAS_VBG_DIAG_ADC1A', 'Vbgadc1a[V]', 'Vbgadc1a[1]', codeVCell  ,      0 ),
    (  ID_MEAS_VBG_DIAG_ADC1B, 'MEAS_VBG_DIAG_ADC1B', 'Vbgadc1b[V]', 'Vbgadc1b[1]', codeVCell  ,      0 ),
    (  ID_MEAS_VAN0          , 'MEAS_AN0'           , 'Tan0[C]'    , 'Tan0[1]'    , codeAnDegC ,      0 ),
    (  ID_MEAS_VAN1          , 'MEAS_AN1'           , 'Tan1[C]'    , 'Tan1[1]'    , codeAnDegC ,      0 ),
    (  ID_MEAS_VAN2          , 'MEAS_AN2'           , 'Tan2[C]'    , 'Tan2[1]'    , codeAnDegC ,      0 ),
    (  ID_MEAS_VAN3          , 'MEAS_AN3'           , 'Tan3[C]'    , 'Tan3[1]'    , codeAnDegC ,      0 ),
    (  ID_MEAS_VAN4          , 'MEAS_AN4'           , 'Tan4[C]'    , 'Tan4[1]'    , codeAnDegC ,      0 ),
    (  ID_MEAS_VAN5          , 'MEAS_AN5'           , 'Tan5[C]'    , 'Tan5[1]'    , codeAnDegC ,      0 ),
    (  ID_MEAS_VAN6          , 'MEAS_AN6'           , 'Tan6[C]'    , 'Tan6[1]'    , codeAnDegC ,      0 )
    ]    

STATISTIC_PARSER = [
  #     ID                       name                 header        , header raw  ,  decoder     , physical
    (  ID_MEAS_VPWR          , 'MEAS_STACK'         , 'Vstack[V]'  , 'Vstack[1]'  , codeVPwr   ,      0 ),
    (  ID_MEAS_VCT1          , 'MEAS_CELL1'         , 'Vct1[V]'    , 'Vct1[1]'    , codeVCell  ,      0 ),
    (  ID_MEAS_VCT2          , 'MEAS_CELL2'         , 'Vct2[V]'    , 'Vct2[1]'    , codeVCell  ,      0 ),
    (  ID_MEAS_VCT3          , 'MEAS_CELL3'         , 'Vct3[V]'    , 'Vct3[1]'    , codeVCell  ,      0 ),
    (  ID_MEAS_VCT4          , 'MEAS_CELL4'         , 'Vct4[V]'    , 'Vct4[1]'    , codeVCell  ,      0 ),
    (  ID_MEAS_VCT5          , 'MEAS_CELL5'         , 'Vct5[V]'    , 'Vct5[1]'    , codeVCell  ,      0 ),
    (  ID_MEAS_VCT6          , 'MEAS_CELL6'         , 'Vct6[V]'    , 'Vct6[1]'    , codeVCell  ,      0 ),
    (  ID_MEAS_VCT7          , 'MEAS_CELL7'         , 'Vct7[V]'    , 'Vct7[1]'    , codeVCell  ,      0 ),
    (  ID_MEAS_VCT8          , 'MEAS_CELL8'         , 'Vct8[V]'    , 'Vct8[1]'    , codeVCell  ,      0 ),
    (  ID_MEAS_VCT9          , 'MEAS_CELL9'         , 'Vct9[V]'    , 'Vct9[1]'    , codeVCell  ,      0 ),
    (  ID_MEAS_VCT10         , 'MEAS_CELL10'        , 'Vct10[V]'   , 'Vct10[1]'   , codeVCell  ,      0 ),
    (  ID_MEAS_VCT11         , 'MEAS_CELL11'        , 'Vct11[V]'   , 'Vct11[1]'   , codeVCell  ,      0 ),
    (  ID_MEAS_VCT12         , 'MEAS_CELL12'        , 'Vct12[V]'   , 'Vct12[1]'   , codeVCell  ,      0 ),
    (  ID_MEAS_VCT13         , 'MEAS_CELL13'        , 'Vct13[V]'   , 'Vct13[1]'   , codeVCell  ,      0 ),
    (  ID_MEAS_VCT14         , 'MEAS_CELL14'        , 'Vct14[V]'   , 'Vct14[1]'   , codeVCell  ,      0 ),
    (  ID_MEAS_VCUR          , 'MEAS_ISENSE'        , 'Visense[mV]', 'Visense[1]' , codeVCur   ,      0 ),
#     (  ID_MEAS_ICTEMP        , 'MEAS_IC_TEMP'       , 'Tchip[C]'   , 'Tchip[1]'   , codeICDegC ,      0 ),
#     (  ID_MEAS_VBG_DIAG_ADC1A, 'MEAS_VBG_DIAG_ADC1A', 'Vbgadc1a[V]', 'Vbgadc1a[1]', codeVCell  ,      0 ),
#     (  ID_MEAS_VBG_DIAG_ADC1B, 'MEAS_VBG_DIAG_ADC1B', 'Vbgadc1b[V]', 'Vbgadc1b[1]', codeVCell  ,      0 ),
##    (  ID_MEAS_VAN0          , 'MEAS_AN0'           , 'Tan0[C]'    , 'Tan0[1]'    , codeAnDegC ,      0 ),
#     (  ID_MEAS_VAN1          , 'MEAS_AN1'           , 'Tan1[C]'    , 'Tan1[1]'    , codeAnDegC ,      0 ),
#     (  ID_MEAS_VAN2          , 'MEAS_AN2'           , 'Tan2[C]'    , 'Tan2[1]'    , codeAnDegC ,      0 ),
#     (  ID_MEAS_VAN3          , 'MEAS_AN3'           , 'Tan3[C]'    , 'Tan3[1]'    , codeAnDegC ,      0 ),
#     (  ID_MEAS_VAN4          , 'MEAS_AN4'           , 'Tan4[C]'    , 'Tan4[1]'    , codeAnDegC ,      0 ),
#     (  ID_MEAS_VAN5          , 'MEAS_AN5'           , 'Tan5[C]'    , 'Tan5[1]'    , codeAnDegC ,      0 ),
#     (  ID_MEAS_VAN6          , 'MEAS_AN6'           , 'Tan6[C]'    , 'Tan6[1]'    , codeAnDegC ,      0 )
    ]    
HVBMSElementList = [
'BCCelement_1_1', 
'BCCelement_1_2', 
'BCCelement_1_3', 
'BCCelement_1_4', 
'BCCelement_1_5', 
'BCCelement_1_6', 
'BCCelement_1_7', 
'BCCelement_1_8', 
'BCCelement_1_9', 
'BCCelement_1_10', 
]
HVBMSCentElementList = [
'BCCelement_2_1', 
'BCCelement_2_2', 
'BCCelement_2_3', 
'BCCelement_2_4', 
'BCCelement_2_5', 
'BCCelement_2_6', 
'BCCelement_2_7', 
'BCCelement_2_8', 
]
HVBMSBCC6ElementList = [
'BCCelement_3_1', 
'BCCelement_3_2', 
'BCCelement_3_3', 
'BCCelement_3_4', 
'BCCelement_3_5', 
'BCCelement_3_6', 
'BCCelement_3_7', 
'BCCelement_3_8', 
'BCCelement_3_9', 
'BCCelement_3_10', 
'BCCelement_3_11', 
'BCCelement_3_12', 
'BCCelement_3_13', 
'BCCelement_3_14', 
'BCCelement_3_15', 
'BCCelement_3_16', 
'BCCelement_3_17', 
'BCCelement_3_18', 
'BCCelement_3_19', 
'BCCelement_3_20',
'BCCelement_3_21', 
'BCCelement_3_22', 
'BCCelement_3_23', 
'BCCelement_3_24', 
'BCCelement_3_25', 
'BCCelement_3_26', 
'BCCelement_3_27', 
'BCCelement_3_28', 
'BCCelement_3_29', 
'BCCelement_3_30',

]
V48BMSElementList = [
'BCCelement_4_1', 
]
V14BMSElementList = [
'BCCelement_6_1', 
'BCCelement_6_2', 
'BCCelement_6_3', 
'BCCelement_6_4', 
'BCCelement_6_5', 
#'BCCelement_6_6', 
]

WBMSElementList = [
'BCCelement_7_1',
'BCCelement_7_2',
'BCCelement_7_3',
'BCCelement_7_4',
'BCCelement_7_5',
'BCCelement_7_6',
'BCCelement_7_7',
'BCCelement_7_8',
#'BCCelement_6_6',
]

WBMSSignalList = [
'WBMS_editS_1',
'WBMS_editS_2',
'WBMS_editS_3',
'WBMS_editS_4',
'WBMS_editS_5',
'WBMS_editS_6',
'WBMS_editS_7',
'WBMS_editS_8',
#'BCCelement_6_6',
]

BMS638VIEW = [
#ID                                                 name                                unit        comment  decoder
( ID_VSENSE0           , 'Vsense0'             , 'mV'   , 'MSD+ [F]'  ,      0 ),
( ID_VSENSE1           , 'Vsense1'             , 'mV'   , 'MSD- [G]'  ,      0 ),
( ID_VSENSE2           , 'Vsense2'             , 'mV'   , 'V_Charger [D]'  ,      0 ),
( ID_VSENSE3           , 'Vsense3'             , 'mV'   , 'VLINK- [E]'  ,      0 ),
( ID_TSENSE0           , 'PTB0'                   , 'mV'   , 'N/A'  ,      0 ),
( ID_TSENSE1           , 'PTB1'                   , 'mV'   , 'VLINK+ [C]'  ,      0 ),
( ID_TSENSE2           , 'PTB2'                   , 'mV'   , 'VPACK [A]'  ,      0 ),
( ID_TSENSE3           , 'PTB3'                   , 'mV'   , 'VPOS [B]'  ,      0 ),
( ID_TSENSE4           , 'PTB4'                   , 'mV'   , 'Shunt Temperature'  ,      0 ),
( ID_ISENSE             , 'Isense'                , 'mA'   , 'current with SHUNT'  ,      0 ),
( ID_TCHIP                , 'Tchip'                 , 'C'     , 'Internal Temperature'  ,      0 ),
#( ID_SOC                   , 'SOC'                   , '%'   , 0  ,      0 ),
#( ID_Coulomb             , 'Coulomb'            , ' '   , 0  ,      0 ),
]

Interface = ("unknown", "SPI", "TPL")
TYPEEVB = ("unknown", "Type 1", "Type Ard.", )
CHIP = ["unknown", "MC33771", "MC33772"]
#SUPPORTED_CHIPS = ["MC33771p2", "MC33772p1", "MC33771p3"]

# gui behaviour
uiRefreshRate = 100  #[ms]
commstimeout  = 1000 #[ms]                    

#LOGFORMAT = ['raw', 'physical']
#BCCLIST = ['BCC6', 'BCC14']
LOGFORMLIST=['physical', 'raw']


class ApplicationWindow(QMainWindow):

    NoClusters = 1    
    SelectCID = 0
    logformat = 0
#    rxcount = 0
    lastTime = (0,0,0)
    excelrefrow = 0
    logdriftformat = False
    noOfClusters = 0
    form = 0
    EswitchInput = 0x00
    sigWaveData = pyqtSignal(int,float)
    
    def __init__(self, parent = None):
        QMainWindow.__init__(self, parent)
        self.ui = Ui_MainWindow()
        self.ui.setupUi(self)

        self.swident = 0

        self.ui.cbCOMBaud.clear()
        self.ui.cbCOMBaud.addItems(baudrates)
        self.ui.cbCOMBaud.setCurrentIndex(1)
        #self.ui.cbCOMPort.refreshComboBox()

        # ---- build PackController database ----
        self.packController = PackController()
        self.clusters = Clusters(1)
        self.createDB()

        self.waveform = RealtimeDemo()
        self.waveform.sigWaveData.connect(self.waveform.DataEngine)
        # -----  Serial port receiver  ----
        self.thread = Worker()
        self.thread.sigBytesWaiting.connect(self.updateStatusBar2)    
        self.thread.sigRxData.connect(self.updateRxData) #chiwg
        
        # ----- connect ui signals ----
        self.uiConnectSignals()

        # ----- status bar -----
        self.uiInitStatusBar()        

        self.ui.cbNoClusters.setCurrentIndex(9)
        self.ui.CT_Error.setCurrentIndex(9)
        self.ui.tabWidget_1.setCurrentIndex(0)
        # datalog
        self.dlog = Datalog()

        self.resize(1600, 1000)
        self.setWindowTitle(self.tr("{} V{}.{:02}".format(title, version, subversion)))
#        self.setWindowIcon(QIcon("HVBMS.ico"))
#        self.ui.lblNXP.setPixmap(QPixmap("nxp_logo.png"))       
        #for i in range(len(HVBMSElementList)):
            #exec('self.ui.%s.item(0,0).setText('111')'%HVBMSElementList[i])   
            #text = QTableWidgetItem('{:04X}'.format(msg))   
     
        #configure for 
#        self.ui.tabWidget_1.setTabEnabled(1,  False);
#        self.ui.tabWidget_1.setTabEnabled(2,  False);
#        self.ui.tabWidget_1.setTabEnabled(3,  False);
#        self.ui.tabWidget_1.setTabEnabled(4,  False);
#        self.ui.tabWidget_1.setTabEnabled(5,  False);
#        self.ui.tabWidget_1.setTabEnabled(6,  False);
#        self.ui.tabWidget_1.setTabEnabled(8,  False);

        #HVBMS demo tab
        for i in range(len(HVBMSElementList)):
            exec('self.ui.%s.horizontalHeader().setVisible(False)'%HVBMSElementList[i])
            exec('self.ui.%s.verticalHeader().setVisible(False)'%HVBMSElementList[i])
            exec('self.ui.%s.setColumnCount(2)'%HVBMSElementList[i])
            exec('self.ui.%s.setRowCount(15)'%HVBMSElementList[i])
            exec('self.ui.%s.horizontalHeader().setDefaultSectionSize(65)'%HVBMSElementList[i])
            
        #HVBMS center demo tab
        for i in range(len(HVBMSCentElementList)):
            exec('self.ui.%s.horizontalHeader().setVisible(False)'%HVBMSCentElementList[i])
            exec('self.ui.%s.verticalHeader().setVisible(False)'%HVBMSCentElementList[i])
            exec('self.ui.%s.setColumnCount(2)'%HVBMSCentElementList[i])
            exec('self.ui.%s.setRowCount(15)'%HVBMSCentElementList[i])
            exec('self.ui.%s.horizontalHeader().setDefaultSectionSize(65)'%HVBMSCentElementList[i])
            
        #HVBMSBCC6 demo tab
        for i in range(len(HVBMSBCC6ElementList)):
            exec('self.ui.%s.horizontalHeader().setVisible(False)'%HVBMSBCC6ElementList[i])
            exec('self.ui.%s.verticalHeader().setVisible(False)'%HVBMSBCC6ElementList[i])
            exec('self.ui.%s.setColumnCount(2)'%HVBMSBCC6ElementList[i])
            exec('self.ui.%s.setRowCount(7)'%HVBMSBCC6ElementList[i])
            exec('self.ui.%s.horizontalHeader().setDefaultSectionSize(60)'%HVBMSBCC6ElementList[i])
            exec('self.ui.%s.verticalHeader().setDefaultSectionSize(17)'%HVBMSBCC6ElementList[i])
            
        #48VBMS demo table
        self.ui.BCCelement_4_1.horizontalHeader().setVisible(False)
        self.ui.BCCelement_4_1.setColumnCount(1)
        self.ui.BCCelement_4_1.horizontalHeader().setDefaultSectionSize(80)
        text = QTableWidgetItem('')  
        self.ui.BCCelement_4_1.setItem(0, 1,  text)
        
        #BJB demo table
        # row headers
        self.BJBTable = NTC_table();
        rowheader = ["{:}  [{:}]".format(name, unit) for _, name, unit, _, _ in BMS638VIEW]
        self.ui.tableviewBJB.setRowCount(len(rowheader))   
        self.ui.tableviewBJB.setVerticalHeaderLabels(rowheader)
        self.ui.tableviewBJB.horizontalHeader().setDefaultSectionSize(80)
        for r in range(self.ui.tableviewBJB.rowCount()):
            text1 = QTableWidgetItem(["{:}".format(com) for _, _, _, com, _ in BMS638VIEW][r])
            self.ui.tableviewBJB.setItem(r,  2,  text1)
            self.ui.tableviewBJB.item(r,2).setTextAlignment(Qt.AlignHCenter | Qt.AlignVCenter)
        
        #14VBMS demo table
        for i in range(len(V14BMSElementList)):
            exec('self.ui.%s.horizontalHeader().setVisible(False)'%V14BMSElementList[i])
            exec('self.ui.%s.verticalHeader().setVisible(False)'%V14BMSElementList[i])
            exec('self.ui.%s.setColumnCount(1)'%V14BMSElementList[i])
            exec('self.ui.%s.setRowCount(2)'%V14BMSElementList[i])
            exec('self.ui.%s.horizontalHeader().setDefaultSectionSize(85)'%V14BMSElementList[i])
            exec('self.ui.%s.verticalHeader().setDefaultSectionSize(23)'%V14BMSElementList[i])

        for i in range(len(WBMSElementList)):
            exec('self.ui.%s.horizontalHeader().setVisible(False)'%WBMSElementList[i])
            exec('self.ui.%s.verticalHeader().setVisible(False)'%WBMSElementList[i])
            exec('self.ui.%s.setColumnCount(2)'%WBMSElementList[i])
            exec('self.ui.%s.setRowCount(15)'%WBMSElementList[i])
            exec('self.ui.%s.horizontalHeader().setDefaultSectionSize(70)'%WBMSElementList[i])
            exec ('self.ui.%s.verticalHeader().setDefaultSectionSize(16)' % WBMSElementList[i])
        
        try:     
            UI_VERSION = 1
            _file = os.path.abspath(sys.argv[0])
            programname = os.path.basename(_file)
            programbase, ext = os.path.splitext(programname)  # extract basename and ext from filename
#            print programname, programbase, ext
            settings = QSettings("nxp", programbase)   
#            print settings.value("size").toSize()
            self.resize(settings.value("size").toSize())
#            print settings.value("geometry").toGeometry()
            self.restoreGeometry(settings.value("geometry").toGeometry())
            self.restoreState(settings.value("state"),UI_VERSION) 
        except:
            print "error"    

        # ----- schedule for updates -----
        self.timer = QTimer()
        self.timer.timeout.connect(self.uiRefresh)        
        self.timer.singleShot(uiRefreshRate, self.uiRefresh)       
        
    def __del__(self):
        self.thread.exiting = True
        self.thread.closeport()

    def createDB(self):
        """ dbBMS [CID-1] [ID_xxx]
        """
        self.dbBMS = [[RingBuf(buflen=N_BUFFER) for _ in range(ID_MAX+1)] for _ in range(MAX_NO_CLUSTER)]


    def closeEvent(self, event):      # user clicked the x or pressed alt-F4...
#        print "close"
        """" chiwg
        UI_VERSION = 1   # increment this whenever the UI changes significantly
        _file = os.path.abspath(sys.argv[0])
        programname = os.path.basename(_file)
        programbase, _ = os.path.splitext(programname)  # extract basename and ext from filename
        settings = QSettings("nxp", programbase)    
        settings.setValue("geometry", self.saveGeometry())  # save window geometry
        settings.setValue("state", self.saveState(UI_VERSION))   # save settings (UI_VERSION is a constant you should increment when your UI changes significantly to prevent attempts to restore an invalid state.)
        settings.setValue("size", self.size())
        settings.setValue("pos", self.pos())
        """ #chiwg

#        settings.setValue("size", self.size(UI_VERSION))   # save settings (UI_VERSION is a constant you should increment when your UI changes significantly to prevent attempts to restore an invalid state.)

#        # save ui values, so they can be restored next time
#        settings.setValue("lineEditUser", self.lineEditUser.text());
#        settings.setValue("lineEditPass", self.lineEditPass.text());
#
#        settings.setValue("checkBoxReplace", self.checkBoxReplace.checkState());
#        settings.setValue("checkBoxFirst", self.checkBoxFirst.checkState());
#
#        settings.setValue("radioButton1", self.radioButton1.isChecked());

        sys.exit()  # prevents second call       

    def uiConnectSignals(self):
        # -----  Signals  ----
        self.ui.btnStart.clicked.connect(self.uiBtnPressed)
        self.ui.btnGlobalReset.clicked.connect(self.uiBtnPressed)
        self.ui.btnSleep.clicked.connect(self.uiBtnPressed)
        self.ui.btnRecover.clicked.connect(self.uiBtnPressed)
        self.ui.btnWakeup.clicked.connect(self.uiBtnPressed)
        self.ui.btnReset.clicked.connect(self.uiBtnPressed)
        self.ui.btnClear.clicked.connect(self.uiBtnPressed)
        self.ui.V48_Crash_Diag.clicked.connect(self.uiBtnPressed)
        self.ui.V48_Relay_Btn.clicked.connect(self.uiBtnPressed)
        self.ui.V48_Fan_Btn.clicked.connect(self.uiBtnPressed)
        self.ui.btnDatalog.clicked.connect(self.datalog)

        # ----- configuration -----
        self.ui.cbNoClusters.activated.connect(self.uiCbSelected)
        self.ui.cbCIDCurrent.activated.connect(self.uiCbSelected)
        self.ui.CT_Error.activated.connect(self.uiCbSelected)
      

        # ----- menue bar -----        
        self.ui.actionClose.triggered.connect(self.close)
        self.ui.actionDocu.triggered.connect(self.openDocu)        
        self.ui.actionWeb.triggered.connect(self.openWeb)        
        self.ui.actionInfo.triggered.connect(self.showInfo)

        self.ui.tableBMSview.clearStatusClicked.connect(self.clearStatus)
        self.ui.tableBMSview.statisticsClicked.connect(self.showStat)
        self.ui.tableBMSview.reverseRtermClicked.connect(self.reverseRterm)
        self.ui.tableConfigView.BitClicked.connect(self.ConfigTableUpdate)
        self.ui.tableStatusView.BitClicked.connect(self.StatusTableUpdate)
        self.ui.tableBMSview.appendText.connect(self.showLog)

        #selct refresh slave ID

    def commTimeout(self):
        """ triggered by EVB commTimeout sigCommTimeout
        """
        self.swident = 0
        self.packController.setBMSState(0)
        self.packController.setFaultState(0)

    def uiRefresh(self):
        """ updates the GUI elements periodically """
        #if self.evb.isConnected()<>None:  chiwg
            #self.evb.connected = (self.swident==swid) 
        #    self.evb.connected = (self.swident==swid) 

        self.uiUpdateStatusBar()

        #update GUI

        #enable/disable UI elemement depending on EVB connectionstate and BCCstate
        if self.thread.isConnected:
                self.ui.btnStart.setEnabled(True)
                self.ui.btnSleep.setEnabled(True)
                self.ui.btnWakeup.setEnabled(True)
                self.ui.btnReset.setEnabled(True)
                self.ui.tableBMSview.setEnabled(True)

        self.ui.tableBMSview.draw(self.noOfClusters)
        self.ui.tableConfigView.draw(self.noOfClusters)
        self.ui.tableStatusView.draw(self.noOfClusters)

        for cid in range(1, self.noOfClusters+1):  
            self.ui.tableBMSview.updateStatus(cid, self.clusters.getLastStatus(cid), self.clusters.getLastError(cid))
            self.ui.tableBMSview.update(cid, self.clusters.getGuid(cid), self.dbBMS[cid-1], showMean=True)    
            #self.ui.tableBMSview.RtermStatus(cid, self.clusters.getLastRterm(cid), self.clusters.getLastRterm(cid))
            self.ui.tableBMSview.updateCommCounter(cid, self.clusters.getCommCounters(cid))
            
        self.CT_OV = 0
        self.CT_UV = 0
        self.AN_OT = 0
        self.AN_UT = 0
        self.IC_OC = 0
        self.VStack = 0
        self.Current = 0
        if self.thread.isConnected:
            #HVBMS page
            if self.ui.tabWidget_1.currentIndex() == 1:
                for cid in range(1, len(HVBMSElementList)+1):  #10 BCCsin HVBMS demo window
                    for idx, _, _, coder, _ in BMSVIEW:
                        #voltage
                        if (idx >=  ID_MEAS_VPWR)  & (idx <=  ID_MEAS_VCT14):
                            value = self.dbBMS[cid-1][idx].getValue()
                            if value != None:
                                text = coder(decode, value, form   = '{:2.3f}V')
                                exec('self.ui.%s.setItem(%s, 0,  QTableWidgetItem(text))'%(HVBMSElementList[cid-1], idx - ID_MEAS_VPWR))
                        #temperature
                        if (idx >=  ID_MEAS_VAN0)  & (idx <=  ID_MEAS_ICTEMP):
                            value = self.dbBMS[cid-1][idx].getValue()
                            if value != None:
                                text = coder(decode, value) + 'C'
                                exec('self.ui.%s.setItem(%s, 1,  QTableWidgetItem(text))'%(HVBMSElementList[cid-1], idx - ID_MEAS_VAN0))
                        if (idx == ID_FAULT_STATUS1):
                            value = self.dbBMS[cid-1][idx].getValue()
                            if value != None:
                                self.CT_OV |= int(value) & 0x0002
                                self.CT_UV |= int(value) & 0x0001 
                                self.AN_OT |= int(value) & 0x0008
                                self.AN_UT |= int(value) & 0x0004
                                self.IC_OC |= int(value) & 0x0010
                        if (idx ==  ID_MEAS_VPWR) :
                            value = self.dbBMS[cid-1][idx].getValue()
                            if value != None:
                                self.VStack += float(codeVPwr(decode, value , form   = '{:5.3f}'))
                        if (idx == ID_MEAS_VCUR):
                            value = self.dbBMS[0][idx].getValue() #only slave #1 could measure current
                            if value != None:
                                self.Current = value
                text = '{:5.1f}V'.format(self.VStack)
                self.ui.BCCTotal1.setItem(0, 0,  QTableWidgetItem(text))
                text = codeCur(decode, self.Current , form   = '{:5.3f}uV')
                self.ui.BCCTotal1.setItem(2, 0,  QTableWidgetItem(text))
                text = '{:d}'.format(bool(self.IC_OC))
                self.ui.BCCTotal1.setItem(3, 0,  QTableWidgetItem(text))
                text = '{:d}'.format(bool(self.CT_OV))
                self.ui.BCCTotal1.setItem(4, 0,  QTableWidgetItem(text))
                text = '{:d}'.format(bool(self.CT_UV))
                self.ui.BCCTotal1.setItem(5, 0,  QTableWidgetItem(text))
                text = '{:d}'.format(bool(self.AN_OT))
                self.ui.BCCTotal1.setItem(6, 0,  QTableWidgetItem(text))                
                text = '{:d}'.format(bool(self.IC_OC))
                self.ui.BCCTotal1.setItem(7, 0,  QTableWidgetItem(text))                
                
            #HVBMS central page
            elif self.ui.tabWidget_1.currentIndex() == 2:#HVBMS center
                for cid in range(1, len(HVBMSCentElementList)+1):  #10 BCCsin HVBMS center demo window
                    for idx, _, _, coder, _ in BMSVIEW:
                        #voltage
                        if (idx >=  ID_MEAS_VPWR)  & (idx <=  ID_MEAS_VCT14):
                            value = self.dbBMS[cid-1][idx].getValue()
                            if value != None:
                                text = coder(decode, value, form   = '{:2.3f}V')
                                exec('self.ui.%s.setItem(%s, 0,  QTableWidgetItem(text))'%(HVBMSCentElementList[cid-1], idx - ID_MEAS_VPWR))
                        #temperature
                        if (idx >=  ID_MEAS_VAN0)  & (idx <=  ID_MEAS_ICTEMP):
                            value = self.dbBMS[cid-1][idx].getValue()
                            if value != None:
                                text = coder(decode, value) + 'C'
                                exec('self.ui.%s.setItem(%s, 1,  QTableWidgetItem(text))'%(HVBMSCentElementList[cid-1], idx - ID_MEAS_VAN0))
                        if (idx == ID_FAULT_STATUS1):
                            value = self.dbBMS[cid-1][idx].getValue()
                            if value != None:
                                self.CT_OV |= int(value) & 0x0002
                                self.CT_UV |= int(value) & 0x0001 
                                self.AN_OT |= int(value) & 0x0008
                                self.AN_UT |= int(value) & 0x0004
                                self.IC_OC |= int(value) & 0x0010
                        if (idx ==  ID_MEAS_VPWR) :
                            value = self.dbBMS[cid-1][idx].getValue()
                            if value != None:
                                self.VStack += float(codeVPwr(decode, value , form   = '{:5.3f}'))
                        if (idx == ID_MEAS_VCUR):
                            value = self.dbBMS[0][idx].getValue() #only slave #1 could measure current
                            if value != None:
                                self.Current = value
                text = '{:5.1f}V'.format(self.VStack)
                self.ui.BCCTotal2.setItem(0, 0,  QTableWidgetItem(text))
                text = codeCur(decode, self.Current , form   = '{:5.3f}uV')
                self.ui.BCCTotal2.setItem(2, 0,  QTableWidgetItem(text))
                text = '{:d}'.format(bool(self.IC_OC))
                self.ui.BCCTotal2.setItem(3, 0,  QTableWidgetItem(text))
                text = '{:d}'.format(bool(self.CT_OV))
                self.ui.BCCTotal2.setItem(4, 0,  QTableWidgetItem(text))
                text = '{:d}'.format(bool(self.CT_UV))
                self.ui.BCCTotal2.setItem(5, 0,  QTableWidgetItem(text))
                text = '{:d}'.format(bool(self.AN_OT))
                self.ui.BCCTotal2.setItem(6, 0,  QTableWidgetItem(text))                
                text = '{:d}'.format(bool(self.IC_OC))
                self.ui.BCCTotal2.setItem(7, 0,  QTableWidgetItem(text))                
            #HVBMS  BCC6 page
            elif self.ui.tabWidget_1.currentIndex() == 3:#HVBMS BCC6
                for cid in range(1, len(HVBMSBCC6ElementList)+1):  #10 BCCsin HVBMS demo window
                    for idx, _, _, coder, _ in BMSVIEW:
                        #voltage
                        if (idx >=  ID_MEAS_VPWR)  & (idx <=  ID_MEAS_VCT6):
                            value = self.dbBMS[cid-1][idx].getValue()
                            if value != None:
                                text = coder(decode, value, form   = '{:2.3f}V')
                                exec('self.ui.%s.setItem(%s, 0,  QTableWidgetItem(text))'%(HVBMSBCC6ElementList[cid-1], idx - ID_MEAS_VPWR))
                        #temperature
                        if (idx >=  ID_MEAS_VAN0)  & (idx <=  ID_MEAS_ICTEMP):
                            value = self.dbBMS[cid-1][idx].getValue()
                            if value != None:
                                text = coder(decode, value) + 'C'
                                exec('self.ui.%s.setItem(%s, 1,  QTableWidgetItem(text))'%(HVBMSBCC6ElementList[cid-1], idx - ID_MEAS_VAN0))
                        if (idx == ID_FAULT_STATUS1):
                            value = self.dbBMS[cid-1][idx].getValue()
                            if value != None:
                                self.CT_OV |= int(value) & 0x0002
                                self.CT_UV |= int(value) & 0x0001 
                                self.AN_OT |= int(value) & 0x0008
                                self.AN_UT |= int(value) & 0x0004
                                self.IC_OC |= int(value) & 0x0010
                        if (idx ==  ID_MEAS_VPWR) :
                            value = self.dbBMS[cid-1][idx].getValue()
                            if value != None:
                                self.VStack += float(codeVPwr(decode, value , form   = '{:5.3f}'))
                        if (idx == ID_MEAS_VCUR):
                            value = self.dbBMS[0][idx].getValue() #only slave #1 could measure current
                            if value != None:
                                self.Current = value
                text = '{:5.1f}V'.format(self.VStack)
                self.ui.BCCTotal3.setItem(0, 0,  QTableWidgetItem(text))
                text = codeCur(decode, self.Current , form   = '{:5.3f}uV')
                self.ui.BCCTotal3.setItem(2, 0,  QTableWidgetItem(text))
                text = '{:d}'.format(bool(self.IC_OC))
                self.ui.BCCTotal3.setItem(3, 0,  QTableWidgetItem(text))
                text = '{:d}'.format(bool(self.CT_OV))
                self.ui.BCCTotal3.setItem(4, 0,  QTableWidgetItem(text))
                text = '{:d}'.format(bool(self.CT_UV))
                self.ui.BCCTotal3.setItem(5, 0,  QTableWidgetItem(text))
                text = '{:d}'.format(bool(self.AN_OT))
                self.ui.BCCTotal3.setItem(6, 0,  QTableWidgetItem(text))             
                text = '{:d}'.format(bool(self.IC_OC))
                self.ui.BCCTotal3.setItem(7, 0,  QTableWidgetItem(text))                              
            #48VBMS page
            elif self.ui.tabWidget_1.currentIndex() == 4:#48VBMS
                for idx, _, _, coder, _ in BMSVIEW:
                    #voltage temperature
                    if (idx >=  ID_MEAS_VPWR)  & (idx <=  ID_MEAS_VCUR):
                        value = self.dbBMS[0][idx].getValue()
                        if value != None:
                            text = coder(decode, value, form   = '{:2.3f}')
                            exec('self.ui.BCCelement_4_1.setItem(%s, 0,  QTableWidgetItem(text))'%(idx - ID_MEAS_VPWR))
                    if (idx >=  ID_MEAS_VAN0)  & (idx <=  ID_MEAS_ICTEMP):
                        value = self.dbBMS[0][idx].getValue()
                        if value != None:
                            text = coder(decode, value) + 'C'
                            exec('self.ui.BCCelement_4_1.setItem(%s, 0,  QTableWidgetItem(text))'%(idx - ID_MEAS_VPWR))
                            if idx == ID_MEAS_VAN5:
                                value = (float(int(value)))* scale_vcell[0]*11
                                text1 = '{:.1f}'.format(value )
                                self.ui.V48_editS_1.setText(text1 + 'V')
                            elif idx == ID_MEAS_VAN6:
                                value = (float(int(value)))* scale_vcell[0]*11
                                text1 = '{:.1f}'.format(value )
                                self.ui.V48_editS_2.setText(text1 + 'V')


                    if (idx == ID_FAULT_STATUS1):
                        value = self.dbBMS[0][idx].getValue()
                        if value != None:
                            self.CT_OV = int(value) & 0x0002
                            self.CT_UV = int(value) & 0x0001 
                            self.AN_OT = int(value) & 0x0008
                            self.AN_UT = int(value) & 0x0004
                            self.IC_OC = int(value) & 0x0010
                    if (idx ==  ID_MEAS_VPWR) :
                        value = self.dbBMS[0][idx].getValue()
                        if value != None:
                            self.VStack += value
                    if (idx == ID_MEAS_VCUR):
                        value = self.dbBMS[0][idx].getValue() #only slave #1 could measure current
                        if value != None:
                            self.Current = value
                text = codeVPwr(decode, self.VStack , form   = '{:5.3f}V')
                self.ui.BCCTotal4.setItem(0, 0,  QTableWidgetItem(text))
                text = codeCur(decode, self.Current , form   = '{:5.3f}uV')
                self.ui.BCCTotal4.setItem(2, 0,  QTableWidgetItem(text))
                text = '{:d}'.format(bool(self.IC_OC))
                self.ui.BCCTotal4.setItem(3, 0,  QTableWidgetItem(text))
                text = '{:d}'.format(bool(self.CT_OV))
                self.ui.BCCTotal4.setItem(4, 0,  QTableWidgetItem(text))
                text = '{:d}'.format(bool(self.CT_UV))
                self.ui.BCCTotal4.setItem(5, 0,  QTableWidgetItem(text))
                text = '{:d}'.format(bool(self.AN_OT))
                self.ui.BCCTotal4.setItem(6, 0,  QTableWidgetItem(text))        
                text = '{:d}'.format(bool(self.IC_OC))
                self.ui.BCCTotal4.setItem(7, 0,  QTableWidgetItem(text))
                value = self.dbBMS[0][ID_LV_NTC1].getValue()
                if value != None:
                    value = (float(int(value))) * 5 / 4095
                    text1 = '{:.3f}'.format(value)
                    self.ui.V48_editS_3.setText(text1 + 'V')
                value = self.dbBMS[0][ID_LV_NTC2].getValue()
                if value != None:
                    value = (float(int(value))) * 5 / 4095
                    text1 = '{:.3f}'.format(value)
                    self.ui.V48_editS_4.setText(text1 + 'V')
                value = self.dbBMS[0][ID_LV_CRASH].getValue()
                if value != None:
                    value = (float(int(value))) * 5 / 4095 *(10+5.1)/5.1
                    text1 = '{:.1f}'.format(value)
                    self.ui.V48_editS_5.setText(text1 + 'V')
            #BJB page
            elif self.ui.tabWidget_1.currentIndex() == 5:#BJB
                for idx in range(ID_VSENSE0,  ID_Coulomb+1):
                    value = self.dbBMS[0][idx].getValue()
                    if value == None:
                        continue
                    stat = self.dbBMS[0][idx].getStat() 
                    value = stat['mean']
                    if idx == ID_ISENSE:
                        text = '{:08X}'.format(int(value))
                    else:
                        text = '{:04X}'.format(int(value))
                        
                    self.ui.tableviewBJB.setItem(idx - ID_VSENSE0, 1, QTableWidgetItem(text))

                    if idx == ID_TCHIP:
                        text1 = '{:.1f}'.format(value * 0.008 - 273 ) #8mk/LSB
                    elif idx == ID_VSENSE0:
                        text1 = '{:.1f}'.format(value * 0.25  ) #0.25mV/LSB
                        self.ui.BJB_editF.setText(text1+ 'mV')
                    elif idx == ID_VSENSE1:
                        text1 ='{:.1f}'.format(value * 0.25  )#0.25mV/LSB
                        self.ui.BJB_editG.setText(text1+ 'mV')
                    elif idx == ID_VSENSE2:
                        text1 = '{:.1f}'.format(value * 0.5  )#0.5mV/LSB
                        self.ui.BJB_editD.setText(text1+ 'mV')
                    elif idx == ID_VSENSE3:
                        text1 = '{:.1f}'.format(value * 1.0  )#1mV/LSB
                        self.ui.BJB_editE.setText(text1+ 'mV')
                    elif (idx == ID_TSENSE0):
                        text1 = '{:.1f}'.format(value * 25.0 / 1000 )#25uV/LSB, PTBx configured as voltage sensing
                    elif (idx == ID_TSENSE1):
                        text1 = '{:.1f}'.format(value * 25.0 / 1000 )#25uV/LSB, PTBx configured as voltage sensing
                        self.ui.BJB_editC.setText(text1 + 'mV')
                    elif (idx == ID_TSENSE2):
                        text1 = '{:.1f}'.format(value * 25.0 / 1000 )#25uV/LSB, PTBx configured as voltage sensing
                        self.ui.BJB_editA.setText(text1+ 'mV')
                    elif (idx == ID_TSENSE3):
                        text1 = '{:.1f}'.format(value * 25.0 / 1000 )#25uV/LSB, PTBx configured as voltage sensing
                        self.ui.BJB_editB.setText(text1+ 'mV')
                    elif (idx == ID_TSENSE4) :
                        
                        mV = value * 19.0 / 1000
                        ohm = 384000 * mV /(2500-mV)
                        temperature = self.BJBTable.CalTemperature(ohm)
                        text1 = '{:.1f}'.format(value * 19.0 / 1000 )#19uV/LSB, PTB4 configured as Temperature sensing
                        text2 ='{:.1f}'.format(temperature)
                        self.ui.BJB_editTemp.setText(text2 + 'C')
                    elif idx == ID_ISENSE:
                        text1 = '{:.1f}'.format(value * 1.0  )#1mA/LSB
                        self.ui.BJB_editCurrent.setText(text1+ 'mA')
                    else:
                        text1 = '{:.1f}'.format(value / 1.0)
                    self.ui.tableviewBJB.setItem(idx - ID_VSENSE0, 0, QTableWidgetItem(text1))
                    
                    for c in range(self.ui.tableviewBJB.columnCount()-1):
                        self.ui.tableviewBJB.item(idx - ID_VSENSE0, c).setTextAlignment(Qt.AlignRight | Qt.AlignVCenter)
            #14VBMS page
            elif self.ui.tabWidget_1.currentIndex() == 6:#14VBMS
                for idx, _, _, coder, _ in BMSVIEW:
                    value = self.dbBMS[0][idx].getValue()
                    if value != None:         
                        if idx == ID_MEAS_VCT6:
                            text = coder(decode, value, form   = '{:2.3f}V')
                            self.ui.BCCelement_6_1.setItem(0, 0,  QTableWidgetItem(text))
                        elif idx == ID_MEAS_VAN1:
                            text = coder(decode, value) + 'C'
                            self.ui.BCCelement_6_1.setItem(1, 0,  QTableWidgetItem(text))
                        elif idx == ID_MEAS_VCT5:
                            text = coder(decode, value, form   = '{:2.3f}V')
                            self.ui.BCCelement_6_2.setItem(0, 0,  QTableWidgetItem(text))
                        elif idx == ID_MEAS_VAN2:
                            text = coder(decode, value) + 'C'
                            self.ui.BCCelement_6_2.setItem(1, 0,  QTableWidgetItem(text))                            
                        elif idx == ID_MEAS_VCT2:
                            text = coder(decode, value, form   = '{:2.3f}V')
                            self.ui.BCCelement_6_3.setItem(0, 0,  QTableWidgetItem(text))
                        elif idx == ID_MEAS_VAN3:
                            text = coder(decode, value) + 'C'
                            self.ui.BCCelement_6_3.setItem(1, 0,  QTableWidgetItem(text))  
                        elif idx == ID_MEAS_VCT1:
                            text = coder(decode, value, form   = '{:2.3f}V')
                            self.ui.BCCelement_6_4.setItem(0, 0,  QTableWidgetItem(text))
                        elif idx == ID_MEAS_VAN4:
                            text = coder(decode, value) + 'C'
                            self.ui.BCCelement_6_4.setItem(1, 0,  QTableWidgetItem(text))
                        elif idx == ID_MEAS_VCUR:
                            text = coder(decode, value, form   = '{:2.1f}uV')
                            self.Current = value
                            self.ui.BCCelement_6_5.setItem(0, 0,  QTableWidgetItem(text))             
                        elif idx == ID_MEAS_VAN5:
                            text = coder(decode, value) + 'C'
                            self.ui.BCCelement_6_5.setItem(1, 0,  QTableWidgetItem(text))    
                        elif idx == ID_MEAS_VPWR:
                            text = coder(decode, value, form   = '{:2.3f}V')
                            self.ui.BCCTotal6.setItem(0, 0,  QTableWidgetItem(text))       
                        elif idx == ID_MEAS_ICTEMP:
                            text = coder(decode, value) + 'C'
                            self.ui.BCCTotal6.setItem(8, 0,  QTableWidgetItem(text))              
                        elif idx == ID_FAULT_STATUS1:
                            self.CT_OV |= int(value) & 0x0002
                            self.CT_UV |= int(value) & 0x0001 
                            self.AN_OT |= int(value) & 0x0008
                            self.AN_UT |= int(value) & 0x0004
                            self.IC_OC |= int(value) & 0x0010
                text = codeCur(decode, self.Current , form   = '{:5.3f}uV')
                self.ui.BCCTotal6.setItem(2, 0,  QTableWidgetItem(text))
                text = '{:d}'.format(bool(self.IC_OC))
                self.ui.BCCTotal6.setItem(3, 0,  QTableWidgetItem(text))
                text = '{:d}'.format(bool(self.CT_OV))
                self.ui.BCCTotal6.setItem(4, 0,  QTableWidgetItem(text))
                text = '{:d}'.format(bool(self.CT_UV))
                self.ui.BCCTotal6.setItem(5, 0,  QTableWidgetItem(text))
                text = '{:d}'.format(bool(self.AN_OT))
                self.ui.BCCTotal6.setItem(6, 0,  QTableWidgetItem(text))                
                text = '{:d}'.format(bool(self.IC_OC))
                self.ui.BCCTotal6.setItem(7, 0,  QTableWidgetItem(text))   
            

        # schedule next update
            # 10 BCCs in WBMS page
            elif self.ui.tabWidget_1.currentIndex() == 7:
                for cid in range(1, len(WBMSElementList)+1):  #10 BCCsin WBMS demo window
                    for idx, _, _, coder, _ in BMSVIEW:
                        #voltage
                        if (idx >=  ID_MEAS_VPWR)  & (idx <=  ID_MEAS_VCT14):
                            value = self.dbBMS[cid-1][idx].getValue()
                            if value != None:
                                text = coder(decode, value, form   = '{:2.3f}V')
                                exec('self.ui.%s.setItem(%s, 0,  QTableWidgetItem(text))'%(WBMSElementList[cid-1], idx - ID_MEAS_VPWR))
                        #temperature
                        if (idx >=  ID_MEAS_VAN0)  & (idx <=  ID_MEAS_ICTEMP):
                            value = self.dbBMS[cid-1][idx].getValue()
                            if value != None:
                                text = coder(decode, value) + 'C'
                                exec('self.ui.%s.setItem(%s, 1,  QTableWidgetItem(text))'%(WBMSElementList[cid-1], idx - ID_MEAS_VAN0))
                        if(idx == ID_COMM_ERR):
                            value = self.dbBMS[cid - 1][idx].getValue()
                            if(value == 0):
                                exec ('self.ui.%s.setStyleSheet(("border-image:url(:/rcMain/BLE.png)"))' % (WBMSSignalList[cid - 1]))
                            else:
                                exec ('self.ui.%s.setStyleSheet(("border-image:url(:/rcMain/BLE_fail.png)"))' % (WBMSSignalList[cid - 1]))
            elif self.ui.tabWidget_1.currentIndex() == 9:
                if self.SelectCID != 0:
                    self.ui.tableConfigView.update(self.dbBMS[self.SelectCID - 1])
                    self.ui.tableStatusView.update(self.dbBMS[self.SelectCID - 1])
        self.timer.singleShot(uiRefreshRate, self.uiRefresh)
        
    def uiInitStatusBar(self):
        """ initialises the status bar
        """
        self.statusComms = QLabel("CAN port closed")
        self.statusComms.setFixedSize(self.statusComms.sizeHint())
        self.statusComms.setAlignment(Qt.AlignCenter)

#         self.statusLoop = QLabel("--")
        self.statusBytes = QLabel("Bytes waiting: -")

        self.statusSW  = QLabel("SW: ---")        
        self.statusInterface = QLabel("Interface: ---")        
        self.statusEVB = QLabel("EVB: ---")        

        self.ui.statusbar.addWidget(self.statusComms, 1)
#         self.ui.statusbar.addWidget(self.statusLoop, 2)
        self.ui.statusbar.addWidget(self.statusBytes, 2)
        self.ui.statusbar.addWidget(self.statusSW, 3)
        self.ui.statusbar.addWidget(self.statusInterface, 4)
        self.ui.statusbar.addWidget(self.statusEVB, 5)


    def uiUpdateStatusBar(self):
        """ updates the status bar 
        """

        if self.thread.isConnected==None:            # comport closed
            self.statusComms.setText("CAN port closed")
            self.statusComms.setStyleSheet("QLabel { color : black;}")
            self.statusSW.setText("SW: -.-- (ID----)")
            self.statusInterface.setText("Interface: ---")
            self.statusEVB.setText("EVB: ---")
            return

        ver, sub = self.packController.getSWVersion()
        intf = self.packController.getInterfaceType()
        evb = self.packController.getEvbType()

        if self.thread.isConnected:
            self.statusComms.setText("connected")
            self.statusComms.setStyleSheet("QLabel { background-color : Lime; }")
            self.statusSW.setText("SW: {}.{:02} (ID{:04X})".format(ver, sub, self.swident))
            self.statusInterface.setText("Interface: {}".format(Interface[int(intf)]))
            self.statusEVB.setText("EVB: {}".format(TYPEEVB[int(evb)]))
            return

        self.statusComms.setText("not connected")
        self.statusComms.setStyleSheet("QLabel { background-color : OrangeRed;}")
        self.statusSW.setText("SW: {}.{:02} (ID{:04X})".format(ver, sub, self.swident))
        self.statusInterface.setText("Interface: ---")
        self.statusEVB.setText("EVB: ---")
    def updateStatusBar2(self, param):
        self.statusBytes.setText("Bytes waiting: {0}".format(param))
    def updateRxData(self, cid, idx, data, text):
        """ handles new data received from worker (serial port)"""
        # handle pack controller data
        if cid>0 and cid <= MAX_NO_CLUSTER:

            if cid > self.noOfClusters:
                if(idx>=ID_MEAS_VPWR)&(idx<=ID_MEAS_VCT14):
                    self.noOfClusters = cid;
            #pick out error status.....
            if idx==ID_COMM_ERR:
                #update clusters
                self.clusters.setCommCounter(cid, data)
                self.dbBMS[cid - 1][idx].setValue(data)
                #data log
                self.datalogAdd(cid, self.dbBMS[cid-1], ErrorMsg[data])
            #pick out node list data
            elif idx==ID_RTERM:
                self.clusters.setRtermStatus(cid,data)

            elif idx==ID_CHIPREV:
                chiprev = data
                if chiprev==None:
                    chiprev = 0
                chip = CHIP[int(chiprev/256)]
                rev = int(chiprev%256/16)
                subrev = int(chiprev%16)
                self.clusters.setChip(cid, chip)
                self.clusters.setChipRev(cid, rev)
                self.clusters.setChipSubRev(cid, subrev)

            elif idx==ID_GUID:
                self.clusters.setGuid(cid, str(text))
                
            # PackController / global data
            elif idx==ID_SW:

                self.swident = int(data/65536) 
                swver = (data%65536)/256
                swsub = (data%65536)%256
                self.packController.setSWVersion(swver, swsub)
            elif idx==ID_EVB:
                self.packController.setEvbType(data)
                self.ui.cbEvbType.setCurrentIndex(data)

            elif idx==ID_INTERFACE:
                self.packController.setInterfaceType(data)
                self.ui.cbIntType.setCurrentIndex(data)

            elif idx==ID_NOCLUSTER:
                self.packController.setNoOfClusters(data)
                self.ui.cbNoClusters.setCurrentIndex(data-1)

            elif idx==ID_CIDCURRMEAS:
                self.ui.cbCIDCurrent.setCurrentIndex(data)

            elif idx==ID_MEASPERIOD:
                self.ui.cbPeriod.setCurrentIndex(data)

            elif idx==ID_MODE:
                self.ui.cbMode.setCurrentIndex(data)

            elif idx==ID_TESTMODE:
                self.ui.cbTest.setCurrentIndex(data)

            elif idx==ID_BMSSTATE:
                self.packController.setBMSState(data)
            elif (idx >=  ID_MEAS_SM1) & (idx <=  ID_MEAS_SMX_OTHER):
                self.dbBMS[cid-1][idx].setValue(data)
            else:
                self.dbBMS[cid-1][idx].setValue(data)
                #waveform refresh
                if (idx >=  ID_MEAS_VCT1) & (idx <=  ID_MEAS_VCT14)  & self.waveform.waveExist:
                    value = float(codeVCell(decode, data, form   = '{:2.3f}'))
                    self.waveform.sigWaveData.emit(cid-1, idx - ID_MEAS_VCT1,  value)
        elif cid ==0:#it's a BJB msg, shared with CID = 1 buffer
            self.dbBMS[cid][idx].setValue(data)
            

    def clearStatus(self, cid):
        self.clusters.clearLastError(cid)
        self.clusters.clearLastStatus(cid)

    def reverseRterm(self, cid):
        RtermStatus = self.clusters.reverseRterm(cid)
        RtermStatus =  RtermStatus
        self.thread.send([0xCF,0x00,(RtermStatus>>8)&0xFF, (RtermStatus>>0)&0xFF ])

    def ConfigTableUpdate(self,addr,bit):
        bitindex = 15-bit
        for addrindex in range(len(self.ui.tableConfigView.CtrlRegMap)):
            if addr == addrindex:
                addrID = self.ui.tableConfigView.CtrlRegMap[addr][0][0]
                RegValue = self.dbBMS[self.SelectCID-1][addrID].getValue()
                RegValue = int(RegValue)
        if((self.ui.tableConfigView.CtrlRegMap[addr][bit+1][1] == 0) or (self.ui.tableConfigView.CtrlRegMap[addr][bit+1][1] == 2)):
            if ( (RegValue&(2**bitindex))!=0):
                RegValue = RegValue - (2**bitindex)
            elif( (RegValue&(2**bitindex))==0):
                RegValue = RegValue + (2 ** bitindex)
        self.thread.send([0xCD, self.ui.tableConfigView.CtrlRegMap[addr][0][2], (RegValue >> 8) & 0xFF, (RegValue >> 0) & 0xFF])
    def StatusTableUpdate(self,addr,bit):
        bitindex = 15-bit
        for addrindex in range(len(self.ui.tableStatusView.StatusRegMap)):
            if addr == addrindex:
                addrID = self.ui.tableStatusView.StatusRegMap[addr][0][0]
                RegValue = self.dbBMS[self.SelectCID-1][addrID].getValue()
                RegValue = int(RegValue)
        if((self.ui.tableStatusView.StatusRegMap[addr][bit+1][1] == 2)):
            if ( (RegValue&(2**bitindex))!=0):
                RegValue = RegValue - (2**bitindex)
        self.thread.send([0xCC, self.ui.tableStatusView.StatusRegMap[addr][0][2], (RegValue >> 8) & 0xFF, (RegValue >> 0) & 0xFF])

    def showStat(self, cid):
        #for  waveform
        self.waveform.createCurve(self.noOfClusters)
        self.waveform.CIDItem.activated.connect(self.waveform.chooseMonitorCID)
        self.waveform.show()
        if self.waveform.waveExist == 0:
            self.waveform.startMonitor()

    def uiCbSelected(self, index):
        sender = self.sender()

        if sender== self.ui.cbNoClusters:
            self.NoClusters = index+1
            print 'self.evb.sendHWConfiguration(3, self.NoClusters)  '
        elif sender == self.ui.cbCIDCurrent:
            self.SelectCID = self.ui.cbCIDCurrent.currentIndex() 
            self.thread.send([0xCE, 0x00,0x00,self.SelectCID])            
        elif (sender == self.ui.CT_Error):
            self.ui.tableBMSview.config(self.ui.CT_Error.currentIndex() +1)
    def showLog(self, msg):
        self.ui.textInfo.append(msg)
        self.ui.textInfo.verticalScrollBar().setValue(self.ui.textInfo.verticalScrollBar().maximum())
    def uiBtnPressed(self):
        sender = self.sender()

        if sender == self.ui.btnStart:
            if self.ui.btnStart.text()=="&Start":
                #open port
                self.ui.btnStart.setText("&Stop")
                self.ui.cbCOMPort.setEnabled(False)
                self.ui.cbCOMBaud.setEnabled(False)
                p = int(self.ui.cbCOMPort.currentText()[3:])-1
                b = int(self.ui.cbCOMBaud.currentText())
                #self.evb.connect(p, b) chiwg
                self.thread.openport(0, b)
                self.thread.exiting = False
            else:
                #close port
                self.ui.btnStart.setText("&Start")
                self.ui.cbCOMPort.setEnabled(True)
                self.ui.cbCOMBaud.setEnabled(True)
                #self.evb.disconnect() chiwg
                self.packController.setSWVersion(0,0)
                self.thread.exiting = True
                self.thread.closeport()
                self.thread.flush()

        elif sender == self.ui.btnRecover:
            #self.ui.tableBMSview.clearContents()
            self.thread.send([0xC2, int(self.ui.cbNoClusters.currentText()), 0x00,0x00])
            self.noOfClusters = 0
        elif sender == self.ui.btnGlobalReset:
            self.thread.send([0xC1, 0x00, 0x00,0x00])
        elif sender == self.ui.btnReset:
            self.thread.send([0xC3, 0x00, 0x00,0x00])
        elif sender == self.ui.btnSleep:
            self.thread.send([0xC4, 0x00, 0x00,0x00])
        elif sender == self.ui.btnWakeup:
            self.thread.send([0xC5, 0x00, 0x00,0x00])
 #       elif sender == self.ui.btnPcConfStore:
 #           self.thread.send([0xC6, 0x00, 0x00,0x00])
        elif sender == self.ui.btnClear:
            self.ui.textInfo.clear()  
            self.clusters.clearCommCounters()
        elif sender == self.ui.V48_Crash_Diag:
            if self.ui.V48_Crash_Diag.text() == "Crash":
                self.ui.V48_Crash_Diag.setText("NoCrash")
                self.ui.V48_editS_5.setStyleSheet("background-color: rgb(255, 0, 0);")
                self.thread.send([0xC7, 0x01, 0x00, 0x00])
            else:
                self.ui.V48_Crash_Diag.setText("Crash")
                self.ui.V48_editS_5.setStyleSheet("background-color: rgb(255, 255, 255);")
                self.thread.send([0xC7, 0x00, 0x00, 0x00])
        elif sender == self.ui.V48_Relay_Btn:
             if self.ui.V48_Relay_Btn.text() == "Relay On":
                 self.EswitchInput = self.EswitchInput | 0x01
                 self.ui.V48_Relay_Btn.setText("Relay Off")
             else:
                 self.EswitchInput = self.EswitchInput & 0xFE
                 self.ui.V48_Relay_Btn.setText("Relay On")
             self.thread.send([0xC8, self.EswitchInput, 0x00, 0x00])
        elif sender == self.ui.V48_Fan_Btn:
             if self.ui.V48_Fan_Btn.text() == "Fan On":
                 self.EswitchInput = self.EswitchInput | 0x10
                 self.ui.V48_Fan_Btn.setText("Fan Off")
             else:
                 self.EswitchInput = self.EswitchInput & 0xEF
                 self.ui.V48_Fan_Btn.setText("Fan On")
             self.thread.send([0xC8, self.EswitchInput, 0x00, 0x00])
       
    def datalog(self):

        if self.dlog.isEnabled():
            # stop logging
            self.ui.cbLog.setEnabled(True)
            self.eventfile.close()
            self.dlog.stop()
            self.ui.btnDatalog.setText("start log")

        else:
            #start logging
            self.logformat = self.ui.cbLog.currentText()
            self.dlog.start(self.logformat)
            self.ui.cbLog.setEnabled(False)
            self.ui.btnDatalog.setText("stop log")
            self.datestring = time.strftime("%d%b%Y_%H-%M-%S", time.localtime(time.time()))
            self.eventfile = open('bcctest-event-{}.0'.format(self.datestring), "wb", 0)  #0 unbuffered

            #info header
            info = []
            info.append(('Date', time.strftime("'%d%b%Y", time.localtime(time.time()) )))
            info.append(('Time', time.strftime("'%H:%M:%S", time.localtime(time.time()))))
            info.append(('SW', "{} V{}.{:02}".format(title, version, subversion)))

#@todo            
#            info.append(('Logging', "{} N={}".format(self.dlog.logFormat(), N_BUFFER)))
            info.append(('Logging', "{} N={}".format(self.dlog.logFormat(), 1)))

#            for cid in range(1, MAX_NO_CLUSTER+1):  
            for cid in range(1, self.packController.getNoOfClusters()+1):  
                info.append(("GUID-CID-{:02}".format(cid), self.clusters.getGuid(cid)))
            info.append('')
            info.append('')
            info.append('')
            self.dlog.addInfo(info)

            #header line
            info = []
            if self.logdriftformat:
                info.append(('t=0 values',''))
                self.dlog.addInfo(info)

            self.datalogNewHeader()
            self.tstart = time.time()


    def datalogNewHeader(self):
            header = []
            if self.logdriftformat:
                range_ = self.packController.getNoOfClusters()+1
            else:
                range_ = 1+1

            for _cid in range(1, range_):           
                header.append("Time")
                header.append("CID")
                header.append("Error")

                parser = DATALOG_PARSER
                for _, _, header_phy, header_raw, _, _  in parser:
                    if self.dlog.logFormat()=='raw':
                        header.append(header_raw)
                    if self.dlog.logFormat()=='physical':
                        header.append(header_phy)

#            if self.logdriftformat:
            self.dlog.addHeader(header)

    def datalogNewEntry(self, ttext):
        """ enters one datapoint (stats) every 1 hour 
        """
        print "datalog entry" 
        initial_entry = (ttext=="0000:01:00")
        print "initial entry = ", initial_entry
        for cid in range(1, MAX_NO_CLUSTER+1):
            self.datalogAddStat(cid, self.bat[cid-1], ttext, initial_entry)            

        if initial_entry:
            self.excelrefrow = self.dlog.getCell()[1]-1
            print "self.excelrefrow:", self.excelrefrow                
            #header line
            info = []
            info.append(('',''))
            info.append(('',''))
            info.append(('',''))
            info.append(('Relative Values',''))
            self.dlog.addInfo(info)
            self.datalogNewHeader()
            for cid in range(1, MAX_NO_CLUSTER+1):
                self.datalogAddStat(cid, self.bat[cid-1], ttext, False)            


    def datalogAddStat(self, cid, bat,ttext, initial_entry=False):

        print "Todo"
        NoCells = 6

        blank_data = not(self.bat[cid-1].DataAvail())
        new_line = (cid== MAX_NO_CLUSTER)

        if self.dlog.isEnabled()==False:
            return

        values = []
#        values.append(ttext[:4])
        values.append(ttext)
        values.append('CID-{}'.format(cid))
        values.append('Ok') #errorcode

        if self.dlog.logFormat()=='raw':
            if blank_data:
                for n in range(5+self.NoCells+7):
                    values.append('')
            else:        
                x = 3+(cid-1)*(15+self.NoCells)
                y = self.excelrefrow
                #v0 Vstack 
                n,_,_,mean,_ = bat.getVpwrStat(scale=False)
                if initial_entry:
                    values.append('{}'.format(mean))                
                else:
                    values.append('={}-{}'.format(mean, self.dlog.xy2ExcelAZ(x, y)))                
                    x = x+1    
                #Vcells
                for n in range(self.NoCells):
                    n,_,_,mean,_ = bat.getVcellStat(n+1, False)
                    if initial_entry:
                        values.append('{}'.format(mean))    
                    else:
                        values.append('={}-{}'.format(mean, self.dlog.xy2ExcelAZ(x, y)))                
                        x = x+1    

                #v current        
                n,_,_,mean,_ = bat.getVcurStat(scale=False)
                if initial_entry:
                    values.append('{}'.format(mean))                
                else:
                    values.append('={}-{}'.format(mean, self.dlog.xy2ExcelAZ(x, y)))                
                    x = x+1    
                #t chip
                n,_,_,mean,_ = bat.getTchipStat(scale=False)
                if initial_entry:
                    values.append('{}'.format(mean))                
                else:
#                    values.append('={}-{}'.format(mean, self.dlog.xy2ExcelAZ(x, y)))                
                    values.append('{}'.format(mean))                
                    x = x+1    
                #vbg
                n,_,_,mean,_ = bat.getVbgADC1AStat(scale=False)
                if initial_entry:
                    values.append('{}'.format(mean))                
                else:
                    values.append('={}-{}'.format(mean, self.dlog.xy2ExcelAZ(x, y)))                
                    x = x+1    

                n,_,_,mean,_ = bat.getVbgADC1BStat(scale=False)
                if initial_entry:
                    values.append('{}'.format(mean))                
                else:
                    values.append('={}-{}'.format(mean, self.dlog.xy2ExcelAZ(x, y)))                
                    x = x+1    
                #V Anx
                for n in range(0, 7):            
                    n,_,_,mean,_ = bat.getVanStat(anno=n, scale=False)
                    if initial_entry:
                        values.append('{}'.format(mean))                
                    else:
                        values.append('={}-{}'.format(mean, self.dlog.xy2ExcelAZ(x, y)))                
                        x = x+1    

        if self.dlog.logFormat()=='physical':

            if blank_data:
                for n in range(5+self.NoCells+7):
                    values.append('')
            else:        
                x = 3+(cid-1)*(15+self.NoCells)
                y = self.excelrefrow
                #v0 Vstack 
                n,_,_,mean,_ = bat.getVpwrStat(scale=True)
                if initial_entry:
                    values.append('{}'.format(1000*mean))                
                else:
                    values.append('={}-{}'.format(1000*mean, self.dlog.xy2ExcelAZ(x, y)))                
                    x = x+1    

                #Vcells
                for n in range(self.NoCells):
                    n,_,_,mean,_ = bat.getVcellStat(n+1, True)
                    if initial_entry:
                        values.append('{}'.format(1000*mean))    
                    else:
                        values.append('={}-{}'.format(1000*mean, self.dlog.xy2ExcelAZ(x, y)))                
                        x = x+1    

                #v current        
                n,_,_,mean,_ = bat.getVcurStat(scale=True)
                if initial_entry:
                    values.append('{}'.format(1000*mean))    
                else:
                    values.append('={}-{}'.format(1000*mean, self.dlog.xy2ExcelAZ(x, y)))                
                    x = x+1    

                #t chip
                n,_,_,mean,_ = bat.getTchipStat(scale=True)
                if initial_entry:
                    values.append('{}'.format(mean))    
                else:
#                    values.append('={}-{}'.format(mean, self.dlog.xy2ExcelAZ(x, y)))                
                    values.append('{}'.format(mean))                
                    x = x+1    

                #vbg
                n,_,_,mean,_ = bat.getVbgADC1AStat(scale=True)
                if initial_entry:
                    values.append('{}'.format(1000*mean))    
                else:
                    values.append('={}-{}'.format(1000*mean, self.dlog.xy2ExcelAZ(x, y)))                
                    x = x+1    

                n,_,_,mean,_ = bat.getVbgADC1BStat(scale=True)
                if initial_entry:
                    values.append('{}'.format(1000*mean))    
                else:
                    values.append('={}-{}'.format(1000*mean, self.dlog.xy2ExcelAZ(x, y)))                
                    x = x+1    

                #V Anx
                for n in range(0, 7):            
                    n,_,_,mean,_ = bat.getVanStat(anno=n, scale=True)
                    if initial_entry:
                        values.append('{}'.format(mean))    
                    else:
                        values.append('={}-{}'.format(mean, self.dlog.xy2ExcelAZ(x, y)))                
                        x = x+1    

        self.dlog.addData(values, newline=new_line)


    def datalogAdd(self, cid, dbBMS, msg):
        if self.dlog.isEnabled()==False:
            return
        self.dt = int((time.time()-self.tstart)*1000)  #ms since start
        values = []
        values.append('{}'.format(self.dt))
        values.append('CID-{}'.format(cid))
#        values.append('Ok') #errorcode
        values.append(msg) #errorcode
        if msg==ErrorMsg[0]:
            parser = DATALOG_PARSER
            for idx, _, _, _, coder, _  in parser:
                rawdata = self.dbBMS[cid-1][idx].getValue()

                if self.dlog.logFormat()=='physical':
                    if rawdata==None:
                        text = ""
                    else:                            
                        text = coder(decode, rawdata)
                else:
                    if rawdata==None:
                        text = ""
                    else:                            
                        text = str(int(rawdata))
                values.append(text)

        self.dlog.addData(values)


    def eventlogAdd(self, msg):
        if self.dlog.isEnabled():
            self.dt = int((time.time()-self.tstart)*1000)  #ms since start
            self.eventfile.write('{}'.format(self.dt))
            self.eventfile.write(sep)
            self.eventfile.write(msg)        
            self.eventfile.write('\n')        


    def openWeb(self):
        new = 2
        url = link_to_web
        webbrowser.open(url,new=new)

    def openDocu(self):
        # open an HTML file on my own (Windows) computer
        # path (absolute) to docu is ../link_to_GUI_docu relative to this exetable
        # e.g. c:\program files\freescale\
        new = 2

        p = os.path.abspath(os.path.dirname(sys.argv[0])) 
#        print p        
#        x = os.path.split(p)

        x1 = link_to_GUI_docu
#        print x1
#        p1 = os.path.join(x[0], x1)
        p1 = os.path.join(p, x1)
#        print p1

        url = "file://"+p1
        print "url:", url
        webbrowser.open(url,new=new)        

    def showInfo(self):
        """ Shows About window with revision history
        """
def main():
    app = QApplication(sys.argv)
    form = ApplicationWindow()
    form.show()
    app.exec_()

if __name__ == "__main__":
    main()
