from distutils.core import setup

import py2exe
import sys 
import ctypes

sys.path.append("C:\\Python27\\Lib\\site-packages\\xy\\dist\\Microsoft.VC90.CRT")
sys.path.append("C:\\Python27\\Lib\\site-packages\\numpy\\core")
sys.path.append("C:\\Python27\\Lib\\site-packages\\numpy\\.libs")

setup(
	# The first three parameters are not required, if at least a
	# 'version' is given, then a versioninfo resource is built from
	# them and added to the executables.
	version = "1.30.0",
	description = "MC3377x Test GUI",
	name = "MC3377x Test GUI",
	author="NXP Semiconductors",
	windows =  [{
            "script": "BCC_EMCTest_GUI.py"
        }],
#   data_files = [('', ['HVBMS.ico', 'NXP_logo.png'])], 
	options = {'py2exe': {
			"bundle_files":1, 
			"compressed": True,
			"includes" : ["sip"], 
			"excludes":["Tkconstants", "Tkinter", "tcl", "matplotlib", "h5py", "email", "json", "bsddb", "compiler", "multiprocessing", "nose", "PIL", "pywin", "pyreadline"],
        "dll_excludes": [ "mswsock.dll", "powrprof.dll", "MSVCP90.dll" ]
        }
    },
    zipfile = None
)
