

NTCS0603E3103MT = [
#resistor / TCR / temperature
(243448,  -6.06,  -40), 
(180772,  - 5.85,  -35), 
(135623,  - 5.65,  -30), 
(102751,  - 5.46,  -25), 
(78576,  - 5.28,  -20), 
(60623,  - 5.10,  -15), 
(47168,  - 4.94,  -10), 
(36995,  - 4.78,  -5), 
(29240,  - 4.63,  0), 
(23280,  - 4.49,  5), 
(18664,  - 4.35,  10), 
(15064,  - 4.22,  15), 
(12236,  - 4.10,  20), 
(10000,  - 3.98,  25), 
(8220.3,  - 3.86,  30), 
(6795.2,  - 3.75,  35), 
(5647.3,  - 3.65,  40), 
(4717.5,  - 3.55,  45), 
(3960.3,  - 3.45,  50),
(3340.4,  - 3.36,  55),
(2830.3,  - 3.27,  60),
(2408.6,  - 3.18,  65),
(2058.4,  - 3.10,  70),
(1766.2,  - 3.02,  75), 
(1521.4,  - 2.95,  80), 
(1315.4,  - 2.87,  85), 
(1141.4,  - 2.80,  90), 
(993.91,  - 2.63,  95), 
(868.35,  - 2.67,  100), 
(761.11,  - 2.61,  105), 
(669.19,  - 2.54,  110), 
(590.14,  - 2.48,  115), 
(521.94,  - 2.43,  120), 
(462.92,  - 2.37,  125), 
(411.68,  - 2.32,  130), 
(367.08,  - 2.27,  135), 
(328.14,  - 2.22,  140), 
(294.05,  - 2.17,  145), 
(264.12,  - 2.12,  150), 
]


class NTC_table(object):

    def __init__(self):
        self.TableLong = len(NTCS0603E3103MT)

    def CalTemperature(self, Res):
        TableLast = self.TableLong - 1
        if(Res >= NTCS0603E3103MT[0][0]):
            deltaT = (NTCS0603E3103MT[0][0] - Res)/NTCS0603E3103MT[0][0]/NTCS0603E3103MT[0][1]*100
            TemResult = NTCS0603E3103MT[0][2] - deltaT
            return TemResult
        elif(Res <= NTCS0603E3103MT[TableLast][0]):
            deltaT = (Res - NTCS0603E3103MT[TableLast][0] )/NTCS0603E3103MT[TableLast][0]/NTCS0603E3103MT[TableLast][1]*100
            TemResult = NTCS0603E3103MT[TableLast][2] + deltaT
            return 0
        else:
            Index = self.SearchRes(Res)
            deltaT1 = (Res - NTCS0603E3103MT[Index][0] ) / NTCS0603E3103MT[Index][0] / NTCS0603E3103MT[Index][1] * 100
            deltaT2 = (NTCS0603E3103MT[Index+1][0] - Res)/NTCS0603E3103MT[Index+1][0]/NTCS0603E3103MT[Index+1][1] * 100
            if(deltaT1 <= deltaT2):
                TemResult = NTCS0603E3103MT[Index][2] + deltaT1
            else:
                TemResult = NTCS0603E3103MT[Index+1][2] - deltaT2
            return TemResult

    def SearchRes(self, ResData):
        st = 0
        ed = self.TableLong

        while(st<ed):
            m = (int)((st+ed)/2)
            if((ResData <= NTCS0603E3103MT[m][0])&(ResData > NTCS0603E3103MT[m+1][0])):
                break;
            if(ResData > NTCS0603E3103MT[m][0]):
                ed = m
            elif(ResData < NTCS0603E3103MT[m][0]):
                st = m

        if(st > ed):
            return 0

        return m
