# -*- coding: utf-8 -*-
"""
// ----------------------------------------------------------------------------
//  Copyright (c) 2015, Freescale Semiconductor, Inc.
//  All rights reserved.
// 
//  Redistribution and use in source and binary forms, with or without modification,
//  are permitted provided that the following conditions are met:
// 
//  o Redistributions of source code must retain the above copyright notice, this list
//    of conditions and the following disclaimer.
// 
//  o Redistributions in binary form must reproduce the above copyright notice, this
//    list of conditions and the following disclaimer in the documentation and/or
//    other materials provided with the distribution.
// 
//  o Neither the name of Freescale Semiconductor, Inc. nor the names of its
//    contributors may be used to endorse or promote products derived from this
//    software without specific prior written permission.
// 
//  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
//  ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
//  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
//  DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR
//  ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
//  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
//  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
//  ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
//  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
//  SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
// ----------------------------------------------------------------------------
"""

#import numpy as np
from NTC_Table import *


BJBTable = NTC_table()
# ------------------------------------------------------------------------------
#BCC parameters (datasheet 4.0)
#         y =   m * x +    c
scale_vpwr   = [ 2.44414E-3     , 0       ]      #V/LSB
scale_vcell  = [152.58789E-6     , 0       ]      #V/LSB
scale_vth    = [128*152.58789E-6 , 0       ]   #V/LSB
scale_vcur   = [0.6E-3          , 0       ]      #mV/LSB 
scale_cur   =  [0.6E-2          , 0       ]      #mA/LSB @ 100uV/A
scale_tIC    = [0.032           , -273.15 ]#degC/LSB
#scale_t1     = [0.1525925E-3 , 0 ]     #V/LSB
scale_tth    = [5.0/255         , 0       ]    #V/LSB
scale_ntc    = [5.0/32767       , 0       ]    #V/LSB
scale_ntcth  = [5.0/1023        , 0       ]    #V/LSB

VCOM = 5.0          #V

decode = 0      # decode ADC data -> physical value
encode = 1      # encode physical value -> ADC data

def bitMask(bitlen):
    """ returns a mask for no. of bitlen, e.g. bitlen = 7 => 0x7F
    """
    return 2**bitlen - 1

# ------------------------------------------------------------------------------
# for temperature from -40C....125C  
##ext. components
#Rpre = 10000        #Ohm
#t0   = 25           #C nom. temperature value     
#Rntc = 10e3         #Ohm ntc nominal value at t0
#Bntc = 3900         #K beta value for ntc in temperature range
#Kelvin = 273.15    #C for C to K calculations    
ADC_vs_Temp = [
           #        deg C    ,   ADC value N , 
                  (        -40,      32058   ) ,
                  (        -39,      31998   ) ,
                  (        -38,      31934   ) ,
                  (        -37,      31867   ) ,
                  (        -36,      31795   ) ,
                  (        -35,      31719   ) ,
                  (        -34,      31639   ) ,
                  (        -33,      31554   ) ,
                  (        -32,      31465   ) ,
                  (        -31,      31371   ) ,
                  (        -30,      31272   ) ,
                  (        -29,      31167   ) ,
                  (        -28,      31058   ) ,
                  (        -27,      30942   ) ,
                  (        -26,      30821   ) ,
                  (        -25,      30694   ) ,
                  (        -24,      30561   ) ,
                  (        -23,      30422   ) ,
                  (        -22,      30276   ) ,
                  (        -21,      30124   ) ,
                  (        -20,      29965   ) ,
                  (        -19,      29799   ) ,
                  (        -18,      29626   ) ,
                  (        -17,      29446   ) ,
                  (        -16,      29259   ) ,
                  (        -15,      29065   ) ,
                  (        -14,      28863   ) ,
                  (        -13,      28654   ) ,
                  (        -12,      28437   ) ,
                  (        -11,      28213   ) ,
                  (        -10,      27982   ) ,
                  (        -9,      27743   ) ,
                  (        -8,      27496   ) ,
                  (        -7,      27242   ) ,
                  (        -6,      26981   ) ,
                  (        -5,      26712   ) ,
                  (        -4,      26436   ) ,
                  (        -3,      26153   ) ,
                  (        -2,      25863   ) ,
                  (        -1,      25567   ) ,
                  (        0,      25263   ) ,
                  (        1,      24954   ) ,
                  (        2,      24638   ) ,
                  (        3,      24317   ) ,
                  (        4,      23990   ) ,
                  (        5,      23658   ) ,
                  (        6,      23320   ) ,
                  (        7,      22978   ) ,
                  (        8,      22632   ) ,
                  (        9,      22282   ) ,
                  (        10,      21928   ) ,
                  (        11,      21570   ) ,
                  (        12,      21210   ) ,
                  (        13,      20848   ) ,
                  (        14,      20483   ) ,
                  (        15,      20117   ) ,
                  (        16,      19749   ) ,
                  (        17,      19381   ) ,
                  (        18,      19012   ) ,
                  (        19,      18642   ) ,
                  (        20,      18274   ) ,
                  (        21,      17905   ) ,
                  (        22,      17538   ) ,
                  (        23,      17172   ) ,
                  (        24,      16809   ) ,
                  (        25,      16447   ) ,
                  (        26,      16087   ) ,
                  (        27,      15730   ) ,
                  (        28,      15377   ) ,
                  (        29,      15025   ) ,
                  (        30,      14679   ) ,
                  (        31,      14336   ) ,
                  (        32,      13997   ) ,
                  (        33,      13662   ) ,
                  (        34,      13333   ) ,
                  (        35,      13007   ) ,
                  (        36,      12686   ) ,
                  (        37,      12371   ) ,
                  (        38,      12060   ) ,
                  (        39,      11755   ) ,
                  (        40,      11456   ) ,
                  (        41,      11161   ) ,
                  (        42,      10873   ) ,
                  (        43,      10590   ) ,
                  (        44,      10313   ) ,
                  (        45,      10041   ) ,
                  (        46,      9775   ) ,
                  (        47,      9515   ) ,
                  (        48,      9260   ) ,
                  (        49,      9011   ) ,
                  (        50,      8769   ) ,
                  (        51,      8531   ) ,
                  (        52,      8298   ) ,
                  (        53,      8074   ) ,
                  (        54,      7853   ) ,
                  (        55,      7637   ) ,
                  (        56,      7428   ) ,
                  (        57,      7223   ) ,
                  (        58,      7023   ) ,
                  (        59,      6831   ) ,
                  (        60,      6641   ) ,
                  (        61,      6458   ) ,
                  (        62,      6278   ) ,
                  (        63,      6105   ) ,
                  (        64,      5936   ) ,
                  (        65,      5771   ) ,
                  (        66,      5612   ) ,
                  (        67,      5455   ) ,
                  (        68,      5303   ) ,
                  (        69,      5156   ) ,
                  (        70,      5013   ) ,
                  (        71,      4875   ) ,
                  (        72,      4741   ) ,
                  (        73,      4610   ) ,
                  (        74,      4483   ) ,
                  (        75,      4360   ) ,
                  (        76,      4238   ) ,
                  (        77,      4122   ) ,
                  (        78,      4009   ) ,
                  (        79,      3899   ) ,
                  (        80,      3792   ) ,
                  (        81,      3688   ) ,
                  (        82,      3589   ) ,
                  (        83,      3492   ) ,
                  (        84,      3398   ) ,
                  (        85,      3305   ) ,
                  (        86,      3217   ) ,
                  (        87,      3131   ) ,
                  (        88,      3044   ) ,
                  (        89,      2963   ) ,
                  (        90,      2886   ) ,
                  (        91,      2809   ) ,
                  (        92,      2735   ) ,
                  (        93,      2660   ) ,
                  (        94,      2590   ) ,
                  (        95,      2523   ) ,
                  (        96,      2456   ) ,
                  (        97,      2394   ) ,
                  (        98,      2331   ) ,
                  (        99,      2269   ) ,
                  (        100,      2212   ) ,
                  (        101,      2154   ) ,
                  (        102,      2100   ) ,
                  (        103,      2045   ) ,
                  (        104,      1993   ) ,
                  (        105,      1943   ) ,
                  (        106,      1894   ) ,
                  (        107,      1847   ) ,
                  (        108,      1800   ) ,
                  (        109,      1756   ) ,
                  (        110,      1711   ) ,
                  (        111,      1667   ) ,
                  (        112,      1625   ) ,
                  (        113,      1587   ) ,
                  (        114,      1548   ) ,
                  (        115,      1509   ) ,
                  (        116,      1473   ) ,
                  (        117,      1437   ) ,
                  (        118,      1401   ) ,
                  (        119,      1368   ) ,
                  (        120,      1334   ) ,
                  (        121,      1304   ) ,
                  (        122,      1274   ) ,
                  (        123,      1243   ) ,
                  (        124,      1213   ) ,
                  (        125,      1185   ) ,
              ]


def codeHex16(coding, x):
    bstart = 0
    bits   = 16
    form   = '0x{:04X}'
    mask = bitMask(bits)
    if coding==decode:
#        value= int( (x>>bstart)&mask )
        value= int( x)
        return form.format(value)       #returns string

    if coding==encode:        
        data = int(x)
        data = (data&mask)<<bstart
        return data                     # returns int value

def codeHex32(coding, x):
    bstart = 0
    bits   = 32
    form   = '0x{:08X}'
    mask = bitMask(bits)
    if coding==decode:
#        value= int( (x>>bstart)&mask )
        value= int(x )
        return form.format(value)       #returns string

    if coding==encode:        
        data = int(x)
        data = (data&mask)<<bstart
        return data                     # returns int value


def codeVPwr(coding, x, form   = '{:2.3f}'):
    bstart = 0
    bits   = 15
#    form   = '{:2.3f}'
    mask = bitMask(bits)
    if coding==decode:
        value= float( (int(x)>>bstart)&mask )
#        value= float(x)
        value= scale_vpwr[0]*value + scale_vpwr[1]      
        return form.format(value)       #returns string

    if coding==encode:        
        data = int( (x-scale_vpwr[1])/scale_vpwr[0])
        data = (data&mask)<<bstart
        return data                     # returns int value


def codeVCur(coding, x, form='{:2.3f}'):
    bstart = 0
    bits   = 19
#    form   = '{:2.3f}'
    mask = bitMask(bits)
    if coding==decode:
#        print type(x)
#        if type(x)==np.uint32:
#            if x&0x80000000:      x = x-0xFFFFFFFF-1  #signed 32 bit 2er comp
        value= float( x )
        value= scale_vcur[0]*value + scale_vcur[1]      
        return form.format(value)       #returns string

    if coding==encode:        
        data = int( (x-scale_vcur[1])/scale_vcur[0])
        data = (data&mask)<<bstart
        return data                     # returns int value


def codeCur(coding, x, form='{:2.3f}'):
    bstart = 0
    bits   = 19
#    form   = '{:2.3f}'
    mask = bitMask(bits)
    if coding==decode:
#        print type(x)
#        if type(x)==np.uint32:
#            if x&0x80000000:      x = x-0xFFFFFFFF-1  #signed 32 bit 2er comp
        value= float( x )
        value= scale_cur[0]*value + scale_cur[1]      
        return form.format(value)       #returns string

    if coding==encode:        
        data = int( (x-scale_cur[1])/scale_cur[0])
        data = (data&mask)<<bstart
        return data                     # returns int value


def codeVCell(coding, x, form   = '{:2.3f}'):
    bstart = 0
    bits   = 15
#    form   = '{:2.3f}'
    mask = bitMask(bits)
    if coding==decode:
        value= float( (int(x)>>bstart)&mask )
#        value= float( x )
        value= scale_vcell[0]*value + scale_vcell[1]      
        return form.format(value)       #returns string

    if coding==encode:        
        data = int( (x-scale_vcell[1])/scale_vcell[0])
        data = (data&mask)<<bstart
        return data                     # returns int value


def codeThVolt(coding, x):
    bstart = 0
    bits   = 8
    form   = '{:2.3f}'
    mask = bitMask(bits)        
    if coding==decode:
#        value= float( (x>>bstart)&mask )
        value= float(x)
        value= scale_vth[0]*value  +  scale_vth[1]   
        return form.format(value)       #returns string

    if coding==encode:        
        data = int((x-scale_vth[1])/scale_vth[0])
        if data>mask: data = mask
#        data = (data&mask)<<bstart
        return data                     # returns int value


def codeICDegC(coding, x):
    bstart = 0
    bits   = 15
    form   = '{:2.0f}'
    mask = bitMask(bits)
    if coding==decode:
        value= float( (int(x)>>bstart)&mask )
#        value= float( x )
        value= scale_tIC[0]*value  +  scale_tIC[1]   
        return form.format(value)       #returns string

    if coding==encode:        
        data = int((x-scale_vth[1])/scale_vth[0])
        data = (data&mask)<<bstart
        return data                     # returns int value
    

def codeAnDegC(coding, x):
    bstart = 0
    bits   = 15
    mask = bitMask(bits)
    form   = '{:2.0f}'
    temps = [y[0] for y in ADC_vs_Temp]    
    N_ADC = [y[1] for y in ADC_vs_Temp]    
    if coding==decode:
        
#        data = (int(x)>>bstart) & mask 
#        value = 200             #dummy value
#        for c in temps:
#            if data<= N_ADC[temps.index(c)]:
#                value = c-1

        #in latest schematic, the hardware use 6.8K resistor from VCOM instead of 10K
        x= scale_vcell[0]*x + scale_vcell[1]  
        y = 6800 * x / (5 - x) 
        value = BJBTable.CalTemperature(y) #NTC resistor has replaced in TJN RefDesign Board
        return form.format(value)
    
    if coding==encode:        
        if x< -40: x = -40
        if x> 125: x = 125
        i = temps.index(int(x)) 
        return (N_ADC[i])       

def codeThDegC(coding, x):
    form   = '{:2.0f}'
    temps = [y[0] for y in ADC_vs_Temp]    
    N_ADC = [y[1] for y in ADC_vs_Temp]    
    if coding==decode:
        data = int(x)<<5          #10 MSB bits of the 15bit value "right aligned" 
        value = 200             #dummy value
        for c in temps:
            if data<= N_ADC[temps.index(c)]:
                value = c
        return form.format(value)
    
    if coding==encode:        
        if x< -40: x = -40
        if x> 125: x = 125
        i = temps.index(int(x)) 
        return (N_ADC[i] >>5)       
    
