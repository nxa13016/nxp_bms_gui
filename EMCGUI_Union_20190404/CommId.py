# -*- coding: utf-8 -*-
# ------------------------------------------------------------------------------
# Copyright (c) 2015, NXP Semiconductors.
# All rights reserved.
#  
# Redistribution and use in source and binary forms, with or without modification,
# are permitted provided that the following conditions are met:
#  
# o Redistributions of source code must retain the above copyright notice, this list
#   of conditions and the following disclaimer.
#  
# o Redistributions in binary form must reproduce the above copyright notice, this
#   list of conditions and the following disclaimer in the documentation and/or
#   other materials provided with the distribution.
#  
# o Neither the name of NXP Semiconductors nor the names of its
#   contributors may be used to endorse or promote products derived from this
#   software without specific prior written permission.
#  
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
# ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
# WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
# DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR
# ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
# (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
# LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
# ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
# (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
# SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
# ------------------------------------------------------------------------------

"""
Communication Indexes

@author: R51406
@version: 

 ID coding:
 1       SW ID      (32bit)
 SWID   = 0x00011000     #1.10
 10      Fault/BCC state
 100...   Status bits
 200...   Control bits
 300...   Measurements (value readonly)
 400...   Thresholds (static but modifyable values)
 500...   Diagnostic data
 
 600..727  Read Register scripting tool
 
 3840..4095  (0xF00..0XFFF)  PC -> GUI
 
"""

TID_DOWNLOAD_START     = 4091   #data is u16 noOfByte
TID_DOWNLOAD_DATA      = 4092   #data is u32  (4 bytes) 
TID_DOWNLOAD_STOP      = 4093   #data is u16 checksum (not used yet)

TID_SEREQUEST          = 4094   #data is u8
TID_COMMAND            = 4095   #data is u8   0: Reset, 1: GotoSleep, 2: Wakeup
  
# --------------------

ID_MAX                 = 599   #1023

# command ID definition for J638

ID_VSENSE0    =  40
ID_VSENSE1    =  41
ID_VSENSE2    =  42
ID_VSENSE3    =  43
ID_TSENSE0     =   44
ID_TSENSE1      =  45
ID_TSENSE2      =  46
ID_TSENSE3      =  47
ID_TSENSE4      =  48  
ID_ISENSE        =  49
ID_TCHIP           =  50
ID_SOC              = 51
ID_Coulomb      = 52


#command ID for MC3377x

ID_SW                  = 0
ID_INTERFACE           = 1
ID_EVB                 = 2
ID_CHIPREV             = 3
ID_NOCLUSTER           = 4
ID_NOCELL              = 5
ID_MODE                = 6
ID_CIDCURRMEAS         = 7
ID_MEASPERIOD          = 8
ID_TESTMODE            = 9
                       
ID_BMSSTATE            = 10
ID_FAULTPIN            = 11
ID_COMM_ERR            = 12
ID_MESSAGE             = 14
ID_VCOM                = 15
ID_EVENT               = 16
ID_READREG_DONE        = 34     #used to trigger excel data logging

ID_BCCSTATE            = 17
ID_TIMESTAMP           = 18

# for scripting tool:
ID_STATE               = 19
#ID_LINENR              = 20  
ID_PROGCOUNTER         = 20
ID_READBIT             = 21
ID_CONTEXT_A           = 22
ID_CONTEXT_B           = 23
ID_CONTEXT_C           = 24
ID_CONTEXT_D           = 25
ID_CONTEXT_NODES       = 26
ID_CONTEXT_LSTRES      = 27
ID_CONTEXT_CCR         = 28
ID_CONTEXT_NODE        = 29
ID_CONTEXT_NAVERAGE    = 30
ID_CONTEXT_OP1         = 31
ID_CONTEXT_OP2         = 33




                       
ID_GUID                = 32
ID_RTERM               = 35
                       
ID_BASE_STATUS         = 100

ID_CELL_OV             = 100
ID_CELL_UV             = 101
ID_CB_OPEN_FAULT       = 102
ID_CB_SHORT_FAULT      = 103
ID_CB_DRV_STATUS       = 104
ID_GPIO_STATUS         = 105
ID_AN_OT_UT            = 106
ID_GPIO_SHORT_OPEN     = 107
ID_I_STATUS            = 108
ID_COM_STATUS          = 109
ID_FAULT_STATUS1       = 110
ID_FAULT_STATUS2       = 111
ID_FAULT_STATUS3       = 112
ID_MEAS_ISENSE2        = 113

ID_INIT                = 200
ID_SYS_CFG_GLOBAL      = 201
ID_SYS_CFG1            = 202
ID_SYS_CFG2            = 203
ID_SYS_DIAG            = 204
ID_ADC_CFG             = 205
ID_OV_UV_EN            = 206
ID_GPIO_CFG1           = 207
ID_GPIO_CFG2           = 208
ID_GPIO_STS            = 209
ID_FAULT_MASK1         = 210
ID_FAULT_MASK2         = 211
ID_FAULT_MASK3         = 212
ID_WAKEUP_MASK1        = 213
ID_WAKEUP_MASK2        = 214
ID_WAKEUP_MASK3        = 215
ID_                    = 217
ID_                    = 218
ID_ADC2_OFFSET_COMP    = 219
ID_CB_CFG_1            = 220
ID_CB_CFG_2            = 221
ID_CB_CFG_3            = 222
ID_CB_CFG_4            = 223
ID_CB_CFG_5            = 224
ID_CB_CFG_6            = 225
ID_CB_CFG_7            = 226
ID_CB_CFG_8            = 227
ID_CB_CFG_9            = 228
ID_CB_CFG_10           = 229
ID_CB_CFG_11           = 230
ID_CB_CFG_12           = 231
ID_CB_CFG_13           = 232
ID_CB_CFG_14           = 233

ID_EEPROM_CTRL         = 235



ID_MEAS_VPWR           = 300
ID_MEAS_VCT1           = 301
ID_MEAS_VCT2           = 302
ID_MEAS_VCT3           = 303
ID_MEAS_VCT4           = 304
ID_MEAS_VCT5           = 305
ID_MEAS_VCT6           = 306
ID_MEAS_VCT7           = 307
ID_MEAS_VCT8           = 308
ID_MEAS_VCT9           = 309
ID_MEAS_VCT10          = 310
ID_MEAS_VCT11          = 311
ID_MEAS_VCT12          = 312
ID_MEAS_VCT13          = 313
ID_MEAS_VCT14          = 314
ID_MEAS_VCUR           = 315
ID_MEAS_VAN0           = 316
ID_MEAS_VAN1           = 317
ID_MEAS_VAN2           = 318
ID_MEAS_VAN3           = 319
ID_MEAS_VAN4           = 320
ID_MEAS_VAN5           = 321
ID_MEAS_VAN6           = 322
ID_MEAS_ICTEMP         = 323
ID_MEAS_VBG_DIAG_ADC1A = 324
ID_MEAS_VBG_DIAG_ADC1B = 325
ID_CC_NB_SAMPLE        = 326
ID_COULOMB_CNT         = 327
ID_SILIVON_REV         = 328
ID_DED_HAMMING_CNT     = 329

ID_FAULT1              = 382 
ID_FAULT2              = 383 
ID_FAULT3              = 384 

ID_MEAS_SM1        = 360
ID_MEAS_SM2        = 361
ID_MEAS_SM3        = 362
ID_MEAS_SM4        = 363
ID_MEAS_SM5        = 364
ID_MEAS_SM6        = 365
ID_MEAS_SM7        = 366
ID_MEAS_SM36       = 367
ID_MEAS_SM40       = 368
ID_MEAS_SM41       = 369
ID_MEAS_SMX_AN       = 370
ID_MEAS_SMX_OTHER       = 371

ID_TH_ALL_CT_OV        = 400 
ID_TH_ALL_CT_UV        = 401
ID_TH_CT1_OV           = 402
ID_TH_CT1_UV           = 403
ID_TH_CT2_OV           = 404
ID_TH_CT2_UV           = 405
ID_TH_CT3_OV           = 406
ID_TH_CT3_UV           = 407
ID_TH_CT4_OV           = 408
ID_TH_CT4_UV           = 409
ID_TH_CT5_OV           = 410
ID_TH_CT5_UV           = 411
ID_TH_CT6_OV           = 412
ID_TH_CT6_UV           = 413
ID_TH_CT7_OV           = 414
ID_TH_CT7_UV           = 415
ID_TH_CT8_OV           = 416
ID_TH_CT8_UV           = 417
ID_TH_CT9_OV           = 418
ID_TH_CT9_UV           = 419
ID_TH_CT10_OV          = 420
ID_TH_CT10_UV          = 421
ID_TH_CT11_OV          = 422
ID_TH_CT11_UV          = 423
ID_TH_CT12_OV          = 424
ID_TH_CT12_UV          = 425
ID_TH_CT13_OV          = 426
ID_TH_CT13_UV          = 427
ID_TH_CT14_OV          = 428
ID_TH_CT14_UV          = 429
ID_TH_AN0_OT           = 430
ID_TH_AN1_OT           = 431
ID_TH_AN2_OT           = 432
ID_TH_AN3_OT           = 433
ID_TH_AN4_OT           = 434
ID_TH_AN5_OT           = 435
ID_TH_AN6_OT           = 436
ID_TH_AN0_UT           = 437
ID_TH_AN1_UT           = 438
ID_TH_AN2_UT           = 439
ID_TH_AN3_UT           = 440
ID_TH_AN4_UT           = 441
ID_TH_AN5_UT           = 442
ID_TH_AN6_UT           = 443
ID_TH_ISENSE_OC        = 444
ID_TH_COULOMB_CNT      = 445

ID_LV_NTC1             =499
ID_LV_NTC2             =498
ID_LV_CRASH            =497

ID_GROUP_VCELL = (
    ID_MEAS_VCT1  ,
    ID_MEAS_VCT2  ,
    ID_MEAS_VCT3  ,
    ID_MEAS_VCT4  ,
    ID_MEAS_VCT5  ,
    ID_MEAS_VCT6  ,
    ID_MEAS_VCT7  ,
    ID_MEAS_VCT8  ,
    ID_MEAS_VCT9  ,
    ID_MEAS_VCT10 ,
    ID_MEAS_VCT11 ,
    ID_MEAS_VCT12 ,
    ID_MEAS_VCT13 ,
    ID_MEAS_VCT14 
 )

ID_GROUP_SHOWMEAN = (
    ID_MEAS_VPWR  ,
    ID_MEAS_VCT1  ,
    ID_MEAS_VCT2  ,
    ID_MEAS_VCT3  ,
    ID_MEAS_VCT4  ,
    ID_MEAS_VCT5  ,
    ID_MEAS_VCT6  ,
    ID_MEAS_VCT7  ,
    ID_MEAS_VCT8  ,
    ID_MEAS_VCT9  ,
    ID_MEAS_VCT10 ,
    ID_MEAS_VCT11 ,
    ID_MEAS_VCT12 ,
    ID_MEAS_VCT13 ,
    ID_MEAS_VCT14 ,
    ID_MEAS_VCUR  ,
    ID_MEAS_VBG_DIAG_ADC1A ,
    ID_MEAS_VBG_DIAG_ADC1B ,
    
  )


ID_GROUP_DIAGNOSE = (
    ID_MEAS_SM1 , 
    ID_MEAS_SM2 , 
    ID_MEAS_SM3 , 
    ID_MEAS_SM4 , 
    ID_MEAS_SM5 , 
    ID_MEAS_SM6 , 
    ID_MEAS_SM7 , 
    ID_MEAS_SM36 , 
    ID_MEAS_SM40 , 
    ID_MEAS_SM41 , 
    ID_MEAS_SMX_AN , 
    ID_MEAS_SMX_OTHER
 )



ID_GROUP_CONTEXT1 = (
    ID_PROGCOUNTER      ,
    ID_CONTEXT_CCR      ,
    ID_CONTEXT_LSTRES   ,
    ID_CONTEXT_NAVERAGE ,
#    ID_CONTEXT_OP1      ,
#    ID_CONTEXT_OP2      ,
)

ID_GROUP_CONTEXT2 = (
    ID_CONTEXT_A        ,
    ID_CONTEXT_B        ,
    ID_CONTEXT_C        ,
    ID_CONTEXT_D        ,
    ID_CONTEXT_NODE     ,
    ID_CONTEXT_NODES    ,
)
