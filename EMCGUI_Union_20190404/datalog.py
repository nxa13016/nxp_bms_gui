"""
// ----------------------------------------------------------------------------
//  Copyright (c) 2015, Freescale Semiconductor, Inc.
//  All rights reserved.
// 
//  Redistribution and use in source and binary forms, with or without modification,
//  are permitted provided that the following conditions are met:
// 
//  o Redistributions of source code must retain the above copyright notice, this list
//    of conditions and the following disclaimer.
// 
//  o Redistributions in binary form must reproduce the above copyright notice, this
//    list of conditions and the following disclaimer in the documentation and/or
//    other materials provided with the distribution.
// 
//  o Neither the name of Freescale Semiconductor, Inc. nor the names of its
//    contributors may be used to endorse or promote products derived from this
//    software without specific prior written permission.
// 
//  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
//  ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
//  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
//  DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR
//  ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
//  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
//  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
//  ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
//  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
//  SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
// ----------------------------------------------------------------------------
"""
import time
#import numpy as np
import locale
# Logfile config
SEP  = "\t"
SEPLINE = "\n"
#germany
#SEPDEC = ","        #decimal point seperator
#france
#SEPDEC = "."        #decimal point seperator
dataFileName  = "datalog"


class Datalog(object):
    """This class handles the dataloging"""
    raw = 0
    physical = 1
#    LOGFORMAT = ['raw', 'physical']
    LOGFORMLIST= ('raw', 'physical')
    datafile = None
    logformat = raw
    dec_pt_chr = ''  
    
    def __init__(self):
        # users preferred locale settings for other categories by calling
        locale.setlocale(locale.LC_ALL, '')
        
        # gets the local setting from windows
        self.dec_pt_chr = locale.localeconv()['decimal_point']
        
#        print "datalog - init"
        self.datafile = None
        self.datalog_enabled = False

    def decimalPoint(self):
        return self.dec_pt_chr
        
    def isEnabled(self):
        return self.datalog_enabled

    def logFormat(self):
        return self.LOGFORMLIST[self.logformat]

    def start(self, logformat):
        """ starts the logging, creates the files, etc. """
        self.stop()
        self.logformat = self.LOGFORMLIST.index(logformat)
       
#        print "datalog start - logformat: {}".format(self.LOGFORMLIST[self.logformat])
#        self.btnDatalog.setText("stop log")
        self.datestring = time.strftime("%d%b%Y_%H-%M-%S", time.localtime(time.time()))
#        self.datafile  = open('{}-{}.0'.format(dataFileName, self.datestring), "wb")
        self.datafile  = open('{}-{}.0'.format(dataFileName, self.datestring), "wb", 0) #0 unbuffered
        self.tstart = time.time()
        self.datalog_enabled = True
        self.cell = [0,0]

    def nextCell(self):    
        self.cell[0] = self.cell[0] + 1

    def newLineCell(self):    
        self.cell[1] = self.cell[1] + 1
        self.cell[0] = 0

    def getCell(self):
        return self.cell

    def stop(self):
#        print "datalog - stop"
        if self.datafile is None:
            return  # nothing to do
        self.datafile.close()
        self.datalog_enabled = False
        self.datafile = None

    def addInfo(self, info):
        """ Adds the info type list to the file. """
#        print "datalog - dataAddInfo"
        if self.datafile is None:      return
        # info part    
        for line in info:
            for entry in line:
                self.datafile.write(entry)
                self.datafile.write(SEP)
                self.nextCell()
            self.datafile.write(SEPLINE)
            self.newLineCell()            
#        self.datafile.write(SEPLINE)
#        self.newLineCell()            


    def addHeader(self, header):
#        print "datalog - addHeader"
        if self.datafile is None:       
            return
        # header part    
        for entry in header:
            self.datafile.write(entry)
            self.datafile.write(SEP)
            self.nextCell()
 
        self.datafile.write(SEPLINE)
        self.newLineCell()            
 

    def addData(self, data, newline=True):
#        print "datalog - addData"

        if self.datafile is None:     return
        self.dt = int((time.time()-self.tstart)*1000)  #ms since start

        # measurements
        for entry in data:
            self.datafile.write(str(entry).replace(".", self.dec_pt_chr))    #replace decimal symbol
            self.datafile.write(SEP)
            self.nextCell()
        
        if newline==True:
            self.datafile.write(SEPLINE)
            self.newLineCell()            
        
        
    #      '=MAX(B$14:B$100000)'  
    def xy2ExcelAZ(self, x, y):
        """ xy2ExcelAZ()
        returns a reference to a cell based on its x/y position. E.g. 0,0 => "A1" 
        """
        text = ""
        N = 26
        EXCELCOL = [chr(a) for a in range(65, 65+N)]
        while x/N:
            text =EXCELCOL[x%N]+text
            x=x/N-1
        else:
            text = EXCELCOL[x%N]+text
            
        return text+"{:}".format(y+1)            
        
# ------------------------------------------------------------------------------
if __name__ == "__main__":
    dlog = Datalog()
    print "Decimal point: '{:}'".format(dlog.decimalPoint())
    print dlog.logFormat()

