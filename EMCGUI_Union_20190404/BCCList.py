# -*- coding: utf-8 -*-
"""
Created on Wed Sep 07 17:10:22 2016

@author: R51406
"""

import sys
from PyQt4.QtCore import *                          #@UnusedWildImport (Eclipse)
from PyQt4.QtGui import *                           #@UnusedWildImport (Eclipse)
import numpy as np

CHIP = ["unknown", "MC33771", "MC33772"]
COMMCOUNTER = 5
MAX_NO_CLUSTER = 50

# -----------------------------------------------------------------------------        
class PackController(object):
    
    def __init__ (self):
        self._swver = 0
        self._swsubver = 0
        self._interfacetype = 0
        self._evbtype = 0
        self._bmsstate = 0
        self._faultstate = 0
        self._noofclusters = 0
        
    def setSWVersion(self, ver, subver):
        self._swver = ver
        self._swsubver = subver

    def getSWVersion(self):
        return (self._swver, self._swsubver)

    def setInterfaceType(self, interfacetype):
        self._interfacetype = interfacetype

    def getInterfaceType(self):
        return self._interfacetype    

    def setEvbType(self, evbtype):
        self._evbtype = evbtype

    def getEvbType(self):
        return self._evbtype
        
    def setBMSState(self, state):
        self._bmsstate = state    

    def getBMSState(self):
        return self._bmsstate

    def setFaultState(self, state):
        self._faultstate = state
        
    def getFaultState(self):
        return self._faultstate
        
    def setNoOfClusters(self, noofclusters):
        self._noofclusters = noofclusters

    def getNoOfClusters(self):
        return self._noofclusters


class Clusters(object):
    
    def __init__ (self, NoOfNodes):
        self.NoOfNodes = int(NoOfNodes)
        guidtype = np.dtype((str, 12))
        chiptype = np.dtype((str, 12))
        self._chip = np.array(['---' for _ in range(MAX_NO_CLUSTER)], dtype= chiptype)
        self._chiprev = np.array([0  for _ in range(MAX_NO_CLUSTER)], dtype= np.int8 )
        self._chipsubrev = np.array([0 for _ in range(MAX_NO_CLUSTER)], dtype= np.int8)
        self._guid = np.array(['' for _ in range(MAX_NO_CLUSTER)], dtype= guidtype)
        self._wasUpdated = True        
        self._commCounters = np.zeros((MAX_NO_CLUSTER, COMMCOUNTER), dtype=np.int)    
        self._lastError  = np.zeros((MAX_NO_CLUSTER), dtype=np.int)    
        self._lastStatus = np.zeros((MAX_NO_CLUSTER), dtype=np.int)    
        self._lastRterm = np.zeros((MAX_NO_CLUSTER), dtype=np.int)


    def changeNoOfNodes(self, NoOfNodes):
        if NoOfNodes<>self.NoOfNodes:
            self.NoOfNodes = int(NoOfNodes)
            self._wasUpdated = True

    def wasUpdated(self):
        if self._wasUpdated:
            self._wasUpdated = False
            return True
        return False    

    def cidCheck(self, cid):
        if cid<1:
            print "CID out off range"
            return False
        if cid>MAX_NO_CLUSTER:
            print "CID out off range"
            return False
        return True

    def test(self):
        print "chip:      ", self._chip
        print "chiprev:   ", self._chiprev
        print "chipsubrev:", self._chipsubrev
        print "guid:      ", self._guid

    def setChip(self, cid, chiptext):
        if self.cidCheck(cid):
            if self._chip[cid-1]<>chiptext:
                self._chip[cid-1] = chiptext
                self._wasUpdated = True        
    
    def setChipRev(self, cid, rev):
        if self.cidCheck(cid):
            if self._chiprev[cid-1]<>rev:
                self._chiprev[cid-1] = rev
                self._wasUpdated = True        

    def setChipSubRev(self, cid, subrev):
        if self.cidCheck(cid):
            if self._chipsubrev[cid-1]<>subrev: 
                self._chipsubrev[cid-1] = subrev
                self._wasUpdated = True        

    def setGuid(self, cid, guid):
        if self.cidCheck(cid):
            if guid<>self._guid[cid-1]:
                self._guid[cid-1] = guid
                self._wasUpdated = True        

    def setCommCounter(self, cid, counter):
        if self.cidCheck(cid):
            self._commCounters[cid-1][counter] = self._commCounters[cid-1][counter] + 1
            self.setLastError(cid, counter)
            self._wasUpdated = True        

    def setRtermStatus(self, cid, counter):
        if(counter == 0):
            self._lastRterm[cid-1] = 0
        else:
            self._lastRterm[cid-1] = 1

    def setLastError(self, cid, counter):
        self._lastStatus[cid-1] = counter
        if counter<>0:
            # store / latch last error 
            self._lastError[cid-1] = counter
                
    def getLastError(self, cid):
        return self._lastError[cid-1]

    def getLastStatus(self, cid):
        return self._lastStatus[cid-1]

    def getLastRterm(self, cid):
        return self._lastRterm[cid-1]

    def clearLastStatus(self, cid):
        self._lastStatus[cid-1] = 0
        
    def clearLastError(self, cid):
        self._lastError[cid-1] = 0

    def reverseRterm(self, cid):
        RtermS = self._lastRterm[cid-1]
        self._lastRterm[cid - 1] = not(RtermS)
        Rarray = (self._lastRterm[0] << 0) | (self._lastRterm[1] << 1) | (self._lastRterm[2] << 2) | (self._lastRterm[3] << 3) \
                 | (self._lastRterm[4] << 4) | (self._lastRterm[5] << 5) | (self._lastRterm[6] << 6) | (self._lastRterm[7] << 7)\
                 | (self._lastRterm[8] << 8) | (self._lastRterm[9] << 9) | (self._lastRterm[10] << 10) | (self._lastRterm[11] << 11)\
                 | (self._lastRterm[12] << 12) | (self._lastRterm[13] << 13) | (self._lastRterm[14] << 14)

        return Rarray

    def getCommCounter(self, cid, counter):
        if self.cidCheck(cid):
            return self._commCounters[cid-1][counter]

    def getCommCounters(self, cid):
        if self.cidCheck(cid):
            return self._commCounters[cid-1]


    def clearCommCounters(self):
        self._commCounters = np.zeros((MAX_NO_CLUSTER, COMMCOUNTER), dtype=np.int)    

    def getChip(self, cid):
        if self.cidCheck(cid):
            return self._chip[cid-1]

    
    def getGuid(self, cid):
        if self.cidCheck(cid):
            return self._guid[cid-1]

    def getSelectedChip(self, cid):
        selectedChip = "{}p{}".format(self._chip[cid-1], self._chiprev[cid-1])       #e.g. MC33772p1
        return selectedChip
            
    def getData(self):
#        print self.NoOfNodes
        data = [(self._chip[cid], self._chiprev[cid], self._chipsubrev[cid], self._guid[cid])  for cid in range(0, self.NoOfNodes)]      
        return data
        
# -----------------------------------------------------------------------------        
class BCCNodeWidget(QWidget):
    
    def __init__ (self, parent = None):
        super(BCCNodeWidget, self).__init__(parent)
        # ---- widget layout ----
        self.nodedetails = QVBoxLayout()
        self.lbChip = QLabel()
        self.lbChiprev = QLabel()
        self.lbGuid = QLabel()
        self.nodedetails.addWidget(self.lbChip)
        self.nodedetails.addWidget(self.lbChiprev)
        self.nodedetails.addWidget(self.lbGuid)

        self.allQHBoxLayout  = QHBoxLayout()
        self.lbNode      = QLabel()

        self.allQHBoxLayout.addWidget(self.lbNode, 0)
        self.allQHBoxLayout.addLayout(self.nodedetails, 1)
        self.setLayout(self.allQHBoxLayout)
        # setStyleSheet
#        self.guid.setStyleSheet('''
#            color: rgb(0, 0, 255);
#        ''')
        #blue and bold
        self.lbNode.setStyleSheet('''
            font-weight: bold; 
        ''')
#        self.setStyleSheet('''
#            border: 2px solid green; 
#            border-radius: 4px;''')
        
    def setNode(self, cid):
        self.lbNode.setText("CID-{:02}".format(cid))

    def setChip(self, chip):
        self.lbChip.setText("Chip: {:}".format(chip))

    def setChipRev(self, rev, subrev):
        self.lbChiprev.setText("Rev: {:}.{:}".format(rev, subrev))

    def setGUID(self, guid):
        self.lbGuid.setText("GUID: {:}".format(guid))

# -----------------------------------------------------------------------------        
class BCCListWidget(QListWidget):
    
    nodeClicked = pyqtSignal(int)    
    
    def __init__(self, parent = None):
        super(BCCListWidget, self).__init__()
        self.itemClicked.connect(self.nodeSelected)

    def update(self, nodeList):
        self.clear()
        cid = 1
        for chip, chiprev, chipsubrev, guid in nodeList:
            self.addNode(cid, chip, chiprev, chipsubrev, guid)
            cid = cid + 1
        
    def addNode(self, cid, chip, rev, subrev, guid):      
        # Create QCustomQWidget
        myQCustomQWidget = BCCNodeWidget()
        myQCustomQWidget.setChip(chip)
        myQCustomQWidget.setChipRev(rev, subrev)
        myQCustomQWidget.setNode(cid)
        myQCustomQWidget.setGUID(guid)
        # Create QListWidgetItem
        myQListWidgetItem = QListWidgetItem(self)
        # Set size hint
        myQListWidgetItem.setSizeHint(myQCustomQWidget.sizeHint())

        # Add QListWidgetItem into QListWidget
        self.addItem(myQListWidgetItem)
        self.setItemWidget(myQListWidgetItem, myQCustomQWidget)

    def deleteNode(self, n):  
        self.removeItemWidget(self.item(n-1))


    def nodeSelected(self, item):
        cid = self.currentRow() + 1
        self.nodeClicked.emit(cid)


# -----------------------------------------------------------------------------
if __name__ == "__main__":
    app = QApplication(sys.argv)
#    window = exampleQMainWindow()
    window = QMainWindow()
    window.show()
    nodeView = BCCListWidget()    
    window.setCentralWidget(nodeView) 
   
    nodes = Clusters(4)

    nodes.setChip(1, "MC33772")
    nodes.setChipRev(1, 2)
    nodes.setChipSubRev(1, 1)
    nodes.setGuid(1, "0x00000002")
    
    print nodes._guid
    print nodes.getGuid(3)
    
    nodes.test()
    print nodes.getData()
    nodeView.update(nodes.getData())
   
    sys.exit(app.exec_())
