# -*- coding: utf-8 -*-
# ------------------------------------------------------------------------------
# Copyright (c) 2015, NXP Semiconductors.
# All rights reserved.
#  
# Redistribution and use in source and binary forms, with or without modification,
# are permitted provided that the following conditions are met:
#  
# o Redistributions of source code must retain the above copyright notice, this list
#   of conditions and the following disclaimer.
#  
# o Redistributions in binary form must reproduce the above copyright notice, this
#   list of conditions and the following disclaimer in the documentation and/or
#   other materials provided with the distribution.
#  
# o Neither the name of NXP Semiconductors nor the names of its
#   contributors may be used to endorse or promote products derived from this
#   software without specific prior written permission.
#  
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
# ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
# WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
# DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR
# ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
# (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
# LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
# ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
# (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
# SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
# ------------------------------------------------------------------------------

"""
Created on Thu Sep 08 16:17:31 2016

@author: R51406
@version: 
"""
import time
from PyQt4.QtCore import QThread, pyqtSignal
from PyQt4.QtCore import *
from CommId import *                                   
from MC3377x_RegMap import *

import ctypes
from ctypes import *
class InitConfig(Structure):  
    _fields_ = [ ("AccCode", c_ulong),  
                ("AccMask", c_ulong),  
                ("Reserved", c_ulong) , 
                ("Filter", c_ubyte) , 
                ("Timing0", c_ubyte) , 
                ("Timing1", c_ubyte ), 
                ("Mode", c_ubyte)] 
class CAN_OBJ(Structure):  
    _fields_ = [ ("ID", c_ulong),  
                ("TimeStamp", c_ulong),  
                ("TimeFlag", c_ubyte),  
                ("SendType", c_ubyte),  
                ("RemoteFlag", c_ubyte),  
                ("ExternFlag", c_ubyte),  
                ("DataLen", c_ubyte),  
                ("Data", c_ubyte * 8),  
                ("Reserved", c_ubyte * 3)]

class Worker(QThread):

    sigRxData = pyqtSignal(int,int,int,str)
    sigBytesWaiting = pyqtSignal(int)
    
#    GC_CANdll = ctypes.windll.LoadLibrary( "ECanVci.dll" )
    VCI_CANdll= ctypes.windll.LoadLibrary( "ControlCAN.dll" )

    #support GuangCheng CAN card device
 #  CAN_GC_Func = {
 #       'Open': GC_CANdll.OpenDevice,
 #       'Init': GC_CANdll.InitCAN,
 #       'Start': GC_CANdll.StartCAN,
 #       'Close': GC_CANdll.CloseDevice,
 #       'Transmit': GC_CANdll.Transmit,
 #       'Receive': GC_CANdll.Receive,
 #   }

    #support ChuangXin CAN card device
    CAN_VCI_Func = {
        'Open': VCI_CANdll.VCI_OpenDevice, 
        'Init': VCI_CANdll.VCI_InitCAN,        
        'Start': VCI_CANdll.VCI_StartCAN, 
        'Close': VCI_CANdll.VCI_CloseDevice,     
        'Transmit': VCI_CANdll.VCI_Transmit,     
        'Receive': VCI_CANdll.VCI_Receive,     
    }
    CAN_Func = CAN_VCI_Func;
    
    isConnected = False
    #CAN device parameter
    DeviceType = 4
    DeviceInd= 0
    CANInd = 0
    #CAN transfer object
    CANRev = (CAN_OBJ *50)()
    CANSnd = CAN_OBJ()
#    framecnt = 0
    errMsg = 0

    def __init__(self, parent = None):
        QThread.__init__(self, parent)
        self.exiting = False
        self.buffer = ''

    def __del__(self):
        self.exiting = True
        self.wait()
        
    def openport(self, port, baud):
        #CAN initialization parameter.
        self.CANInitConfig = InitConfig()
        self.CANInitConfig.AccCode = 0
        self.CANInitConfig.AccMask = 0xFFFFFFFF
        self.CANInitConfig.Filter = 0
        self.CANInitConfig.Timing0 = 0
        self.CANInitConfig.Mode = 0
        if baud == 500:
            self.CANInitConfig.Timing1 = 0x1C
        elif baud == 1000:
            self.CANInitConfig.Timing1 = 0x14
        else:
            self.CANInitConfig.Timing1 = 0x14
            
        try:
            self.res = self.CAN_VCI_Func['Open'](self.DeviceType, self.DeviceInd, 0)
            print("GC_OpenDevice ", self.res)
            if self.res != 1:
                self.res = self.CAN_VCI_Func['Open'](self.DeviceType, self.DeviceInd, 0)
                if self.res ==1:
                    self.CAN_Func = self.CAN_VCI_Func;
                print("VCI_OpenDevice ", self.res)
            
            self.res = self.CAN_Func['Init'](self.DeviceType, self.DeviceInd, self.CANInd, byref(self.CANInitConfig))
            if self.res == 1:
                print("InitCAN ", self.res)
                
            self.res = self.CAN_Func['Start'](self.DeviceType, self.DeviceInd,  self.CANInd)
            if self.res == 1:
                print("StartCAN ", self.res)
                self.isConnected = True
                
            self.start()            # start the thread  -> run()
#            self.framecnt = 0
        except ValueError:
            print "could not open CAN{}".format(port+1)
 
    def closeport(self):
        self.wait()             # stop the thread    
        self.res = self.CAN_Func['Close'](self.DeviceType, self.DeviceInd)
        if self.res == 1:
            print("CloseDevice ", self.res)
            self.isConnected = False
        
#        print "FrameCnt:", self.framecnt
    def send(self, byte):
        #send only one frame at one time. no necessary bulk send
        self.CANSnd.ID = 0x18800000 # CAN ID
        self.CANSnd.SendType = 0 #0 - nromal send; 1 single send(no auto resend); 2 self send & receive ;3 single self send & receive for CAN card test
        self.CANSnd.RemoteFlag = 0 #0 - data frame; 1- remote frame
        self.CANSnd.ExternFlag = 1 #0 - standard frame; 1- external frame
        self.CANSnd.DataLen = 8 # data length , no more than 8
        self.CANSnd.Data[0] = byte[0] #1 byte for command type 
        self.CANSnd.Data[1] = byte[1] #1 byte for command data
        self.CANSnd.Data[2] = byte[2]  # 3 byte for command type
        self.CANSnd.Data[3] = byte[3]  # 4 byte for command type

        self.res = self.CAN_Func['Transmit'](self.DeviceType, self.DeviceInd, self.CANInd, byref(self.CANSnd) , 1)
        print "CAN Transmit return ", self.res

    def flush(self):
        pass
    def run(self):
        while not self.exiting:
            try:
                self.res = self.CAN_Func['Receive'](self.DeviceType, self.DeviceInd, self.CANInd, byref(self.CANRev) , 50, 100)
            except ValueError:
                print "CAN device Error."
            if self.res>=1:      
                frame = 0
                while frame < self.res:
                    self.decode(frame)                    
                    frame += 1
                #print "Receive ", self.res  

            self.sigBytesWaiting.emit(self.res)              
            time.sleep(0.01)
    def decode(self, frame):
        #decodes one CAN frame
        IDtemp = self.CANRev[frame].ID
        if IDtemp == 0x18811100:
            IDtemp =self.CANRev[frame].ID
        if (self.CANRev[frame].ID & 0xFFF00000) == 0x18800000: #it's a frame from HVBMS master board
            chain = (self.CANRev[frame].ID & 0x000F0000) >>16  #chain ID
            type = (self.CANRev[frame].ID   & 0x0000F000) >> 12 #data type
            cid = (self.CANRev[frame].ID    & 0x00000F00) >> 8 #BCC ID
            bagID = (self.CANRev[frame].ID & 0x000000FF)  #bagID ID
            cid = chain * 10 + cid # TBD node count for each chain,
            if type == 0: #it's a command frame
                pass
            elif type == 1: # it's a voltage data frame
                if bagID ==0:
                   self.sigRxData.emit(cid,ID_MEAS_VPWR,(self.CANRev[frame].Data[0] << 8) | (self.CANRev[frame].Data[1] ),'') 
                   self.sigRxData.emit(cid,ID_MEAS_VCT14,(self.CANRev[frame].Data[2] << 8) | (self.CANRev[frame].Data[3] ),'') 
                   self.sigRxData.emit(cid,ID_MEAS_VCT13,(self.CANRev[frame].Data[4] << 8) | (self.CANRev[frame].Data[5] ),'') 
                   self.sigRxData.emit(cid,ID_MEAS_VCT12,(self.CANRev[frame].Data[6] << 8) | (self.CANRev[frame].Data[7] ),'') 
                elif bagID == 4:
                   self.sigRxData.emit(cid,ID_MEAS_VCT11,(self.CANRev[frame].Data[0] << 8) | (self.CANRev[frame].Data[1] ),'') 
                   self.sigRxData.emit(cid,ID_MEAS_VCT10,(self.CANRev[frame].Data[2] << 8) | (self.CANRev[frame].Data[3] ),'') 
                   self.sigRxData.emit(cid,ID_MEAS_VCT9,(self.CANRev[frame].Data[4] << 8) | (self.CANRev[frame].Data[5] ),'') 
                   self.sigRxData.emit(cid,ID_MEAS_VCT8,(self.CANRev[frame].Data[6] << 8) | (self.CANRev[frame].Data[7] ),'') 
                elif bagID == 8:
                   self.sigRxData.emit(cid,ID_MEAS_VCT7,(self.CANRev[frame].Data[0] << 8) | (self.CANRev[frame].Data[1] ),'') 
                   self.sigRxData.emit(cid,ID_MEAS_VCT6,(self.CANRev[frame].Data[2] << 8) | (self.CANRev[frame].Data[3] ),'') 
                   self.sigRxData.emit(cid,ID_MEAS_VCT5,(self.CANRev[frame].Data[4] << 8) | (self.CANRev[frame].Data[5] ),'') 
                   self.sigRxData.emit(cid,ID_MEAS_VCT4,(self.CANRev[frame].Data[6] << 8) | (self.CANRev[frame].Data[7] ),'') 
                elif bagID == 12:
                   self.sigRxData.emit(cid,ID_MEAS_VCT3,(self.CANRev[frame].Data[0] << 8) | (self.CANRev[frame].Data[1] ),'') 
                   self.sigRxData.emit(cid,ID_MEAS_VCT2,(self.CANRev[frame].Data[2] << 8) | (self.CANRev[frame].Data[3] ),'') 
                   self.sigRxData.emit(cid,ID_MEAS_VCT1,(self.CANRev[frame].Data[4] << 8) | (self.CANRev[frame].Data[5] ),'')
                   self.sigRxData.emit(cid,ID_MEAS_VAN6,(self.CANRev[frame].Data[6] << 8) | (self.CANRev[frame].Data[7] ),'')  
                elif bagID == 16:
                   self.sigRxData.emit(cid,ID_MEAS_VAN5,(self.CANRev[frame].Data[0] << 8) | (self.CANRev[frame].Data[1] ),'') 
                   self.sigRxData.emit(cid,ID_MEAS_VAN4,(self.CANRev[frame].Data[2] << 8) | (self.CANRev[frame].Data[3] ),'') 
                   self.sigRxData.emit(cid,ID_MEAS_VAN3,(self.CANRev[frame].Data[4] << 8) | (self.CANRev[frame].Data[5] ),'') 
                   self.sigRxData.emit(cid,ID_MEAS_VAN2,(self.CANRev[frame].Data[6] << 8) | (self.CANRev[frame].Data[7] ),'') 
                elif bagID == 20:
                   self.sigRxData.emit(cid,ID_MEAS_VAN1,(self.CANRev[frame].Data[0] << 8) | (self.CANRev[frame].Data[1] ),'') 
                   self.sigRxData.emit(cid,ID_MEAS_VAN0,(self.CANRev[frame].Data[2] << 8) | (self.CANRev[frame].Data[3] ),'') 
                   self.sigRxData.emit(cid,ID_MEAS_ICTEMP,(self.CANRev[frame].Data[4] << 8) | (self.CANRev[frame].Data[5] ),'') 
                
            elif type == 2: #it's a current/internal temperature data frame
                self.sigRxData.emit(cid,ID_MEAS_VCUR,(self.CANRev[frame].Data[0] << 24) |(self.CANRev[frame].Data[1] << 16) |(self.CANRev[frame].Data[2] << 8) | (self.CANRev[frame].Data[3] ),'') 
            elif type == 4: #it's a status frame
                self.sigRxData.emit(cid,ID_FAULT_STATUS1,(self.CANRev[frame].Data[0] << 8) | (self.CANRev[frame].Data[1] ),'') 
                self.sigRxData.emit(cid,ID_FAULT_STATUS2,(self.CANRev[frame].Data[2] << 8) | (self.CANRev[frame].Data[3] ),'') 
                self.sigRxData.emit(cid,ID_FAULT_STATUS3,(self.CANRev[frame].Data[4] << 8) | (self.CANRev[frame].Data[5] ),'') 
                self.sigRxData.emit(cid,ID_COM_STATUS,(self.CANRev[frame].Data[6] << 8) | (self.CANRev[frame].Data[7]), '')
            elif type == 5: #it's a diagnose frame
                if bagID ==0:
                    self.sigRxData.emit(cid,ID_MEAS_SM1,(self.CANRev[frame].Data[0] << 24) | (self.CANRev[frame].Data[1]<<16 ) | (self.CANRev[frame].Data[2]<<8 )| (self.CANRev[frame].Data[3] ),'') 
                elif bagID == 1:
                    self.sigRxData.emit(cid,ID_MEAS_SM2,(self.CANRev[frame].Data[0] << 24) | (self.CANRev[frame].Data[1]<<16 ) | (self.CANRev[frame].Data[2]<<8 )| (self.CANRev[frame].Data[3] ),'') 
                elif bagID == 2:
                    self.sigRxData.emit(cid,ID_MEAS_SM3,(self.CANRev[frame].Data[0] << 24) | (self.CANRev[frame].Data[1]<<16 ) | (self.CANRev[frame].Data[2]<<8 )| (self.CANRev[frame].Data[3] ),'') 
                elif bagID == 3:
                    self.sigRxData.emit(cid,ID_MEAS_SM4,(self.CANRev[frame].Data[0] << 24) | (self.CANRev[frame].Data[1]<<16 ) | (self.CANRev[frame].Data[2]<<8 )| (self.CANRev[frame].Data[3] ),'') 
                elif bagID == 4:
                    self.sigRxData.emit(cid,ID_MEAS_SM5,(self.CANRev[frame].Data[0] << 24) | (self.CANRev[frame].Data[1]<<16 ) | (self.CANRev[frame].Data[2]<<8 )| (self.CANRev[frame].Data[3] ),'') 
                elif bagID == 5:
                    self.sigRxData.emit(cid,ID_MEAS_SM6,(self.CANRev[frame].Data[0] << 24) | (self.CANRev[frame].Data[1]<<16 ) | (self.CANRev[frame].Data[2]<<8 )| (self.CANRev[frame].Data[3] ),'') 
                elif bagID == 6:
                    self.sigRxData.emit(cid,ID_MEAS_SM7,(self.CANRev[frame].Data[0] << 24) | (self.CANRev[frame].Data[1]<<16 ) | (self.CANRev[frame].Data[2]<<8 )| (self.CANRev[frame].Data[3] ),'') 
                elif bagID == 7:
                    self.sigRxData.emit(cid,ID_MEAS_SM36,(self.CANRev[frame].Data[0] << 24) | (self.CANRev[frame].Data[1]<<16 ) | (self.CANRev[frame].Data[2]<<8 )| (self.CANRev[frame].Data[3] ),'') 
                elif bagID == 8:
                    self.sigRxData.emit(cid,ID_MEAS_SM40,(self.CANRev[frame].Data[0] << 24) | (self.CANRev[frame].Data[1]<<16 ) | (self.CANRev[frame].Data[2]<<8 )| (self.CANRev[frame].Data[3] ),'') 
                elif bagID == 9:
                    self.sigRxData.emit(cid,ID_MEAS_SM41,(self.CANRev[frame].Data[0] << 24) | (self.CANRev[frame].Data[1]<<16 ) | (self.CANRev[frame].Data[2]<<8 )| (self.CANRev[frame].Data[3] ),'') 
                elif bagID == 10:
                    self.sigRxData.emit(cid,ID_MEAS_SMX_AN,(self.CANRev[frame].Data[0] << 24) | (self.CANRev[frame].Data[1]<<16 ) | (self.CANRev[frame].Data[2]<<8 )| (self.CANRev[frame].Data[3] ),'') 
                elif bagID == 11:
                    self.sigRxData.emit(cid,ID_MEAS_SMX_OTHER,(self.CANRev[frame].Data[0] << 24) | (self.CANRev[frame].Data[1]<<16 ) | (self.CANRev[frame].Data[2]<<8 )| (self.CANRev[frame].Data[3] ),'') 

            elif type == 6: #it's a config reg
                RegAddr1 = self.CANRev[frame].Data[1]
                RegAddr2 = self.CANRev[frame].Data[5]
                RegMap = MC3377x_CTRL
                for index in range(len(RegMap)):
                    if RegAddr1 == RegMap[index][0][2]:
                        CtrlReg1 = (self.CANRev[frame].Data[2]<<8)|(self.CANRev[frame].Data[3])
                        self.sigRxData.emit(cid, RegMap[index][0][0],CtrlReg1,'')
                    elif RegAddr2 == RegMap[index][0][2]:
                        CtrlReg2 = (self.CANRev[frame].Data[6]<<8)|(self.CANRev[frame].Data[7])
                        self.sigRxData.emit(cid, RegMap[index][0][0],CtrlReg2,'')
            elif type == 7: #it's a status reg
                RegAddr1 = self.CANRev[frame].Data[1]
                RegAddr2 = self.CANRev[frame].Data[5]
                RegMap = MC3377x_STAT
                for index in range(len(RegMap)):
                    if RegAddr1 == RegMap[index][0][2]:
                        STATReg1 = (self.CANRev[frame].Data[2]<<8)|(self.CANRev[frame].Data[3])
                        self.sigRxData.emit(cid, RegMap[index][0][0],STATReg1,'')
                    elif RegAddr2 == RegMap[index][0][2]:
                        STATReg2 = (self.CANRev[frame].Data[6]<<8)|(self.CANRev[frame].Data[7])
                        self.sigRxData.emit(cid, RegMap[index][0][0],STATReg2,'')
            elif type == 8:
                self.sigRxData.emit(1, ID_LV_NTC1, (self.CANRev[frame].Data[0] << 8) | (self.CANRev[frame].Data[1]),'')
                self.sigRxData.emit(1, ID_LV_NTC2, (self.CANRev[frame].Data[2] << 8) | (self.CANRev[frame].Data[3]),'')
                self.sigRxData.emit(1, ID_LV_CRASH, (self.CANRev[frame].Data[4] << 8) | (self.CANRev[frame].Data[5]), '')
            elif type == 0xA:
                RtermStatus = (self.CANRev[frame].Data[0] << 8) | self.CANRev[frame].Data[1];
                for clusterid in range(1, 15):
                    self.sigRxData.emit(clusterid, ID_RTERM,(RtermStatus & (1<<(clusterid-1))), '')
            else:
                pass
                
            if type == 3:
                self.errMsg =0
                errCode = self.CANRev[frame].Data[1] 
                if errCode == 0xA3: #RXtimeout -- No Response error
                    #['Ok', "Tx", "NoRes", "Len", "CRC" ] 
                    self.errMsg = 2
                elif errCode == 0x08: #TPL bus busy - TX error
                    self.errMsg = 1
                #elif(errCode == )
                    #self.errMsg = "Len"
                elif errCode == 0x14: # CRC error
                    self.errMsg = 4
                else:
                    pass
                self.sigRxData.emit(cid, ID_COMM_ERR, self.errMsg,'')

        else:#BJB msg
            ID= self.CANRev[frame].ID
            if ID == 0x84: #VSENSE 0 - 3 0x84
                self.sigRxData.emit(0, ID_VSENSE0, self.CANRev[frame].Data[1] | (self.CANRev[frame].Data[0] << 8),  '' )
                self.sigRxData.emit(0, ID_VSENSE1, self.CANRev[frame].Data[3] | (self.CANRev[frame].Data[2] << 8), '' )
                self.sigRxData.emit(0, ID_VSENSE2, self.CANRev[frame].Data[5] | (self.CANRev[frame].Data[4] << 8), '')
                self.sigRxData.emit(0, ID_VSENSE3, self.CANRev[frame].Data[7] | (self.CANRev[frame].Data[6] << 8), '' )
            elif ID == 0x85: # ISENSE , 0, TSENSE[0] 0x85
                self.sigRxData.emit(0, ID_ISENSE,   self.CANRev[frame].Data[3] | (self.CANRev[frame].Data[2] << 8) | (self.CANRev[frame].Data[1] <<16) | (self.CANRev[frame].Data[0] << 24) , '')
                self.sigRxData.emit(0, ID_TCHIP,      self.CANRev[frame].Data[5] | (self.CANRev[frame].Data[4] << 8) , '')
                self.sigRxData.emit(0, ID_TSENSE0, self.CANRev[frame].Data[7] | (self.CANRev[frame].Data[6] << 8) , '')
            elif ID == 0x86:#TSENSE 1-4
                self.sigRxData.emit(0, ID_TSENSE1, self.CANRev[frame].Data[1] | (self.CANRev[frame].Data[0] << 8) , '')
                self.sigRxData.emit(0, ID_TSENSE2, self.CANRev[frame].Data[3] | (self.CANRev[frame].Data[2] << 8) , '')
                self.sigRxData.emit(0, ID_TSENSE3, self.CANRev[frame].Data[5] | (self.CANRev[frame].Data[4] << 8) , '')
                self.sigRxData.emit(0, ID_TSENSE4, self.CANRev[frame].Data[7] | (self.CANRev[frame].Data[6] << 8) , '')
def main():
    app = QApplication(sys.argv)
    form = ApplicationWindow()
    form.show()
    app.exec_()
