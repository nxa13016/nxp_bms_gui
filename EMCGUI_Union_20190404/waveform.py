#coding=utf-8
import sip
sip.setapi('QString', 2)
sip.setapi('QVariant', 2)
sip.setapi('QDate', 2)
sip.setapi('QDateTime', 2)
sip.setapi('QTextStream', 2)
sip.setapi('QTime', 2)
sip.setapi('QUrl', 2)
from PyQt4.QtCore import QThread, pyqtSignal
from PyQt4.QtGui import *
from guiqwt.plot import PlotManager, CurvePlot
from guiqwt.builder import make
from array import array 
from CommId import * 
from RingBuf import * 
import numpy as np

CID_DEFINE =  ["CID1","CID2","CID3","CID4","CID5","CID6","CID7","CID8","CID9","CID10","CID11","CID12","CID13","CID14","CID15"]
PLOT_DEFINE =  ["CT1", "CT2", "CT3", "CT4", "CT5", "CT6", "CT7","CT8", "CT9", "CT10", "CT11", "CT12", "CT13", "CT14"]

COLORS = ['green', 'gray', 'darkblue', 'purple', 'orange', 'peru', 'darkred', 'aqua', 'cadetblue', 'cyan', 'gold', 'firebrick', 'indianred', "red",]
DT = 1
CID_MAX = 50

class RealtimeDemo(QWidget):
    sigWaveData = pyqtSignal(int,int,float)

    plotNum = 0
    WaveData = np.zeros((50,14)) #[RingBuf(15)] * 50 #upto 15 CTx channels x CID(50)
    waveExist = 0
    needRefresh = 0;
    def __init__(self):
        super(RealtimeDemo, self).__init__()
        self.setWindowTitle(u"CTx Voltage Monitoring")
        self.timeid = 0
        self.data = {u"t":array("d")}
        self.plot = { CurvePlot()}
        for name2 in CID_DEFINE:
            for name1 in PLOT_DEFINE:
                name = name2 + "-" + name1
                self.data[name] = array('d')

########

    def __del__(self):
        self.waveExist = 0
        self.stopTimer()
    def setup_toolbar(self, NoOfNode):
        self.auto_yrange_checkbox = QCheckBox("Y AutoScale")
        self.auto_xrange_checkbox = QCheckBox("X AutoScale")
        self.xrange_box = QSpinBox()
        self.xrange_box.setMinimum(1)
        self.xrange_box.setMaximum(150)
        self.xrange_box.setValue(5)
        self.auto_xrange_checkbox.setChecked(True)        
        self.auto_yrange_checkbox.setChecked(True)
        self.CIDItem = QComboBox()
        for chooseitem in range(NoOfNode):
            self.CIDItem.addItem(CID_DEFINE[chooseitem])
#        for curve_name in enumerate(PLOT_DEFINE):
#            QCheckBox(curve_name)
#            toolbar.addWidget(QCheckBox(curve_name))
        self.toolbar.addWidget(self.auto_yrange_checkbox)
        self.toolbar.addWidget(self.auto_xrange_checkbox)
        self.toolbar.addWidget(self.xrange_box)
        self.toolbar.addWidget(self.CIDItem)

        return self.toolbar

    def createCurve(self, NoOfNode):
        Arr_Temp = []
        for i in range(NoOfNode):
            Arr_Temp.append(CID_DEFINE[i])

            #        plot = [PLOT_DEFINE] * CID_MAX
        self.curves = {}
        self.t = 0
        self.toolbar = QToolBar(self)
        self.setup_toolbar(NoOfNode)
        self.toolbar.setGeometry(10,10,500,50)

        self.manager = PlotManager(self)

        self.plots = []
        self.plotapp = [CurvePlot()]*20
        for i, CCID in enumerate(Arr_Temp):
            plot = CurvePlot(self)
            plot.axisScaleDraw(CurvePlot.Y_LEFT).setMinimumExtent(60)
            self.manager.add_plot(plot)
            self.plots.append(plot)
            plot.plot_id = id(plot)
            for j, curve_name in enumerate(PLOT_DEFINE):
                curve_name = CCID +"-"+ curve_name
                curve = self.curves[curve_name] = make.curve([0], [0], color=COLORS[j], title=curve_name)
                plot.add_item(curve)
            plot.add_item(make.legend("BL"))
            self.plotapp[i] = plot
            self.plotapp[i].setGeometry(50, 50, 0, 0)
            if i == 0:
                self.plotapp[i].setGeometry(50,50,800,600)
        self.plotNum = i+1

        self.manager.register_standard_tools()
        self.manager.get_default_tool().activate()
        self.manager.synchronize_axis(CurvePlot.X_BOTTOM, self.manager.plots.keys())

    def chooseMonitorCID(self):
        sender = self.sender()
        for i, CCID in enumerate(CID_DEFINE):
            unitext = self.CIDItem.currentText()
            strtext = unitext.encode('unicode-escape').decode('string_escape')
            if CCID == strtext:
                break;
        for j in range(self.plotNum):
            if j == i :
                self.plotapp[j].setGeometry(50, 50, 800, 600)
            else:
                self.plotapp[j].setGeometry(50, 50, 0, 0)

    def startMonitor(self):
        self.timeid = self.startTimer(DT*1000)
    def DataEngine(self, cid, idx, data):
        t = self.t
        self.WaveData[cid][idx] = data
        self.needRefresh = 1;
    def timerEvent(self, event):

        self.waveExist = 1
        if self.needRefresh == 0:
            return        
        self.needRefresh = 0
        t = self.t
        self.data[u"t"].append(t)
        self.t += DT
        for cid in range(2):
            for CT in range(14):
                curvename = CID_DEFINE[cid] + "-" + PLOT_DEFINE[CT]
                self.data[curvename].append(self.WaveData[cid][CT])

#            self.t += DT

        if self.auto_xrange_checkbox.isChecked():
            xmax = self.data["t"][-1]
            xmin = max(xmax - self.xrange_box.value(), 0)
        else:
            xmin, xmax = self.plots[0].get_axis_limits('bottom')

 #           for key, curve in self.curves.iteritems():
 #               ydata = key
        for key, curve in self.curves.iteritems():
            xdata = self.data[u"t"]
            ydata = self.data[key]
                #x, y = get_peak_data(xdata, ydata, xmin, xmax, 100, 1/DT)
            curve.set_data(xdata, ydata)

        for plot in self.plots:
            if self.auto_yrange_checkbox.isChecked() and self.auto_xrange_checkbox.isChecked():
                plot.do_autoscale()
            elif self.auto_xrange_checkbox.isChecked():
                plot.set_axis_limits("bottom", xmin, xmax)
                plot.replot()
            else:
                plot.replot()

    def closeEvent(self, event):
        for i in range(self.plotNum):
            self.plots[i].del_all_items()
            sip.delete(self.plotapp[i])
        self.toolbar.clear()
        sip.delete(self.toolbar)
        self.waveExist = 0
        self.killTimer(self.timeid)
        self.data = {u"t":array("d")}
        for name2 in CID_DEFINE:
            for name1 in PLOT_DEFINE:
                name = name2 + "-" + name1
                self.data[name] = array('d')
        self.timeid = 0

def main():    
    import sys
    app = QApplication(sys.argv)
    form = RealtimeDemo()
    form.startTimer(200)
    form.show()
    sys.exit(app.exec_())
    

if __name__ == '__main__':
    main()

