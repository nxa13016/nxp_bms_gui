
import sys
from PyQt4.QtCore import *                              #@UnusedWildImport (Eclipse)
from PyQt4.QtGui import *                               #@UnusedWildImport (Eclipse)
#from MySerial import *                                  #@UnusedWildImport (Eclipse)
#from batpack import *                                   #@UnusedWildImport (Eclipse)
from MC3377x import *                                   #@UnusedWildImport (Eclipse)
from CommId import *                                    #@UnusedWildImport (Eclipse)
from MC3377x_RegMap import *
from HtmlColors import *

from uiEMCGuiMain import Ui_MainWindow
#import numpy as np


CONFIGVIEW = [
    #  ID                ,    name              ,  decoder   , RegAddress
#( ID_BCCSTATE            , 'Fault'              , ''    , codeHex16  ,      0 ),
( ID_INIT                , 'INIT'            ,  None       ,      0x01 ),
( ID_SYS_CFG_GLOBAL      , 'SYS_CFG_GLOBAL', None       ,      0x02 ),
( ID_SYS_CFG1            , 'SYS_CFG1'       , codeVPwr   ,      0x03 ),
( ID_SYS_CFG2            , 'SYS_CFG2'       , codeVCell  ,      0x04 ),
( ID_SYS_DIAG            , 'SYS_DIAG'       , codeVCell  ,      0x05 ),
( ID_ADC_CFG             , 'ADC_CFG'        , codeVCell  ,      0x06 ),
( ID_OV_UV_EN            , 'OV_UV_EN'       , codeVCell  ,      0x08 ),
( ID_GPIO_CFG1           , 'GPIO_CFG1'      , codeVCell  ,      0x1D ),
( ID_GPIO_CFG2           , 'GPIO_CFG2'      , codeVCell  ,      0x1E ),
( ID_FAULT_MASK1         , 'FAULT_MASK1'    , codeVCell  ,      0x27 ),
( ID_FAULT_MASK2         , 'FAULT_MASK2'    , codeVCell  ,      0x28 ),
( ID_FAULT_MASK3         , 'FAULT_MASK3'    , codeVCell  ,      0x29 ),
( ID_WAKEUP_MASK1        , 'WAKEUP_MASK1'   , codeVCell  ,      0x2A ),
( ID_WAKEUP_MASK2        , 'WAKEUP_MASK2'   , codeVCell  ,      0x2B ),
( ID_WAKEUP_MASK3        , 'WAKEUP_MASK3'   , codeVCell  ,      0x2C ),
( ID_ADC2_OFFSET_COMP    , 'ADC2_OFFSET_COMP       ', codeVCell  ,      0x07 ),
( ID_CB_CFG_1            , 'CB_CFG_1'        , codeVCell  ,      0x0C ),
( ID_CB_CFG_2            , 'CB_CFG_2'        , codeCur    ,      0x0D ),
( ID_CB_CFG_3            , 'CB_CFG_3'        , codeICDegC ,      0x0E ),
( ID_CB_CFG_4            , 'CB_CFG_4'        , codeAnDegC ,      0x0F ),
( ID_CB_CFG_5            , 'CB_CFG_5'        , codeAnDegC ,      0x10 ),
( ID_CB_CFG_6            , 'CB_CFG_6'        , codeAnDegC ,      0x11 ),
( ID_CB_CFG_7            , 'CB_CFG_7'        , codeAnDegC ,      0x12 ),
( ID_CB_CFG_8            , 'CB_CFG_8'        , codeAnDegC ,      0x13 ),
( ID_CB_CFG_9            , 'CB_CFG_9'        , codeAnDegC ,      0x14 ),
( ID_CB_CFG_10           , 'CB_CFG_10'       , codeAnDegC ,      0x15 ),
( ID_CB_CFG_11           , 'CB_CFG_11'       , codeAnDegC ,      0x16 ),
( ID_CB_CFG_12           , 'CB_CFG_12'       , codeAnDegC ,      0x17 ),
( ID_CB_CFG_13           , 'CB_CFG_13'       , codeAnDegC ,      0x18 ),
( ID_CB_CFG_14           , 'CB_CFG_14'       , codeAnDegC ,      0x19 ),
]

showErrors = True
BITNUM = 16

class TableConfigView(QTableWidget):

    BitClicked = pyqtSignal(int,int)
    NoClusters = 0
    rowHeight = 27
    CtrlRegMap = MC3377x_CTRL

    #bit colors  color off,  color on
    bcolor = [ ( QColor(DarkGreen)  , QColor(LimeGreen)   ),   # 0 - r/w
               ( QColor(SteelBlue)  , QColor(DeepskyBlue) ),   # 1 -  r
               ( QColor(FireBrick)  , QColor(Red)         ),   # 2 - w
               ( QColor(Grey)       , QColor(Grey)        ),   # 3 - not implemented
               ( QColor(DarkOrange) , QColor(Yellow)      ) ]  # 4 - w1

    def __init__(self, parent = None):
        QTableWidget.__init__(self)
        self.cellClicked.connect(self.BitClick)

    def BitClick(self, row, col):
        self.BitClicked.emit(row, col)

    def draw(self, clusters):

        if self.NoClusters == clusters:
            return

        self.NoClusters = clusters
      #  self.configui.table_Configview.clear()
        self.clear()
        # row headers
        rowheader = ["{:}".format(name) for _, name, _, _ in CONFIGVIEW]
  #      if showErrors:
  #          rowheader = rowheader + ["{:}".format(name) for _, name, _, _ in CONFIGVIEW]

        self.setRowCount(len(rowheader))
        self.setVerticalHeaderLabels(rowheader)
        self.setColumnCount(BITNUM)

        # column header
        hheaders = []
        for i in range(BITNUM):
            hheaders.append("BIT-{:02d}".format(15-i))
        self.setHorizontalHeaderLabels(hheaders)

        # first row
        for r in range(0,self.rowCount()):
            self.setRowHeight(r, self.rowHeight)
            self.setColumnWidth(r, 70)
            for c in range(0,self.columnCount()):
                self.setItem(r, c, QTableWidgetItem(self.CtrlRegMap[r][c+1][0]))
                self.item(r, c).setToolTip(self.CtrlRegMap[r][c+1][0])
                self.item(r, c).setTextAlignment(Qt.AlignCenter)
                self.item(r, c).setFlags(Qt.ItemIsEnabled);
                color = self.CtrlRegMap[r][c+1][1]
                self.item(r, c).setBackgroundColor(self.bcolor[color][0])  # bit type

    def update(self,dataset):
        self.index = [x[0] for x in CONFIGVIEW]
        for n in self.index:
            row = self.index.index(n)
            data = dataset[n].getValue()
            if data == None:
                data = 0
            data = int(data)
            bit15to0 = range(15, -1, -1)
            # update bits
            for b in bit15to0:
                bitstate = (data & (2 ** b) > 0)
                color = self.CtrlRegMap[row][16 - b][1]
                self.item(row, 16 - b -1).setBackgroundColor(self.bcolor[color][bitstate])  # bit type

    #                self.lst.item(r,c).setFlags(Qt::ItemIsSelectable|Qt::ItemIsEnabled);
 #   self.setAlternatingRowColors(1)
#        self.setFixedHeight(40+self.NoClusters*30)        # height of whole table
