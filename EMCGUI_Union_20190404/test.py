import numpy as np
from guiqwt.pyplot import *

t = np.linspace(0, 20, 1000)
u = np.sin(t) + np.random.randn(1000)
i = np.cos(t) + np.random.randn(1000)
subplot(2,1,1)
plot(t, u, "r-", label=u"电压")
xlabel(u"时间(秒)")
ylabel(u"电压(伏特)")
legend()
subplot(2,1,2)
plot(t, i, "g-", label=u"电流")
xlabel(u"时间(秒)")
ylabel(u"电流(安培)")
legend()
title(u"电压-电流")
show()
