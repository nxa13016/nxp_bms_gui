# -*- coding: utf-8 -*-
# ------------------------------------------------------------------------------
# Copyright (c) 2015, NXP Semiconductors.
# All rights reserved.
#  
# Redistribution and use in source and binary forms, with or without modification,
# are permitted provided that the following conditions are met:
#  
# o Redistributions of source code must retain the above copyright notice, this list
#   of conditions and the following disclaimer.
#  
# o Redistributions in binary form must reproduce the above copyright notice, this
#   list of conditions and the following disclaimer in the documentation and/or
#   other materials provided with the distribution.
#  
# o Neither the name of NXP Semiconductors nor the names of its
#   contributors may be used to endorse or promote products derived from this
#   software without specific prior written permission.
#  
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
# ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
# WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
# DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR
# ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
# (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
# LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
# ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
# (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
# SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
# ------------------------------------------------------------------------------

"""
Created on Thu Sep 08 16:17:31 2016

@author: R51406
@version: 
"""

import numpy as np

class RingBuf(object):

    """This class is to collect data and the data history (e.g. N last datapoints)"""
    def __init__(self, buflen=1, dtype=np.float):
        self._buflen = buflen
        self._idx = -1
        self._data = np.zeros((buflen), dtype)
        self._full = False  
        
    def flush(self):
        """ Flushes the ring buffer"""        
        self._idx = -1
        self._full = False
        
 
    def setValue(self, newvalue):
        """ adds the *newvalue* to the ring buffer  
        
        Test **bold** *italic*
        
        .. role:: red
        
        An example for :red:'red text'
        
        ============
        Part title..
        ============
        
        Section title..
        ===============
        
        
        ***************
        Chapter title..
        ***************
        
        Subsubsection title..
        ^^^^^^^^^^^^^^^^^^^^^
        
        
        ``monospace``
        
            fsdfsd
        - list
        - list
        :Test:
        
        See the following example:
            
        :Example:
            
        ``monospace asdkjasldhasld``
                    
            
            
        
        :Parameters:
        ============
        
            - `lines`: a list of one-line strings without newlines.
            - `until_blank`: Stop collecting at the first blank line if
              true (1).
            - `strip_indent`: Strip common leading indent if true (1,
              default).
        
        :Return:
            - a list of indented lines with mininum indent removed;
            - the amount of the indent;
            - whether or not the block finished with a blank line or at
              the end of `lines`.
              """
        
        # increment index
        self._idx += 1  
        if self._idx== self._buflen-1:
            self._full = True
        self._idx %=  self._buflen   
        # store value
        self._data[self._idx] = newvalue  
        
        
    def getValue(self, noOfValues='one'):
        """ gets the last value or values. Returns None if no data is available """
        if self._idx==-1:
            return None
        
        if noOfValues=='one':
            return self._data[self._idx]
        elif noOfValues=='all':
            if self._full:
                return np.concatenate((self._data[self._idx+1:], self._data[:self._idx+1]))
            return self._data[:self._idx+1]
    
    def getStat(self):
        """  returns the statistics (min, max, mean, stdev) """
        
        if self._full:
            noofvalues = self._buflen
        else:
            noofvalues = self._idx+1

        if noofvalues==0:
#            print "\nno values"
            data = []          
            stat = {'samples': 0, 'mean': 0.0, 'dev':0, 'min':0, 'max':0}
        else:
            data = self._data[:noofvalues]
            stat = {'samples': noofvalues, 'mean': np.mean(data), 'std': np.std(data), 'min':np.min(data),'max': np.max(data)}

            #print sdata
#          stat = (noofvalues, np.min(data), np.max(data), np.mean(data), np.std(data))            

        return stat        

    def getStat2(self):
        """  returns the statistics (min, max, mean, stdev) """
        
        if self._full:
            noofvalues = self._buflen
        else:
            noofvalues = self._idx+1

        if noofvalues==0:
            mean = 0
            std  = 0
            mini = 0
            maxi = 0
        else:
            data = self._data[:noofvalues]
            mean = np.mean(data)
            std  = np.std(data)
            mini = np.min(data)
            maxi = np.max(data)

        return mean, std, noofvalues, mini, maxi       



# ------------------------------------------------------------------------------
# testing
if __name__ == "__main__":

    print "Testing...."
    data = RingBuf()     
    print data._buflen
      
    data = RingBuf(buflen=10, dtype=np.uint16)       
    print data._buflen
    
    data.setValue(123)
#    data.setValue()
#    print data._idx, data._data
#    print data.getStat()
#    print data.getStat()['mean']
#    print data.getValue()
#    
#    print
#    
#    data.setValue(120)
#    print data._idx, data._data
#    print data.getStat()
#    print "mean=", data.getStat()['mean']
#    print data.getValue()
#    print data.getValue('all')
#    
#    
#    print
#    print
#    print
#    
#    d2 = RingBuf(buflen=10, dtype=np.uint8)
#    [d2.setValue(n) for n in range(23)]
#    print d2._idx, d2._data
#    print d2.getStat()
#    print d2.getStat()['mean']
#    print d2.getValue()
#    print d2.getValue('all')
#    
#    
#    print
#    print
#    print
#    
#    d2 = RingBuf(buflen=10, dtype=np.uint8)
#    [d2.setValue(n) for n in range(15)]
#    print d2._idx, d2._data
#    print d2.getStat()
#    print d2.getStat()['mean']
#    print d2.getValue()
#    print d2.getValue('all')

    size = np.dtype('float').itemsize
    buflen = 36
    items = 1024
    cluster = 15
    print "memory demand for float in byte:", size
    print 
    print "for items:", items
    print "for buflen:", buflen
    print "for cluster:", cluster
    
    print "total:", cluster *items*buflen*size
