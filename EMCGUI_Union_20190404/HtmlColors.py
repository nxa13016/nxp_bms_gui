# -*- coding: utf-8 -*-
"""
// --------------------------------------------------------------------
//  Copyright (c) 2015, NXP Semiconductors.
//  All rights reserved.
//
//  Redistribution and use in source and binary forms, with or without modification,
//  are permitted provided that the following conditions are met:
//
//  o Redistributions of source code must retain the above copyright notice, this list
//    of conditions and the following disclaimer.
//
//  o Redistributions in binary form must reproduce the above copyright notice, this
//    list of conditions and the following disclaimer in the documentation and/or
//    other materials provided with the distribution.
//
//  o Neither the name of NXP Semiconductors nor the names of its
//    contributors may be used to endorse or promote products derived from this
//    software without specific prior written permission.
//
//  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
//  ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
//  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
//  DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR
//  ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
//  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
//  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
//  ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
//  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
//  SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
// --------------------------------------------------------------------
"""
# color definition based on X11 Color Names, see
# http://preformattedtext.weebly.com/html-colors.html


#oranges
OrangeRed       = "#ff4500"
DarkOrange      = "#ffbc00"
Orange          = "#ffa500"
#L               = "#000000"

#yellows
Gold            = "#ffd700"
Yellow          = "#ffff00"

#reds
Red             = "#ff0000"
FireBrick       = "#b22222"
DarkRed         = "#8b0000"

#greens
Lime            = "#00ff00"
LimeGreen       = "#32cd32"
SeaGreen        = "#2e8b57"
ForestGreen     = "#228b22"
Green           = "#008000"
DarkGreen       = "#006400"
YellowGreen     = "#9acd32"

#blues
SteelBlue       = "#4682b4"
DeepskyBlue     = "#00bfff"

#greys
LightGrey       = "#d3d3d3"
Silver          = "#c0c0c0"
DarkGrey        = "#a9a9a9"
Grey            = "#808080"
Black           = "#000000"
